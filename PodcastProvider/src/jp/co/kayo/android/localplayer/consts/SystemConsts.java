package jp.co.kayo.android.localplayer.consts;

public interface SystemConsts {

    public static final String VIDEO_EXT = ".mp4,.avi";
    public static final String MEDIA_EXT = ".mp3,.ogg";

    //public final String SITE = "http://192.168.1.23:8888";
    public final String SITE = "http://justplayer-dev.appspot.com";
    
}
