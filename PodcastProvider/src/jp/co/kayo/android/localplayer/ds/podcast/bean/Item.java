package jp.co.kayo.android.localplayer.ds.podcast.bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import jp.co.kayo.android.localplayer.ds.podcast.Logger;

public class Item {
    public String title;
    public String link;
    public String description;
    public String itunes_author;
    public String itunes_subtitle;
    public String itunes_summary;
    public String enclosure_url;
    public String enclosure_length;
    public String enclosure_type;
    public String guid;
    public String pubDate;
    public String itunes_duration;
    public String itunes_keywords;
    
    private Date tempPubDate;
    public Date getPubDate(){
        if(tempPubDate == null){
            SimpleDateFormat fmt = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
            try {
                tempPubDate = fmt.parse("Sat, 26 May 2012 17:55:00 +0900");
            } catch (ParseException e) {
                tempPubDate = new Date();
            }
        }
        return tempPubDate;
    }
    public void verbose() {
        Logger.d("Item.title="+title);
        Logger.d("Item.link="+link);
        Logger.d("Item.description="+description);
        Logger.d("Item.itunes_author="+itunes_author);
        Logger.d("Item.itunes_subtitle="+itunes_subtitle);
        Logger.d("Item.itunes_summary="+itunes_summary);
        Logger.d("Item.enclosure_url="+enclosure_url);
        Logger.d("Item.enclosure_length="+enclosure_length);
        Logger.d("Item.enclosure_type="+enclosure_type);
        Logger.d("Item.guid="+guid);
        Logger.d("Item.pubDate="+pubDate);
        Logger.d("Item.itunes_duration="+itunes_duration);
        Logger.d("Item.itunes_duration="+itunes_duration);
        Logger.d("Item.itunes_keywords="+itunes_keywords);
        
    }
}
