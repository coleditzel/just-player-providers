package jp.co.kayo.android.localplayer.ds.podcast.bean;

import jp.co.kayo.android.localplayer.ds.podcast.Logger;

public class Image {
    public String url;
    public String title;
    public String link;
    
    public Image(){}
    public Image(String url){
        this.url = url;
    }
    public void vervose() {
        Logger.d("Image.url="+url);
        Logger.d("Image.title"+title);
        Logger.d("Image.link"+link);
    }
}
