package jp.co.kayo.android.localplayer.ds.podcast;

import java.util.ArrayList;
import java.util.HashMap;

import jp.co.kayo.android.localplayer.ds.podcast.bean.Channel;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class SelectListAdapter extends BaseExpandableListAdapter {
    private LayoutInflater mInflator;
    Context mContext;
    ArrayList<CategoryList> mTreemap;
    private int mSortNum;
    
    public SelectListAdapter(Context context, ArrayList<CategoryList> treemap, int sortnum) {
        this.mContext = context;
        this.mTreemap = treemap;
        this.mSortNum = sortnum;
    }
    
    public LayoutInflater getInflator(Context context) {
        if (mInflator == null) {
            mInflator = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        return mInflator;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mTreemap.get(groupPosition).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
            boolean isLastChild, View convertView, ViewGroup parent) {
        View v = getInflator(mContext).inflate(R.layout.child, parent, false);
        
        CategoryList list = mTreemap.get(groupPosition);
        
        Channel channel = list.get(childPosition);
        
        ((TextView)v.findViewById(R.id.textName)).setText(channel.title);
        ((TextView)v.findViewById(R.id.textSummary)).setText(channel.description);
        if(mSortNum == 0){
            ((TextView)v.findViewById(R.id.textAddition)).setText(channel.getCategory());
        }
        else{
            ((TextView)v.findViewById(R.id.textAddition)).setText(channel.language);
        }
        ((TextView)v.findViewById(R.id.textPoint)).setText(String.format(mContext.getString(R.string.txt_point), channel.point));
        
        return v;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mTreemap.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mTreemap.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return mTreemap.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
        View v = getInflator(mContext).inflate(R.layout.parent, parent, false);
        
        CategoryList list = mTreemap.get(groupPosition);
        
        ((TextView)v.findViewById(R.id.textCategory)).setText(list.category);
        
        
        return v;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
