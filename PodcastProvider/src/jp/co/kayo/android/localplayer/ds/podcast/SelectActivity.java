package jp.co.kayo.android.localplayer.ds.podcast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import jp.co.kayo.android.localplayer.ds.podcast.bean.Channel;
import jp.co.kayo.android.localplayer.ds.podcast.bean.Image;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Spinner;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.Toast;

public class SelectActivity extends FragmentActivity implements OnChildClickListener {
    private ExpandableListView listView;
    private Spinner spinSort;
    private ExpandableListAdapter adapter;
    private String json;
    private ArrayList<Channel> channels = new ArrayList<Channel>();
    private ArrayList<CategoryList> treemap = new ArrayList<CategoryList>();
    private int currentSort = 0;
    
    private final int EVT_RELOAD = 0;
    private final int EVT_LIST = 1;
    private final int EVT_TOAST = 2;
    
    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch(msg.what){
                case EVT_RELOAD:{
                    adapter = new SelectListAdapter(SelectActivity.this, treemap, currentSort);
                    listView.setAdapter(adapter);
                }break;
                case EVT_LIST:{}break;
                case EVT_TOAST:{Toast.makeText(SelectActivity.this, (String)msg.obj, Toast.LENGTH_LONG).show();}break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);

        // ソフトウエアキーボードを解除
        this.getWindow().setSoftInputMode(
                LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setContentView(R.layout.list);
        listView = (ExpandableListView)findViewById(android.R.id.list);
        spinSort = (Spinner)findViewById(R.id.spinSort);
        listView.setOnChildClickListener(this);
        spinSort.setOnItemSelectedListener(new OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                    int arg2, long arg3) {
                if(currentSort != arg2){
                    currentSort = arg2;
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            sortMap();
                        }
                    }).start();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
                
            }});
        
        Intent intent = getIntent();
        if(intent!=null){
            json = intent.getStringExtra("json");
            new Thread(new Runnable() {
                @Override
                public void run() {
                    parseJSON();
                }
            }).start();
        }
    }
    
    private void parseJSON(){
        if(json!=null && json.length()>0){
            try {
                JSONArray array = new JSONArray(json);
                for(int i=0; i<array.length(); i++){
                    JSONObject obj = array.getJSONObject(i);
                    Channel c = new Channel();
                    c.title = obj.getString("title");
                    c.description = obj.getString("description");
                    c.language = obj.getString("language");
                    c.itunes_author = obj.getString("owner");
                    c.site = obj.getString("site");
                    c.image = new Image(obj.getString("image"));
                    c.setCategory(obj.getString("category"));
                    c.point = obj.getLong("point");
                    
                    channels.add(c);
                }
                
                sortMap();
            } catch (JSONException e) {
            }
        }
    }
    
    private Comparator<Channel> myComparetor = new Comparator<Channel>(){

        @Override
        public int compare(Channel lhs, Channel rhs) {
            if(lhs.point>rhs.point){
                return -1;
            }
            else if(lhs.point < rhs.point){
                return 1;
            }
            return 0;
        }
        
    };
    
    private void sortMap(){
        treemap.clear();
        HashMap<String, CategoryList> map = new HashMap<String, CategoryList>();
        for(Channel c : channels){
            if(currentSort == 0){
                //languageで分ける場合
                CategoryList list = map.get(c.language);
                if(list == null){
                    list = new CategoryList();
                    list.category = c.language;
                    map.put(c.language, list);
                }
                list.add(c);
            }else{
                //Categoryで分ける場合
                for(String category : c.itunes_category){
                    if(category!=null){
                        CategoryList list = map.get(category);
                        if(list == null){
                            list = new CategoryList();
                            list.category = category;
                            map.put(category, list);
                        }
                        list.add(c);
                    }
                }
            }
        }
        for(CategoryList list : map.values()){
            Collections.sort(list, myComparetor);
        }
        treemap.addAll(map.values());
        mHandler.sendMessage(mHandler.obtainMessage(EVT_RELOAD, treemap));
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v,
            int groupPosition, int childPosition, long id) {
        Channel c = treemap.get(groupPosition).get(childPosition);
        Intent ret = new Intent();
        ret.putExtra("site", c.site);
        setResult(RESULT_OK, ret);
        finish();
        
        return true;
    }

}
