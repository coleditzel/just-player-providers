package jp.co.kayo.android.localplayer.ds.boxnet;

public class FileMetaInfo {
    //%A = アルバム名
    public String album;
    //%a = アーティスト名
    public String artist;
    //%c = ID3のコメント
    public String comment;
    //%T = トラック番号
    public String track;
    //%t = 曲名
    public String title;
    //%y = 年
    public String year;
    //%o = その他
    public String desc;
    
    public String genre;
    public long duration;
    
    public long folderId;
    public long fileId;
    public String name;
    public long albumArt;
    public String cacheFile;
    
    public FileMetaInfo(){
        title = "";
        album = "";
        artist = "";
        genre = "";
        desc = "";
        cacheFile = null;
    }
}
