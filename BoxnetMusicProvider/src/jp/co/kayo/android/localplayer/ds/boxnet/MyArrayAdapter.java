package jp.co.kayo.android.localplayer.ds.boxnet;

import java.util.Date;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MyArrayAdapter extends ArrayAdapter<TreeListItem> {
    private final Context context;

    public MyArrayAdapter(Context contextt, int textViewResourceId, List<TreeListItem> objects) {
        super(contextt, textViewResourceId, objects);
        context = contextt;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView tv = new TextView(context);
        TreeListItem item = getItem(position);
        if (item.type == TreeListItem.TYPE_FOLDER) {
            tv.append("FOLDER: ");
        }
        else if (item.type == TreeListItem.TYPE_FILE) {
            tv.append("FILE: ");
        }
        tv.append(item.name);
        tv.append("\n");
        tv.append(DateFormat.getDateFormat(context).format(new Date(item.updated * 1000)));
        tv.setPadding(10, 20, 10, 20);
        tv.setTypeface(Typeface.DEFAULT_BOLD);
        return tv;
    }
}
