package jp.co.kayo.android.localplayer.ds.boxnet;

import android.os.Build;
import android.os.StrictMode;

public class StrictHelper {
    public static final boolean STRICTMODE = false;
    
    public static void registStrictMode(){
        if(STRICTMODE && Build.VERSION.SDK_INT>8){
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                //.detectDiskReads()
                //.detectDiskWrites()
                .detectNetwork()
                .penaltyLog()
                .penaltyDeath()
                .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .penaltyLog()
                .penaltyDeath()
                .build());
        }
        else if(Build.VERSION.SDK_INT>=11){
        	StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
        	/*
            try {
                Class strictModeClass=Class.forName("android.os.StrictMode");
                Class strictModeThreadPolicyClass=Class.forName("android.os.StrictMode$ThreadPolicy");
                Object laxPolicy = strictModeThreadPolicyClass.getField("LAX").get(null);
                Method method_setThreadPolicy = strictModeClass.getMethod(
                        "setThreadPolicy", strictModeThreadPolicyClass );
                method_setThreadPolicy.invoke(null,laxPolicy);
            } catch (Exception e) {

            }*/
        }
    }

}
