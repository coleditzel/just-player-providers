
package jp.co.kayo.android.localplayer.ds.boxnet;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.kayo.android.localplayer.util.SdCardAccessHelper;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

public class LocalFileBrowser extends FragmentActivity implements OnItemClickListener,
        LoaderCallbacks<List<File>>, OnItemLongClickListener {
    private SharedPreferences prefs;
    private ListView listView;
    private String rootPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_local_tree);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        listView = (ListView) findViewById(android.R.id.list);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);

        String sdcard = SdCardAccessHelper.getExternalStorageDirectory();

        rootPath = prefs.getString("rootPath", sdcard);

        getSupportLoaderManager().initLoader(R.layout.main_local_tree, null, this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Loader<List<File>> onCreateLoader(int arg0, Bundle arg1) {
        LocalFileLoader loader = new LocalFileLoader(this, rootPath);
        loader.forceLoad();
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<File>> arg0, List<File> arg1) {
        LocalFileListViewAdapter adapter = new LocalFileListViewAdapter(this, arg1);
        listView.setAdapter(adapter);
    }

    @Override
    public void onLoaderReset(Loader<List<File>> arg0) {
        LocalFileListViewAdapter adapter = new LocalFileListViewAdapter(this, new ArrayList<File>());
        listView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View arg1, int position, long arg3) {
        File file = (File) adapter.getItemAtPosition(position);
        if (file.toString().equals("..") || file.isDirectory()) {
            if (file.toString().equals("..")) {
                rootPath = new File(rootPath).getParent();
            } else {
                rootPath = file.getPath();
            }
            getSupportLoaderManager().restartLoader(R.layout.main_local_tree, null, this);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapter, View arg1, int position, long arg3) {
        final File file = (File) adapter.getItemAtPosition(position);
        if (!file.toString().equals("..")) {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            final CharSequence[] menus = {
                    getString(R.string.txt_delete), getString(R.string.txt_upload)
            };
            alertDialogBuilder.setCancelable(true);
            alertDialogBuilder.setItems(menus,
                    new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == 0) {
                                deleteFile(file);
                            } else {
                                AsyncUploder task = new AsyncUploder(LocalFileBrowser.this,file);
                                task.execute((Void)null);
                            }
                        }
                    });
            alertDialogBuilder.create().show();
        }
        return false;
    }

    private void deleteFile(final File file) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.txt_caution));
        alertDialogBuilder.setMessage(getString(R.string.txt_delete_file)+"["+file.getName()+"]");
        alertDialogBuilder.setPositiveButton(getString(R.string.txt_yes), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteDirectory(file);
                getSupportLoaderManager().restartLoader(R.layout.main_local_tree, null, LocalFileBrowser.this);
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.txt_yes), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialogBuilder.create().show();
    }
    
    private void deleteDirectory(File file){
        if(file.isDirectory()){
            for(File f : file.listFiles()){
                deleteDirectory(f);
            }
        }
        file.delete();
    }

    private static class LocalFileLoader extends AsyncTaskLoader<List<File>> {
        private String path;
        private String sdcard;

        public LocalFileLoader(Context context, String path) {
            super(context);
            this.path = path;
            sdcard = SdCardAccessHelper.getExternalStorageDirectory();
        }

        @Override
        public List<File> loadInBackground() {
            ArrayList<File> list = new ArrayList<File>();
            if (!path.equals(sdcard)) {
                list.add(new File(".."));
            }
            File dir = new File(path);
            for (File f : dir.listFiles()) {
                String type = LocalFileListViewAdapter.getType(f);
                if (type != null) {
                    list.add(f);
                }
            }

            return list;
        }

    }
}
