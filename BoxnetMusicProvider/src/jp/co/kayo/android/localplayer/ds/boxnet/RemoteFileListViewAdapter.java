package jp.co.kayo.android.localplayer.ds.boxnet;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.util.HashMap;

import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.FileMedia;
import jp.co.kayo.android.localplayer.ds.boxnet.db.BoxnetContentProvider;
import jp.co.kayo.android.localplayer.util.SdCardAccessHelper;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Handler;
import android.provider.BaseColumns;
import android.support.v4.widget.CursorAdapter;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class RemoteFileListViewAdapter extends CursorAdapter {
    LayoutInflater inflator;
    Handler handler = new Handler();
    java.text.DateFormat format;
    HashMap<String, String> names = new HashMap<String, String>();
    int defaultColor = Color.BLACK;

    public RemoteFileListViewAdapter(Context context, Cursor c) {
        super(context, c, true);
        format = DateFormat.getMediumDateFormat(context);

        names.put("folder", context.getString(R.string.txt_type_folder));
        names.put("audio", context.getString(R.string.txt_type_audio));
        names.put("zip", context.getString(R.string.txt_type_zip));
    }

    public LayoutInflater getInflator(Context context) {
        if (inflator == null) {
            inflator = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        return inflator;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        long id = cursor.getLong(cursor.getColumnIndex(FileMedia._ID));
        String title = cursor.getString(cursor.getColumnIndex(FileMedia.TITLE));
        String type = cursor.getString(cursor.getColumnIndex(FileMedia.TYPE));
        String data = cursor.getString(cursor.getColumnIndex(FileMedia.DATA));
        long modified = cursor.getLong(cursor
                .getColumnIndex(FileMedia.DATE_MODIFIED));

        int color = selectColor(type, data);
        
        holder.textView1.setText(title);
        holder.textView2.setText(names.get(type));
        holder.textView3.setText(format.format(modified));
        if(color!=-1){
            holder.textView1.setTextColor(color);
            holder.textView2.setTextColor(color);
            holder.textView3.setTextColor(color);
        }
        else{
            holder.textView1.setTextColor(defaultColor);
            holder.textView2.setTextColor(defaultColor);
            holder.textView3.setTextColor(defaultColor);
        }
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = getInflator(context).inflate(R.layout.text_list_row, parent,
                false);
        ViewHolder holder = new ViewHolder();
        holder.textView1 = (TextView) v.findViewById(R.id.text1);
        holder.textView2 = (TextView) v.findViewById(R.id.text2);
        holder.textView3 = (TextView) v.findViewById(R.id.text3);
        v.setTag(holder);
        return v;
    }
    
    private int selectColor(String type, String data){
        if(type.equals("folder")){
            if(data!=null && isRegisterdAlbum(data.hashCode())){
                return Color.BLUE;
            }
        }else if(type.equals("audio")){
            if(data!=null && isCachedSong(data)){
                return Color.BLUE;
            }
        }
        return -1;
    }
    
    public boolean isRegisterdAlbum(long id) {
        // 有無をチェック
        Cursor cur = null;
        try {
            cur = mContext.getContentResolver().query(
                    BoxnetContentProvider.ALBUM_CONTENT_URI,
                    new String[] {
                            BaseColumns._ID
                    },
                    AudioAlbum.ALBUM_KEY + " = ?",
                    new String[] {
                        Long.toString(id)
                    }, null);
            if (cur != null && cur.moveToFirst()) {
                return true;
            }
            return false;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }
    
    public boolean isCachedSong(String path) {
        return existCachFile(path);
    }
    
    private static String getFilename(String uri) {
        if (uri != null) {
            String ret = uri.replaceAll("auth=[^&]+", "");
            if (ret.equals(uri)) {
                ret = uri.replaceAll("ssid=[^&]+", "");
            }

            String fname = "media" + Integer.toString(ret.hashCode()) + ".dat";
            // Logger.d("fname = "+ fname);
            return fname;
        } else {
            return null;
        }
    }
    
    public static boolean existCachFile(String uri) {
        String f = getFilename(uri);
        if (f != null) {
            File cacheFile = new File(SdCardAccessHelper.cachedMusicDir, f);
            if (cacheFile != null && cacheFile.exists()) {
                return true;
            }
        }
        return false;
    }
    
    class ViewHolder {
        TextView textView1;
        TextView textView2;
        TextView textView3;
        
    }
}
