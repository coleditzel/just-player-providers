package jp.co.kayo.android.localplayer.ds.boxnet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.secret.Keys;
import android.app.ListActivity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.box.androidlib.Box;
import com.box.androidlib.DAO.BoxFile;
import com.box.androidlib.DAO.BoxFolder;
import com.box.androidlib.ResponseListeners.GetAccountTreeListener;

public class Browse extends ListActivity {

    private SharedPreferences prefs;
    private String authToken;
    private long folderId = 0l;
    private MyArrayAdapter adapter;
    ArrayList<TreeListItem> items = new ArrayList<TreeListItem>();
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tree);
        
        prefs = getSharedPreferences(SystemConsts.PREFS_FILE_NAME, 0);
        authToken = prefs.getString(SystemConsts.PREFS_KEY_AUTH_TOKEN, null);
        
        adapter = new MyArrayAdapter(this, 0, items);
        setListAdapter(adapter);
        
        refresh();
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        
        TreeListItem item = adapter.getItem(position);
        folderId = item.id;
        refresh();
    }



    private void refresh() {
        final Box box = MyApp.getInstance().getBoxnet();
        box.getAccountTree(authToken, folderId, new String[] {Box.PARAM_ONELEVEL}, new GetAccountTreeListener() {

            @Override
            public void onComplete(BoxFolder boxFolder, String status) {
                if (!status.equals(GetAccountTreeListener.STATUS_LISTING_OK)) {
                    Toast.makeText(getApplicationContext(), "There was an error.", Toast.LENGTH_SHORT).show();
                    finish();
                    return;
                }

                /**
                 * Box.getAccountTree() was successful. boxFolder contains a list of subfolders and files. Shove those into an array so that our list adapter
                 * displays them.
                 */

                items.clear();
                
                int i = 0;

                Iterator<? extends BoxFolder> foldersIterator = boxFolder.getFoldersInFolder().iterator();
                while (foldersIterator.hasNext()) {
                    BoxFolder subfolder = foldersIterator.next();
                    TreeListItem item = new TreeListItem();
                    item.id = subfolder.getId();
                    item.name = subfolder.getFolderName();
                    item.type = TreeListItem.TYPE_FOLDER;
                    item.folder = subfolder;
                    item.updated = subfolder.getUpdated();
                    items.add(item);
                    i++;
                }

                Iterator<? extends BoxFile> filesIterator = boxFolder.getFilesInFolder().iterator();
                while (filesIterator.hasNext()) {
                    BoxFile boxFile = filesIterator.next();
                    TreeListItem item = new TreeListItem();
                    item.id = boxFile.getId();
                    item.name = boxFile.getFileName();
                    item.type = TreeListItem.TYPE_FILE;
                    item.file = boxFile;
                    item.updated = boxFile.getUpdated();
                    items.add(item);
                    i++;
                }

                adapter.notifyDataSetChanged();
                ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onIOException(final IOException e) {
                Toast.makeText(getApplicationContext(), "Failed to get tree - " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
