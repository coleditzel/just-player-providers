package jp.co.kayo.android.localplayer.ds.boxnet.db;

import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioGenres;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylistMember;
import jp.co.kayo.android.localplayer.consts.MediaConsts.BoxnetFile;
import jp.co.kayo.android.localplayer.consts.MediaConsts.VideoMedia;
import jp.co.kayo.android.localplayer.util.Logger;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.provider.BaseColumns;

/***
 * このクラスはJustPlayerが独自に管理するデータへアクセスするためのクラスです
 * 
 * @author yokmama
 */
class BoxnetDatabaseHelper extends SQLiteOpenHelper {
    // データベースのバージョン（スキーマが変わったときにインクリメントする）
    private static final int DATABASE_VERSION = 3;

    /***
     * デフォルトのコンストラクタ
     * 
     * @param context
     * @param databasename
     */
    public BoxnetDatabaseHelper(Context context, String dbname) {
        super(context, dbname, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            // Albumのテーブルを生成
            if (!findTable(db, TableConsts.TBNAME_ALBUM)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_ALBUM)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(AudioAlbum.ALBUM).append(" TEXT");
                sql.append(",").append(AudioAlbum.ALBUM_KEY).append(" TEXT");
                sql.append(",").append(AudioAlbum.ALBUM_ART).append(" TEXT");
                sql.append(",").append(AudioAlbum.FIRST_YEAR).append(" TEXT");
                sql.append(",").append(AudioAlbum.LAST_YEAR).append(" TEXT");
                sql.append(",").append(AudioAlbum.NUMBER_OF_SONGS)
                        .append(" INTEGER");
                sql.append(",").append(AudioAlbum.ARTIST).append(" TEXT");
                sql.append(",").append(AudioAlbum.DATE_ADDED).append(" LONG");
                sql.append(",").append(AudioAlbum.DATE_MODIFIED)
                        .append(" LONG");
                sql.append(",").append(TableConsts.ALBUM_INIT_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.ALBUM_DEL_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.GENRES_TAGS).append(" TEXT");
                sql.append(",").append(TableConsts.FAVORITE_POINT)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // Artistのテーブルを生成
            if (!findTable(db, TableConsts.TBNAME_ARTIST)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_ARTIST)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(AudioArtist.ARTIST).append(" TEXT");
                sql.append(",").append(AudioArtist.ARTIST_KEY).append(" TEXT");
                sql.append(",").append(AudioArtist.NUMBER_OF_ALBUMS)
                        .append(" INTEGER");
                sql.append(",").append(AudioArtist.NUMBER_OF_TRACKS)
                        .append(" INTEGER");
                sql.append(",").append(AudioArtist.DATE_ADDED).append(" LONG");
                sql.append(",").append(AudioArtist.DATE_MODIFIED)
                        .append(" LONG");
                sql.append(",").append(TableConsts.ARTIST_INIT_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.ARTIST_DEL_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.GENRES_TAGS).append(" TEXT");
                sql.append(",").append(TableConsts.FAVORITE_POINT)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // Playlistのテーブルを生成
            if (!findTable(db, TableConsts.TBNAME_PLAYLIST)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_PLAYLIST)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(AudioPlaylist.NAME).append(" TEXT");
                sql.append(",").append(AudioPlaylist.DATE_ADDED)
                        .append(" LONG");
                sql.append(",").append(AudioPlaylist.PLAYLIST_KEY).append(" TEXT");
                sql.append(",").append(AudioPlaylist.DATE_MODIFIED)
                        .append(" LONG");
                sql.append(",").append(TableConsts.PLAYLIST_DEL_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.PLAYLIST_INIT_FLG)
                        .append(" INTEGER");
                sql.append(",").append(AudioPlaylist.DATA)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.GENRES_TAGS).append(" TEXT");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // PlaylistAudioのテーブルを生成
            if (!findTable(db, TableConsts.TBNAME_PLAYLIST_AUDIO)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ")
                        .append(TableConsts.TBNAME_PLAYLIST_AUDIO).append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(AudioPlaylistMember.AUDIO_ID)
                        .append(" LONG");
                sql.append(",").append(AudioPlaylistMember.PLAYLIST_ID)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.PLAY_ORDER)
                        .append(" INTEGER");
                sql.append(",").append(AudioPlaylistMember.MEDIA_KEY)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.TITLE)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.TITLE_KEY)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.DURATION)
                        .append(" LONG");
                sql.append(",").append(AudioPlaylistMember.ARTIST)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.ARTIST_KEY)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.ALBUM)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.ALBUM_KEY)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.DATA)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.TRACK)
                        .append(" INTEGER");
                sql.append(",").append(AudioPlaylistMember.YEAR)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.DATE_ADDED)
                        .append(" LONG");
                sql.append(",").append(AudioPlaylistMember.DATE_MODIFIED)
                        .append(" LONG");
                sql.append(",").append(TableConsts.GENRES_TAGS).append(" TEXT");
                sql.append(",").append(TableConsts.FAVORITE_POINT)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // Audioのテーブルを生成
            if (!findTable(db, TableConsts.TBNAME_AUDIO)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_AUDIO)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(AudioMedia.MEDIA_KEY).append(" TEXT");
                sql.append(",").append(AudioMedia.TITLE).append(" TEXT");
                sql.append(",").append(AudioMedia.TITLE_KEY).append(" TEXT");
                sql.append(",").append(AudioMedia.DURATION).append(" LONG");
                sql.append(",").append(AudioMedia.ARTIST).append(" TEXT");
                sql.append(",").append(AudioMedia.ARTIST_KEY).append(" TEXT");
                sql.append(",").append(AudioMedia.ALBUM).append(" TEXT");
                sql.append(",").append(AudioMedia.ALBUM_KEY).append(" TEXT");
                // sql.append(",").append(AudioMedia.ALBUM_ART).append(" TEXT");
                sql.append(",").append(AudioMedia.DATA).append(" TEXT");
                sql.append(",").append(AudioMedia.TRACK).append(" INTEGER");
                sql.append(",").append(AudioMedia.YEAR).append(" TEXT");
                sql.append(",").append(AudioMedia.DATE_ADDED).append(" LONG");
                sql.append(",").append(AudioMedia.DATE_MODIFIED)
                        .append(" LONG");
                sql.append(",").append(TableConsts.AUDIO_DEL_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.AUDIO_CACHE_FILE)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.GENRES_TAGS).append(" TEXT");
                sql.append(",").append(TableConsts.FAVORITE_POINT)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // orderlist
            if (!findTable(db, TableConsts.TBNAME_ORDERLIST)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ")
                        .append(TableConsts.TBNAME_ORDERLIST).append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(TableConsts.ORDER_TITLE).append(" TEXT");
                sql.append(",").append(TableConsts.ORDER_ARTIST)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.ORDER_ALBUM).append(" TEXT");
                sql.append(",").append(TableConsts.ORDER_MEDIA_ID)
                        .append(" LONG");
                sql.append(",").append(TableConsts.ORDER_ALBUM_KEY)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.ORDER_ARTIST_KEY)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.ORDER_DURATION)
                        .append(" LONG");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // video
            if (!findTable(db, TableConsts.TBNAME_VIDEO)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_VIDEO)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(VideoMedia.MEDIA_KEY).append(" TEXT");
                sql.append(",").append(VideoMedia.TITLE).append(" TEXT");
                sql.append(",").append(VideoMedia.MIME_TYPE).append(" TEXT");
                sql.append(",").append(VideoMedia.RESOLUTION).append(" TEXT");
                sql.append(",").append(VideoMedia.SIZE).append(" TEXT");
                sql.append(",").append(VideoMedia.DURATION).append(" LONG");
                sql.append(",").append(VideoMedia.DATE_ADDED).append(" LONG");
                sql.append(",").append(VideoMedia.DATE_MODIFIED)
                        .append(" LONG");
                sql.append(",").append(VideoMedia.DATA).append(" INTEGER");
                sql.append(",").append(TableConsts.VIDEO_INIT_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.VIDEO_DEL_FLG)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // download
            if (!findTable(db, TableConsts.TBNAME_DOWNLOAD)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_DOWNLOAD)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(TableConsts.DOWNLOAD_ID).append(" LONG");
                sql.append(",").append(TableConsts.DOWNLOAD_MEDIA_ID)
                        .append(" LONG");
                sql.append(",").append(TableConsts.DOWNLOAD_TITLE)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.DOWNLOAD_LOCAL_URI)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.DOWNLOAD_REMOTE_URI)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.DOWNLOAD_TYPE)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.DOWNLOAD_STATUS)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // Genresのテーブルを生成
            if (!findTable(db, TableConsts.TBNAME_GENRES)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_GENRES)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(AudioGenres.NAME).append(" TEXT");
                sql.append(",").append(AudioGenres.GENRES_KEY).append(" TEXT");
                sql.append(",").append(AudioGenres.DATE_ADDED).append(" LONG");
                sql.append(",").append(AudioGenres.DATE_MODIFIED)
                        .append(" LONG");
                sql.append(",").append(TableConsts.GENRES_INIT_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.GENRES_DEL_FLG)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // BoxnetFileのテーブルを生成
            if (!findTable(db, BoxnetFile.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(BoxnetFile.TBNAME)
                        .append(" (");
                sql.append(BoxnetFile._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(BoxnetFile.PARENTID).append(" LONG");
                sql.append(",").append(BoxnetFile.BOXNETID).append(" LONG");
                sql.append(",").append(BoxnetFile.TITLE).append(" TEXT");
                sql.append(",").append(BoxnetFile.TYPE).append(" INTEGER");
                sql.append(",").append(BoxnetFile.PATH).append(" TEXT");
                sql.append(",").append(BoxnetFile.SIZE).append(" LONG");
                sql.append(",").append(BoxnetFile.DATE_MODIFIED).append(" LONG");
                sql.append(");");
                db.execSQL(sql.toString());
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Logger.d("Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        if (oldVersion < 3) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_AUDIO);
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_ALBUM);
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_PLAYLIST);
            db.execSQL("DROP TABLE IF EXISTS "
                    + TableConsts.TBNAME_PLAYLIST_AUDIO);
            onCreate(db);
        }
        else if (oldVersion < 4) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_PLAYLIST);
            onCreate(db);
        } else {
            onCreate(db);
        }
    }

    public void rebuild(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_ALBUM);
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_ARTIST);
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_PLAYLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_PLAYLIST_AUDIO);
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_AUDIO);
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_ORDERLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_VIDEO);
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_GENRES);
        onCreate(db);
    }

    public void reset(SQLiteDatabase db) {
        db.execSQL("DELETE FROM " + TableConsts.TBNAME_ALBUM);
        db.execSQL("DELETE FROM " + TableConsts.TBNAME_ARTIST);
        db.execSQL("DELETE FROM " + TableConsts.TBNAME_PLAYLIST);
        db.execSQL("DELETE FROM " + TableConsts.TBNAME_PLAYLIST_AUDIO);
        db.execSQL("DELETE FROM " + TableConsts.TBNAME_AUDIO);
        db.execSQL("DELETE FROM " + TableConsts.TBNAME_ORDERLIST);
        db.execSQL("DELETE FROM " + TableConsts.TBNAME_VIDEO);
        db.execSQL("DELETE FROM " + TableConsts.TBNAME_GENRES);
    }

    public void rebuildSelect(SQLiteDatabase db, String[] projection) {
        if (containParams(projection, TableConsts.TBNAME_ALBUM)) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_ALBUM);
        }
        if (containParams(projection, TableConsts.TBNAME_ARTIST)) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_ARTIST);
        }
        if (containParams(projection, TableConsts.TBNAME_GENRES)) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_GENRES);
        }
        if (containParams(projection, TableConsts.TBNAME_AUDIO)) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_AUDIO);
        }
        if (containParams(projection, TableConsts.TBNAME_PLAYLIST)) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_PLAYLIST);
        }
        if (containParams(projection, TableConsts.TBNAME_PLAYLIST_AUDIO)) {
            db.execSQL("DROP TABLE IF EXISTS "
                    + TableConsts.TBNAME_PLAYLIST_AUDIO);
        }
        if (containParams(projection, TableConsts.TBNAME_VIDEO)) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_VIDEO);
        }
        if (containParams(projection, TableConsts.TBNAME_ORDERLIST)) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_ORDERLIST);
        }
        onCreate(db);
    }

    private boolean containParams(String[] tables, String checkTable) {
        for (String table : tables) {
            if (table == checkTable) {
                return true;
            }
        }
        return false;
    }

    public long getCount(SQLiteDatabase db, String tbl) {
        Cursor cur = null;
        try {
            if (findTable(db, tbl)) {
                cur = db.rawQuery("select count(*) from " + tbl, null);
                if (cur != null && cur.moveToFirst()) {
                    long count = cur.getLong(0);
                    return count;
                }
            }
            return 0;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public boolean findTable(SQLiteDatabase db, String tbl) {

        StringBuilder where = new StringBuilder();
        where.append("type='table' and name='");
        where.append(tbl);
        where.append("'");

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables("sqlite_master"); // テーブル名
        Cursor cur = null;
        try {
            cur = qb.query(db, null, where.toString(), null, null, null, null,
                    null);
            if (cur != null) {
                if (cur.moveToFirst()) {
                    return true;
                }
            }
            return false;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public int delete(String table, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            int ret = db.delete(table, selection, selectionArgs);
            db.setTransactionSuccessful();
            return ret;
        } finally {
            db.endTransaction();
        }
    }

    public long insert(String table, ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            long id = db.insert(table, null, values);
            db.setTransactionSuccessful();
            return id;
        } finally {
            db.endTransaction();
        }
    }

    public Cursor query(String table, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = getReadableDatabase();
        try {
            return db.query(table, projection, selection, selectionArgs, null,
                    null, sortOrder);
        } finally {
        }
    }

    public int update(String table, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            int ret = db.update(table, values, selection, selectionArgs);
            db.setTransactionSuccessful();
            return ret;
        } finally {
            db.endTransaction();
        }
    }
}
