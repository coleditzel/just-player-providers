package jp.co.kayo.android.localplayer.ds.boxnet;

import java.util.Calendar;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.BoxnetFile;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.ds.boxnet.db.BoxnetContentProvider;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.M3UParser;
import jp.co.kayo.android.localplayer.util.M3UParser.PlaylistEntry;
import android.content.ContentProviderClient;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.BaseColumns;
import android.provider.MediaStore;

import com.box.androidlib.DAO.BoxFile;
import com.box.androidlib.DAO.BoxFolder;
import com.box.androidlib.Utils.BoxConfig;

public class BoxnetHelper {
    public static final Uri BOXNET_CONTENT_AUTHORITY = Uri
            .parse("content://jp.co.kayo.android.localplayer.ds.boxnet");
    public static final Uri NOTIFY_ALBUM_CONTENT_URI = Uri
            .parse("content://jp.co.kayo.android.localplayer/audio/albums");
    public static final Uri NOTIFY_MEDIA_CONTENT_URI = Uri
            .parse("content://jp.co.kayo.android.localplayer/audio/media");
    public static final Uri NOTIFY_ARTIST_CONTENT_URI = Uri
            .parse("content://jp.co.kayo.android.localplayer/audio/artist");

    private SharedPreferences mPrefs;
    private String mAuthToken;
    private Context mContext;

    public BoxnetHelper(Context context) {
        mContext = context;
        mPrefs = context.getSharedPreferences(SystemConsts.PREFS_FILE_NAME, 0);
        mAuthToken = mPrefs.getString(SystemConsts.PREFS_KEY_AUTH_TOKEN, null);
    }

    public boolean isLoggdIn() {
        return true;
    }

    public String getAuthToken() {
        return mAuthToken;
    }

    public Context getContext() {
        return mContext;
    }

    public String getAuthrizedURL(String fileId) {
        final Uri.Builder builder = new Uri.Builder();
        builder.scheme(BoxConfig.getInstance().getDownloadUrlScheme());
        builder.encodedAuthority(BoxConfig.getInstance()
                .getDownloadUrlAuthority());
        builder.path(BoxConfig.getInstance().getDownloadUrlPath());
        builder.appendPath(mAuthToken);
        builder.appendPath(fileId);
        return builder.build().toString();
    }

    public BoxFileInfo findBoxFileInfo(String path) {
        Cursor cursor = null;
        try {
            cursor = getContext().getContentResolver().query(
                    BoxnetContentProvider.BOXNETFILE_CONTENT_URI,
                    new String[] { BoxnetFile._ID, BoxnetFile.PARENTID,
                            BoxnetFile.BOXNETID, BoxnetFile.TITLE,
                            BoxnetFile.TYPE, BoxnetFile.PATH, BoxnetFile.SIZE,
                            BoxnetFile.DATE_MODIFIED },
                    MediaConsts.BoxnetFile.PATH + " = ?",
                    new String[] { path }, null);
            if (cursor != null && cursor.moveToFirst()) {
                BoxFileInfo inf = new BoxFileInfo();
                inf._ID = cursor.getLong(0);
                inf.PARENTID = cursor.getLong(1);
                inf.BOXNETID = cursor.getLong(2);
                inf.TITLE = cursor.getString(3);
                inf.TYPE = cursor.getInt(4);
                inf.PATH = cursor.getString(5);
                inf.SIZE = cursor.getLong(6);
                inf.DATE_MODIFIED = cursor.getLong(7);
                return inf;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }
    
    public Uri registMetaInfo(FileMetaInfo metainf) {
        boolean ret;
        ContentProviderClient client = null;
        try {
            client = getContext().getContentResolver()
                    .acquireContentProviderClient(BOXNET_CONTENT_AUTHORITY);
            boolean hasRegist = isRegisterdSong(client, metainf.fileId);
            if (!hasRegist) {
                Logger.d("metainf.path=" + metainf.fileId);
                Logger.d("metainf.name=" + metainf.name);
                Logger.d("metainf.title=" + metainf.title);
                Logger.d("metainf.album=" + metainf.album);
                Logger.d("metainf.artist=" + metainf.artist);

                // アルバム情報の登録
                ret = isRegisterdAlbum(client, metainf.folderId);
                if (!ret) {
                    Uri uri = insertAlbum(client, metainf);
                    getContext().getContentResolver().notifyChange(
                            NOTIFY_ALBUM_CONTENT_URI, null);
                }
                // アーティスト情報の登録
                if (metainf.artist != null && metainf.artist.length() > 0) {
                    ret = isRegisterdArtist(client, metainf.artist.hashCode());
                    if (!ret) {
                        Uri uri = insertArtist(client, metainf);
                        getContext().getContentResolver().notifyChange(
                                NOTIFY_ARTIST_CONTENT_URI, null);
                    }
                }
                // 楽曲情報の登録
                Uri uri = insertSong(client, metainf);
                getContext().getContentResolver().notifyChange(
                        NOTIFY_MEDIA_CONTENT_URI, null);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (client != null) {
                client.release();
            }
        }
        return null;
    }

    public boolean isRegisterdAlbum(ContentProviderClient client, long id)
            throws RemoteException {
        // 有無をチェック
        Cursor cur = null;
        try {
            cur = client.query(MediaConsts.ALBUM_CONTENT_URI, new String[] {
                    BaseColumns._ID, AudioAlbum.ALBUM }, AudioAlbum.ALBUM_KEY
                    + " = ?", new String[] { Long.toString(id) }, null);
            if (cur != null && cur.moveToFirst()) {
                return true;
            }
            return false;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public boolean isRegisterdArtist(ContentProviderClient client, long id)
            throws RemoteException {
        // 有無をチェック
        Cursor cur = null;
        try {
            cur = client.query(MediaConsts.ARTIST_CONTENT_URI, new String[] {
                    BaseColumns._ID, AudioArtist.ARTIST },
                    AudioArtist.ARTIST_KEY + " = ?",
                    new String[] { Long.toString(id) }, null);
            if (cur != null && cur.moveToFirst()) {
                return true;
            }
            return false;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public boolean isRegisterdSong(ContentProviderClient client, long id)
            throws RemoteException {
        // 有無をチェック
        Cursor cur = null;
        try {
            cur = client.query(MediaConsts.MEDIA_CONTENT_URI,
                    new String[] { AudioMedia.MEDIA_KEY }, AudioMedia.MEDIA_KEY
                            + " = ?", new String[] { Long.toString(id) }, null);
            if (cur != null && cur.moveToFirst()) {
                return true;
            }
            return false;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public Uri insertAlbum(ContentProviderClient client, FileMetaInfo metainf)
            throws RemoteException {
        Calendar cal = Calendar.getInstance();
        Cursor cur = null;
        try {
            ContentValues values = new ContentValues();
            values.put(AudioAlbum.ALBUM_KEY, metainf.folderId);
            values.put(AudioAlbum.ALBUM, metainf.album);
            values.put(AudioAlbum.ALBUM_ART, metainf.albumArt);
            values.put(AudioAlbum.ARTIST, metainf.artist);
            values.put(AudioAlbum.FIRST_YEAR, "");
            values.put(AudioAlbum.LAST_YEAR, "");
            values.put(AudioAlbum.NUMBER_OF_SONGS, "0");
            values.put(AudioAlbum.DATE_ADDED, cal.getTimeInMillis());
            values.put(AudioAlbum.DATE_MODIFIED, cal.getTimeInMillis());
            values.put(AudioAlbum.DEL_FLG, 0);

            return client.insert(MediaConsts.ALBUM_CONTENT_URI, values);
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public Uri insertArtist(ContentProviderClient client, FileMetaInfo metainf)
            throws RemoteException {
        Calendar cal = Calendar.getInstance();
        Cursor cur = null;
        try {
            ContentValues values = new ContentValues();
            values.put(AudioArtist.ARTIST_KEY, metainf.artist.hashCode());
            values.put(AudioArtist.ARTIST, metainf.artist);
            values.put(AudioArtist.NUMBER_OF_ALBUMS, "0");
            values.put(AudioArtist.NUMBER_OF_TRACKS, "0");
            values.put(AudioArtist.DATE_ADDED, cal.getTimeInMillis());
            values.put(AudioArtist.DATE_MODIFIED, cal.getTimeInMillis());
            values.put(AudioAlbum.DEL_FLG, 0);

            return client.insert(MediaConsts.ARTIST_CONTENT_URI, values);
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public Uri insertSong(ContentProviderClient client, FileMetaInfo metainf)
            throws RemoteException {
        Calendar cal = Calendar.getInstance();
        Cursor cur = null;
        try {
            ContentValues values = new ContentValues();

            values.put(AudioMedia.MEDIA_KEY, metainf.fileId);
            values.put(AudioMedia.TITLE, metainf.title);
            values.put(AudioMedia.TITLE_KEY, metainf.title.hashCode());
            values.put(AudioMedia.DURATION, metainf.duration);
            if (metainf.artist != null && metainf.artist.length() > 0) {
                values.put(AudioMedia.ARTIST, metainf.artist);
                values.put(AudioMedia.ARTIST_KEY, metainf.artist.hashCode());
            }
            if (metainf.album != null && metainf.album.length() > 0) {
                values.put(AudioMedia.ALBUM, metainf.album);
            } else {
                values.put(AudioMedia.ALBUM, "unknown");
            }
            values.put(AudioMedia.ALBUM_KEY, metainf.folderId);
            // values.put(AudioMedia.ALBUM_ART, "");
            values.put(AudioMedia.DATA, metainf.fileId);
            if (metainf.track == null || metainf.track.length() == 0) {
                values.put(AudioMedia.TRACK, "9999");
            } else {
                values.put(AudioMedia.TRACK, metainf.track);
            }
            values.put(AudioMedia.DATE_ADDED, cal.getTimeInMillis());
            values.put(AudioMedia.DATE_MODIFIED, cal.getTimeInMillis());
            values.put(AudioMedia.YEAR, metainf.year);
            if (metainf.cacheFile != null) {
                values.put(AudioMedia.AUDIO_CACHE_FILE, metainf.cacheFile);
            }

            Uri uri = client.insert(MediaConsts.MEDIA_CONTENT_URI, values);

            Logger.d("inserted song ="+metainf.title+" "+ContentUris.parseId(uri) + " "+metainf.fileId);
            
            return uri;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public BoxFileInfo findBoxFileInfo(ContentProviderClient client,
            long folderId) {
        Cursor cursor = null;
        try {
            cursor = client.query(MediaConsts.BOXNETFILE_CONTENT_URI, new String[] {
                    BoxnetFile._ID, BoxnetFile.PARENTID, BoxnetFile.BOXNETID, BoxnetFile.TITLE,
                    BoxnetFile.TYPE, BoxnetFile.PATH, BoxnetFile.SIZE,
                    BoxnetFile.DATE_MODIFIED }, MediaConsts.BoxnetFile.BOXNETID
                    + " = ?", new String[] { Long.toString(folderId) }, null);
            if (cursor != null && cursor.moveToFirst()) {
                BoxFileInfo inf = new BoxFileInfo();
                inf._ID = cursor.getLong(0);
                inf.PARENTID = cursor.getLong(1);
                inf.BOXNETID = cursor.getLong(2);
                inf.TITLE = cursor.getString(3);
                inf.TYPE = cursor.getInt(4);
                inf.PATH = cursor.getString(5);
                inf.SIZE = cursor.getLong(6);
                inf.DATE_MODIFIED = cursor.getLong(7);
                return inf;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }
    
    
    public void registerBoxFileInfo(ContentProviderClient client, String folderPath, BoxFolder boxFolder) {
        Logger.d("register Box path ="+folderPath);
        ContentValues value = new ContentValues();
        value.put(BoxnetFile.PARENTID, boxFolder.getParentFolderId());
        value.put(BoxnetFile.BOXNETID, boxFolder.getId());
        value.put(BoxnetFile.TITLE, boxFolder.getFolderName());
        value.put(BoxnetFile.TYPE, 1);
        value.put(BoxnetFile.PATH, folderPath);
        value.put(BoxnetFile.SIZE, boxFolder.getSize());
        value.put(BoxnetFile.DATE_MODIFIED, boxFolder.getUpdated());
        try {
            client.insert(MediaConsts.BOXNETFILE_CONTENT_URI, value);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void updateBoxFileInfo(ContentProviderClient client, String folderPath, BoxFolder boxFolder) {
        Logger.d("update Box path ="+folderPath);
        ContentValues value = new ContentValues();
        value.put(BoxnetFile.PARENTID, boxFolder.getParentFolderId());
        value.put(BoxnetFile.TITLE, boxFolder.getFolderName());
        value.put(BoxnetFile.PATH, folderPath);
        value.put(BoxnetFile.SIZE, boxFolder.getSize());
        value.put(BoxnetFile.DATE_MODIFIED, boxFolder.getUpdated());
        try {
            client.update(MediaConsts.BOXNETFILE_CONTENT_URI, value, BoxnetFile.BOXNETID
                    + " = ?", new String[] { Long.toString(boxFolder.getId()) });
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    
    public void registerBoxFileInfo(ContentProviderClient client, String folderPath, BoxFile boxFolder) {
        Logger.d("register BoxFile path ="+folderPath+"/"+boxFolder.getFileName());
        ContentValues value = new ContentValues();
        value.put(BoxnetFile.PARENTID, boxFolder.getFolderId());
        value.put(BoxnetFile.BOXNETID, boxFolder.getId());
        value.put(BoxnetFile.TITLE, boxFolder.getFileName());
        value.put(BoxnetFile.TYPE, 0);
        value.put(BoxnetFile.PATH, folderPath+"/"+boxFolder.getFileName());
        value.put(BoxnetFile.SIZE, boxFolder.getSize());
        value.put(BoxnetFile.DATE_MODIFIED, boxFolder.getUpdated());
        try {
            client.insert(MediaConsts.BOXNETFILE_CONTENT_URI, value);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    
    public void updateBoxFileInfo(ContentProviderClient client, String folderPath, BoxFile boxFolder) {
        Logger.d("update BoxFile path ="+folderPath+"/"+boxFolder.getFileName());
        ContentValues value = new ContentValues();
        value.put(BoxnetFile.PARENTID, boxFolder.getFolderId());
        value.put(BoxnetFile.TITLE, boxFolder.getFileName());
        value.put(BoxnetFile.PATH, folderPath+"/"+boxFolder.getFileName());
        value.put(BoxnetFile.SIZE, boxFolder.getSize());
        value.put(BoxnetFile.DATE_MODIFIED, boxFolder.getUpdated());
        try {
            client.update(MediaConsts.BOXNETFILE_CONTENT_URI, value, BoxnetFile.BOXNETID
                    + " = ?", new String[] { Long.toString(boxFolder.getId()) });
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    
    public Uri registPlaylistInfo(ContentProviderClient client, PlaylistMetaInfo metainf) {
        long playlsitId;
        // 楽曲情報の登録
        playlsitId = findRegisterdPlaylist(client, metainf);
        if (playlsitId == -1) {
            insertPlaylist(client, metainf);
            Logger.d("inserted playlist ="+playlsitId);
        } else {
            Logger.d("inserted playlist ="+playlsitId);
            updatePlaylist(client, playlsitId, metainf);
        }
        return null;
    }
    
    public long findRegisterdPlaylist(ContentProviderClient client, PlaylistMetaInfo metainf) {
        // 有無をチェック
        Cursor cur = null;
        try {
            cur = client.query(
                    MediaConsts.PLAYLIST_CONTENT_URI,
                    new String[] {AudioPlaylist._ID},
                    AudioPlaylist.PLAYLIST_KEY + " = ?",
                    new String[] {
                            Long.toString(metainf.fileId)
                    }, null);
            if (cur != null && cur.moveToFirst()) {
                return cur.getLong(0);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return -1;
    }
    
    private long findAudioId(ContentProviderClient client, String path, String fname) {
        Logger.d("find Audio ="+ path + "/" + fname);
        Cursor cursor = null;
        try{
            cursor = client.query(
                    MediaConsts.BOXNETFILE_CONTENT_URI, 
                    new String[]{BoxnetFile.BOXNETID}, 
                    BoxnetFile.PATH + " = ?",
                    new String[]{path + "/" + fname}, null);
            if(cursor!=null && cursor.moveToFirst()){
                long boxnet_id = cursor.getLong(0);
                cursor.close();
                cursor = client.query(
                        MediaConsts.MEDIA_CONTENT_URI, 
                        new String[]{AudioMedia._ID}, 
                        AudioMedia.MEDIA_KEY + " = ?",
                        new String[]{Long.toString(boxnet_id)}, null);
                if(cursor!=null && cursor.moveToFirst()){
                    long id = cursor.getLong(0);
                    Logger.d("found Audio ="+ id);
                    return id;
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        finally{
            if(cursor!=null){
                cursor.close();
            }
        }
        return -1;
    }
    
    public void insertPlaylist(ContentProviderClient client, PlaylistMetaInfo metainf) {
        Calendar cal = Calendar.getInstance();


        M3UParser parser = new M3UParser(this);
        Cursor cur = null;
        try {
            ContentValues values = new ContentValues();
            values.put(AudioPlaylist.NAME,parser.getPlaylistName(metainf.name));
            values.put(AudioPlaylist.PLAYLIST_KEY, Long.toString(metainf.fileId));
            values.put(AudioPlaylist.DATA, metainf.filepath);
            values.put(AudioPlaylist.DATE_ADDED, cal.getTimeInMillis());
            values.put(AudioPlaylist.DATE_MODIFIED, cal.getTimeInMillis());
            
            Uri insertedUri = client.insert(MediaConsts.PLAYLIST_CONTENT_URI, values);
            long playlistId = ContentUris.parseId(insertedUri);
            Uri insertUri = ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI, playlistId);
            
            parser.processPlayList(metainf);

            int index = 0;
            for (PlaylistEntry entry : parser.getPlaylistEntries()) {
                long audioId = findAudioId(client, metainf.folder, entry.fname);
                if(audioId != -1){
                    ContentValues values2 = new ContentValues();
                    values2.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, audioId);
                    values2.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, index + 1);
                    client.insert(insertUri, values2);
                    index++;
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }
    
    public void updatePlaylist(ContentProviderClient client, long playlistId, PlaylistMetaInfo metainf) {
        Uri deleteUri = ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI, playlistId);
        try {
            client.delete(deleteUri, null, null);
            insertPlaylist(client, metainf);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
