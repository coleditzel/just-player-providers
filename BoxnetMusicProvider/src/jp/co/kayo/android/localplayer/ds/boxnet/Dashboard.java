//  Copyright 2011 Box.net.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

package jp.co.kayo.android.localplayer.ds.boxnet;

import java.io.IOException;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.secret.Keys;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.box.androidlib.Box;
import com.box.androidlib.ResponseListeners.LogoutListener;

public class Dashboard extends Activity {

    private String authToken;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        final SharedPreferences prefs = getSharedPreferences(SystemConsts.PREFS_FILE_NAME, 0);
        authToken = prefs.getString(SystemConsts.PREFS_KEY_AUTH_TOKEN, null);
        if (authToken == null) {
            Toast.makeText(getApplicationContext(), "You are not logged in.", Toast.LENGTH_SHORT)
                .show();
            finish();
            return;
        }
        
        //Box.netをデフォルトにする
        ContentValues values = new ContentValues();
        values.put("key_current_uri", MediaConsts.BOXNET_AUTHORITY);
        getContentResolver().update(Uri.parse("content://jp.co.kayo.android.localplayer/pref"), values, null, null);

        final Button logoutButton = (Button) findViewById(R.id.logoutButton);
        final Button fullsyncButton = (Button) findViewById(R.id.fullsyncButton);
        final Button syncButton = (Button) findViewById(R.id.syncButton);
        final Button remoteButton = (Button) findViewById(R.id.remoteButton);
        final Button localButton = (Button) findViewById(R.id.localButton);
        final Button confButton = (Button) findViewById(R.id.confButton);
        
        syncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Dashboard.this, MediaScanner.class);
                i.setAction("sync");
                startService(i);
            }
        });
        
        fullsyncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getContentResolver().query(MediaConsts.RESET_CONTENT_URI, null, null, null, null);
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(Dashboard.this);
                Editor editor = pref.edit();
                editor.putInt("mediaSum", -1);
                editor.commit();
            }
        });

        remoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Dashboard.this, RemoteFileBrowser.class);
                startActivity(i);
            }
        });

        localButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Dashboard.this, LocalFileBrowser.class);
                startActivity(i);
            }
        });
        
        confButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Dashboard.this, PrefActivity.class);
                startActivity(i);
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApp.getInstance().getBoxnet().logout(authToken, new LogoutListener() {

                    @Override
                    public void onIOException(IOException e) {
                        Toast.makeText(getApplicationContext(),
                            "Logout failed - " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onComplete(String status) {
                        if (status.equals(LogoutListener.STATUS_LOGOUT_OK)) {
                            // Delete stored auth token and send user back to
                            // splash page
                            final SharedPreferences prefs = getSharedPreferences(
                                SystemConsts.PREFS_FILE_NAME, 0);
                            final SharedPreferences.Editor editor = prefs.edit();
                            editor.remove(SystemConsts.PREFS_KEY_AUTH_TOKEN);
                            editor.commit();
                            Toast
                                .makeText(getApplicationContext(), "Logged out", Toast.LENGTH_LONG)
                                .show();
                            Intent i = new Intent(Dashboard.this, MainActivity.class);
                            startActivity(i);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), "Logout failed - " + status,
                                Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

    }
}
