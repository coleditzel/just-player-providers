package jp.co.kayo.android.localplayer.ds.boxnet.db;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.ds.boxnet.ArrayUtils;
import jp.co.kayo.android.localplayer.ds.boxnet.BoxFileInfo;
import jp.co.kayo.android.localplayer.ds.boxnet.BoxnetHelper;
import jp.co.kayo.android.localplayer.ds.boxnet.Funcs;
import jp.co.kayo.android.localplayer.ds.boxnet.MediaScanner;
import jp.co.kayo.android.localplayer.ds.boxnet.MyApp;
import jp.co.kayo.android.localplayer.ds.boxnet.PlaylistMetaInfo;
import jp.co.kayo.android.localplayer.ds.boxnet.StrictHelper;
import jp.co.kayo.android.localplayer.secret.Keys;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.M3UParser;
import jp.co.kayo.android.localplayer.util.M3UParser.PlaylistEntry;
import jp.co.kayo.android.localplayer.util.SdCardAccessHelper;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.provider.MediaStore;

import com.box.androidlib.Box;
import com.box.androidlib.BoxSynchronous;
import com.box.androidlib.DAO.BoxFile;
import com.box.androidlib.DAO.BoxFolder;
import com.box.androidlib.ResponseListeners.GetAccountTreeListener;
import com.box.androidlib.ResponseParsers.AccountTreeResponseParser;

/***
 * このクラスはMediaProvider（端末の音楽データ）のラッパーです
 * 
 * @author yokmama
 */
public class BoxnetContentProvider extends ContentProvider implements
        MediaConsts {
    public static final Uri BOXNETFILE_CONTENT_URI = Uri.parse(BOXNET_AUTHORITY
            + "config/boxnetfile");

    private UriMatcher uriMatcher = null;

    private SharedPreferences mPreference;
    private BoxnetDatabaseHelper dbhelper;
    private BoxnetHelper boxnetHelper;
    private ConnectivityManager mConnectionMgr;
    private String[] mediaextensions = null;
    private String[] playlistextensions = null;

    public final String FAVORITE_ID = "media_id";
    public final String FAVORITE_TYPE = "type";
    public final String FAVORITE_POINT = "point";
    public final String FAVORITE_TYPE_SONG = "song";
    public final String FAVORITE_TYPE_ALBUM = "album";
    public final String FAVORITE_TYPE_ARTIST = "artist";

    public static final String[] MEDIA_FIELDS = new String[] {
            AudioMedia._ID,
            AudioMedia.TITLE, AudioMedia.MEDIA_KEY, AudioMedia.TITLE_KEY,
            AudioMedia.DURATION, AudioMedia.DATA, AudioMedia.ARTIST,
            AudioMedia.ARTIST_KEY, AudioMedia.ALBUM,
            AudioMedia.ALBUM_KEY,
            // AudioMedia.ALBUM_ART,
            AudioMedia.TRACK, AudioMedia.YEAR, AudioMedia.DATE_ADDED,
            AudioMedia.DATE_MODIFIED, TableConsts.AUDIO_CACHE_FILE,
            TableConsts.GENRES_TAGS, TableConsts.FAVORITE_POINT
    };

    static final String[] audio_cols = new String[] {
            BaseColumns._ID,
            AudioMedia.TITLE, AudioMedia.TITLE_KEY, AudioMedia.DATA,
            AudioMedia.DURATION, AudioMedia.ARTIST, AudioMedia.ARTIST_KEY,
            AudioMedia.ALBUM, AudioMedia.ALBUM_KEY,
            // AudioMedia.ALBUM_ART,
            AudioMedia.TRACK
    };

    static final String[] playlist_audio_cols = new String[] {
            BaseColumns._ID,
            AudioPlaylistMember.AUDIO_ID, AudioMedia.TITLE,
            AudioMedia.TITLE_KEY, AudioMedia.DATA, AudioMedia.DURATION,
            AudioMedia.ARTIST, AudioMedia.ARTIST_KEY, AudioMedia.ALBUM,
            AudioMedia.ALBUM_KEY,
            // AudioMedia.ALBUM_ART,
            AudioMedia.TRACK
    };

    static final String[] genres_audio_cols = new String[] {
            BaseColumns._ID,
            AudioGenresMember.AUDIO_ID, AudioMedia.TITLE, AudioMedia.TITLE_KEY,
            AudioMedia.DATA, AudioMedia.DURATION, AudioMedia.ARTIST,
            AudioMedia.ARTIST_KEY, AudioMedia.ALBUM, AudioMedia.ALBUM_KEY,
            // AudioMedia.ALBUM_ART,
            AudioMedia.TRACK,
    };

    final String[] MEDIA_PROJECTION = new String[] {
            AudioMedia._ID,
            AudioMedia.ALBUM, AudioMedia.ALBUM_KEY, AudioMedia.ARTIST,
            AudioMedia.TITLE, AudioMedia.DURATION, AudioMedia.DATA
    };

    private synchronized BoxnetHelper getBoxnetHelper() {
        if (boxnetHelper == null) {
            boxnetHelper = new BoxnetHelper(getContext()
                    .getApplicationContext());
        }

        return boxnetHelper;
    }

    private boolean isWifiConnected() {
        if (mConnectionMgr == null) {
            mConnectionMgr = (ConnectivityManager) getContext()
                    .getApplicationContext().getSystemService(
                            Context.CONNECTIVITY_SERVICE);
        }
        NetworkInfo info = mConnectionMgr.getActiveNetworkInfo();
        if (info != null) {
            return ((info.getType() == ConnectivityManager.TYPE_WIFI) && (info
                    .isConnected()));
        }

        return false;
    }

    private boolean canUseNetwork() {
        if (mPreference.getBoolean(SystemConsts.CNF_WIFI_ONLY, true)) {
            return isWifiConnected();
        } else {
            return true;
        }
    }

    private synchronized BoxnetDatabaseHelper getDBHelper() {
        if (dbhelper == null) {
            if (dbhelper != null) {
                dbhelper.close();
                dbhelper = null;
            }
            dbhelper = new BoxnetDatabaseHelper(getContext(), "dropbox.db");
            // アップグレードのため一旦書込でひらいておく
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            if (db != null) {
                db.close();
                db = null;
            }
        }
        return dbhelper;
    }

    @Override
    public boolean onCreate() {
        mPreference = PreferenceManager
                .getDefaultSharedPreferences(getContext()
                        .getApplicationContext());

        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/media", CODE_MEDIA);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/media/#", CODE_MEDIA_ID);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/albums", CODE_ALBUMS);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/albums/#", CODE_ALBUMS_ID);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/artist", CODE_ARTIST);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/artist/#", CODE_ARTIST_ID);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/albumart", AUDIO_ALBUMART);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/albumart/#",
                AUDIO_ALBUMART_ID);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/media/#/albumart",
                AUDIO_ALBUMART_FILE_ID);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/playlist", CODE_PLAYLIST);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/playlist/#",
                CODE_PLAYLIST_ID);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/playlistmember",
                CODE_PLAYLISTMEMBER);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/playlistmember/#",
                CODE_PLAYLISTMEMBER_ID);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/favorite", CODE_FAVORITE);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/favorite/#",
                CODE_FAVORITE_ID);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/file", CODE_FILE);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/file/#", CODE_FILE_ID);
        uriMatcher.addURI(BOXNET_AUTHORITY, "video/media", CODE_VIDEO);
        uriMatcher.addURI(BOXNET_AUTHORITY, "video/media/#", CODE_VIDEO_ID);
        uriMatcher.addURI(BOXNET_AUTHORITY, "order/audio", CODE_ORDER_AUDIO);
        uriMatcher.addURI(BOXNET_AUTHORITY, "order/audio/#",
                CODE_ORDER_AUDIO_ID);
        uriMatcher.addURI(BOXNET_AUTHORITY, "download/media", CODE_DOWNLOAD);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/genres", CODE_GENRES);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/genres/#", CODE_GENRES_ID);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/genresmember",
                CODE_GENRESMEMBER);
        uriMatcher.addURI(BOXNET_AUTHORITY, "audio/genresmember/#",
                CODE_GENRESMEMBER_ID);
        uriMatcher.addURI(BOXNET_AUTHORITY, "config/auth", CODE_AUTH);
        uriMatcher.addURI(BOXNET_AUTHORITY, "config/clear", CODE_CLEAR);
        uriMatcher.addURI(BOXNET_AUTHORITY, "config/reset", CODE_RESET);
        uriMatcher.addURI(BOXNET_AUTHORITY, "config/ping", CODE_PING);
        uriMatcher.addURI(BOXNET_AUTHORITY, "config/url", CODE_URL);
        uriMatcher.addURI(BOXNET_AUTHORITY, "config/boxnetfile",
                CODE_BOXNETFILE);
        uriMatcher.addURI(BOXNET_AUTHORITY, "config/boxnetfile/#",
                CODE_BOXNETFILE_ID);

        return true;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case CODE_ORDER_AUDIO: {
                return deleteOrderAudio(uri, selection, selectionArgs);
            }
            case CODE_PLAYLIST: {
                return deletePlaylist(uri, selection, selectionArgs);
            }
            case CODE_PLAYLIST_ID: {
                return deletePlaylistmember(uri, selection, selectionArgs);
            }
            case CODE_ALBUMS: {
                return deleteAlbums(uri, selection, selectionArgs);
            }
            case CODE_ARTIST: {
                return deleteArtist(uri, selection, selectionArgs);
            }
            case CODE_MEDIA: {
                return deleteMedia(uri, selection, selectionArgs);
            }
            case CODE_VIDEO: {
                return deleteVideo(uri, selection, selectionArgs);
            }
            case CODE_BOXNETFILE: {
                return getDBHelper().delete(BoxnetFile.TBNAME, selection,
                        selectionArgs);
            }
            case CODE_BOXNETFILE_ID: {
                long id = ContentUris.parseId(uri);
                return getDBHelper()
                        .delete(BoxnetFile.TBNAME, BoxnetFile._ID + " = ?",
                                new String[] {
                                    Long.toString(id)
                                });
            }
        }
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case CODE_MEDIA: {
                return insertMedia(uri, values);
            }
            case CODE_ALBUMS: {
                return insertAlbum(uri, values);
            }
            case CODE_ARTIST: {
                return insertArtist(uri, values);
            }
            case CODE_PLAYLIST: {
                return insertPlaylist(uri, values);
            }
            case CODE_PLAYLIST_ID: {
                return insertPlaylistmember(uri, values);
            }
            case CODE_FAVORITE: {
                return insertFavorite(uri, values);
            }
            case CODE_VIDEO: {
                return insertVideo(uri, values);
            }
            case CODE_ORDER_AUDIO: {
                return insertOrderAudio(uri, values);
            }
            case CODE_DOWNLOAD: {
                return insertDownload(uri, values);
            }
            case CODE_GENRES: {
                return insertGenres(uri, values);
            }
            case CODE_GENRES_ID: {
                return genresmemberInsert(uri, values);
            }
            case CODE_BOXNETFILE: {
                return ContentUris.withAppendedId(BOXNETFILE_CONTENT_URI,
                        getDBHelper().insert(BoxnetFile.TBNAME, values));
            }
        }
        return null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        // ここでやるべきこと
        // JustPlayer内で定義された各フィールドの名前、これはSDKのMediaProviderと異なる可能性があるので、
        // 変換して検索を行い、また画像データやふりがなは独自のデータベースから取得するようにする
        if (Build.VERSION.SDK_INT > 10) {
            StrictHelper.registStrictMode();
        }

        int match = uriMatcher.match(uri);
        switch (match) {
            case CODE_MEDIA: {
                return queryMedia(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_MEDIA_ID: {
                return queryMediaId(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_ARTIST: {
                // 移植済
                return queryArtist(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_PLAYLIST: {
                // 移植済
                return queryPlaylist(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_PLAYLIST_ID: {
                return queryPlaylistmember(uri, projection, selection,
                        selectionArgs, sortOrder);
            }
            case CODE_FAVORITE: {
                // 移植済
                return queryFavorite(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_VIDEO: {
                // 移植済
                return queryVideo(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_VIDEO_ID: {
                return queryVideoId(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_ALBUMS: {
                // 移植済
                return queryAlbum(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_GENRES: {
                // 移植済
                return queryGenres(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_GENRESMEMBER: {
                return queryGenresmember(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_ORDER_AUDIO: {
                return queryOrderAudio(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_ORDER_AUDIO_ID: {
                return queryOrderAudioId(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_DOWNLOAD: {
                return queryDownload(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_PING: {
                boolean b = getBoxnetHelper().isLoggdIn();
                MatrixCursor cursor = null;
                if (b == true) {
                    cursor = new MatrixCursor(new String[] {
                            Auth._ID,
                            Auth.AUTH_KEY
                    });
                    cursor.addRow(new Object[] {
                            0, "hassession"
                    });

                    // 更新処理
                    Intent i = new Intent(getContext(), MediaScanner.class);
                    i.setAction("sync");
                    getContext().startService(i);
                }
                return cursor;
            }
            case CODE_AUTH:
            case CODE_URL: {
                MatrixCursor cursor = new MatrixCursor(new String[] {
                        Auth._ID,
                        Auth.AUTH_URL
                });
                String url = getBoxnetHelper().getAuthrizedURL(selectionArgs[0]);
                cursor.addRow(new Object[] {
                        0, url
                });
                return cursor;
            }
            case CODE_RESET: {
                SQLiteDatabase db = getDBHelper().getWritableDatabase();
                db.execSQL("DELETE FROM " + TableConsts.TBNAME_ALBUM);
                db.execSQL("DELETE FROM " + TableConsts.TBNAME_ARTIST);
                db.execSQL("DELETE FROM " + TableConsts.TBNAME_AUDIO);
                db.execSQL("DELETE FROM " + TableConsts.TBNAME_VIDEO);
                Cursor cursor = null, cursor2 = null;
                try {
                    cursor = db.query(TableConsts.TBNAME_PLAYLIST, new String[] {
                            AudioPlaylist._ID
                    }, null, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        do {
                            long playlistId = cursor.getLong(0);
                            cursor2 = db.rawQuery("select count(*) from "
                                    + TableConsts.TBNAME_PLAYLIST_AUDIO + " where "
                                    + AudioPlaylistMember.PLAYLIST_ID + " = ?", new String[] {
                                    Long.toString(playlistId)
                            });
                            if (cursor2 != null && cursor2.moveToFirst()) {
                                long count = cursor2.getLong(0);
                                if (count == 0) {
                                    db.delete(TableConsts.TBNAME_PLAYLIST,
                                            AudioPlaylist._ID + " = ?", new String[] {
                                                Long.toString(playlistId)
                                            });
                                }
                                cursor2.close();
                            }
                        } while (cursor.moveToNext());
                    }
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
                break;
            case CODE_FILE: {
                return queryFile(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_FILE_ID: {
                return queryFileId(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_BOXNETFILE: {
                return getDBHelper().query(BoxnetFile.TBNAME, projection,
                        selection, selectionArgs, sortOrder);
            }
            case CODE_BOXNETFILE_ID: {
                long id = ContentUris.parseId(uri);
                return getDBHelper().query(BoxnetFile.TBNAME, projection,
                        BoxnetFile._ID + " = ?",
                        new String[] {
                            Long.toString(id)
                        }, sortOrder);
            }
        }
        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case CODE_MEDIA: {
                return updateMedia(uri, values, selection, selectionArgs);
            }
            case CODE_MEDIA_ID: {
                return mediaIdUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_ALBUMS: {
                return updateAlbum(uri, values, selection, selectionArgs);
            }
            case CODE_ALBUMS_ID: {
                return albumIdUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_ARTIST: {
                return updateArtist(uri, values, selection, selectionArgs);
            }
            case CODE_ARTIST_ID: {
                return updateArtistId(uri, values, selection, selectionArgs);
            }
            case CODE_PLAYLIST: {
                return updatePlaylist(uri, values, selection, selectionArgs);
            }
            case CODE_PLAYLISTMEMBER: {
                return updatePlaylistmember(uri, values, selection, selectionArgs);
            }
            case CODE_FAVORITE: {
                return updateFavorite(uri, values, selection, selectionArgs);
            }
            case CODE_VIDEO: {
                return updateVideo(uri, values, selection, selectionArgs);
            }
            case CODE_VIDEO_ID: {
                return updateVideoId(uri, values, selection, selectionArgs);
            }
            case CODE_DOWNLOAD: {
                return updateDownload(uri, values, selection, selectionArgs);
            }
            case CODE_GENRES: {
                return updateGenres(uri, values, selection, selectionArgs);
            }
            case CODE_BOXNETFILE: {
                return getDBHelper().update(BoxnetFile.TBNAME, values, selection,
                        selectionArgs);
            }
            case CODE_BOXNETFILE_ID: {
                long id = ContentUris.parseId(uri);
                return getDBHelper()
                        .update(BoxnetFile.TBNAME, values, BoxnetFile._ID + " = ?",
                                new String[] {
                                    Long.toString(id)
                                });
            }
        }
        return 0;
    }

    private int updateArtistId(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        Cursor cur = null;
        try {
            long id = ContentUris.parseId(uri);
            db.beginTransaction();
            db.update(TableConsts.TBNAME_ARTIST, values, AudioArtist._ID
                    + " = ?", new String[] {
                    Long.toString(id)
            });

            cur = db.query(TableConsts.TBNAME_ARTIST,
                    new String[] {
                        AudioArtist.ARTIST_KEY
                    }, AudioArtist._ID
                            + " = ?", new String[] {
                        Long.toString(id)
                    }, null,
                    null, null);
            if (cur != null && cur.moveToFirst()) {
                String artistKey = cur.getString(cur
                        .getColumnIndex(AudioArtist.ARTIST_KEY));
                ContentValues audioValues = new ContentValues();
                if (values.containsKey(TableConsts.ARTIST_INIT_FLG)
                        && values.getAsInteger(TableConsts.ARTIST_INIT_FLG) == 0) {
                    // Mediaのキャッシュファイルを削除
                    audioValues
                            .put(TableConsts.AUDIO_CACHE_FILE, (String) null);
                } else {
                    if (values.containsKey(AudioArtist.ARTIST)) {
                        audioValues.put(AudioMedia.ARTIST,
                                values.getAsString(AudioAlbum.ARTIST));
                    }
                }
                if (audioValues.size() > 0) {
                    db.update(TableConsts.TBNAME_AUDIO, audioValues,
                            AudioMedia.ARTIST_KEY + " = ?",
                            new String[] {
                                artistKey
                            });
                }
            }

            db.setTransactionSuccessful();
            return 1;
        } finally {
            if (cur != null) {
                cur.close();
            }
            db.endTransaction();
            getContext().getContentResolver().notifyChange(MEDIA_CONTENT_URI,
                    null);
            getContext().getContentResolver().notifyChange(ARTIST_CONTENT_URI,
                    null);
        }
    }

    private int albumIdUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        Cursor cur = null;
        try {
            long id = ContentUris.parseId(uri);
            db.beginTransaction();
            db.update(TableConsts.TBNAME_ALBUM, values,
                    AudioAlbum._ID + " = ?", new String[] {
                        Long.toString(id)
                    });

            // 関連する音楽の更新
            cur = db.query(TableConsts.TBNAME_ALBUM,
                    new String[] {
                        AudioAlbum.ALBUM_KEY
                    }, AudioAlbum._ID
                            + " = ?", new String[] {
                        Long.toString(id)
                    }, null,
                    null, null);
            if (cur != null && cur.moveToFirst()) {
                String albumKey = cur.getString(cur
                        .getColumnIndex(AudioAlbum.ALBUM_KEY));
                ContentValues audioValues = new ContentValues();
                if (values.containsKey(TableConsts.ALBUM_INIT_FLG)
                        && values.getAsInteger(TableConsts.ALBUM_INIT_FLG) == 0) {
                    // Albumの初期化フラグをOff
                    audioValues
                            .put(TableConsts.AUDIO_CACHE_FILE, (String) null);
                } else {
                    if (values.containsKey(AudioAlbum.ALBUM)) {
                        audioValues.put(AudioMedia.ALBUM,
                                values.getAsString(AudioAlbum.ALBUM));
                    }
                    if (values.containsKey(AudioAlbum.ARTIST)) {
                        audioValues.put(AudioMedia.ARTIST,
                                values.getAsString(AudioAlbum.ARTIST));
                    }
                }
                if (audioValues.size() > 0) {
                    db.update(TableConsts.TBNAME_AUDIO, audioValues,
                            AudioMedia.ALBUM_KEY + " = ?",
                            new String[] {
                                albumKey
                            });
                }
            }

            db.setTransactionSuccessful();
            return 1;
        } finally {
            if (cur != null) {
                cur.close();
            }
            db.endTransaction();
            getContext().getContentResolver().notifyChange(MEDIA_CONTENT_URI,
                    null);
            getContext().getContentResolver().notifyChange(ALBUM_CONTENT_URI,
                    null);
        }
    }

    public int deleteOrderAudio(Uri uri, String selection,
            String[] selectionArgs) {
        return getDBHelper().delete(TableConsts.TBNAME_ORDERLIST, selection,
                selectionArgs);
    }

    public int deletePlaylist(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        int ret = db.delete(TableConsts.TBNAME_PLAYLIST, selection,
                selectionArgs);
        // リストのメンバーも削除する
        db.delete(TableConsts.TBNAME_PLAYLIST_AUDIO,
                MediaConsts.AudioPlaylistMember.PLAYLIST_ID + " = ?",
                selectionArgs);

        return ret;
    }

    public int deletePlaylistmember(Uri uri, String selection,
            String[] selectionArgs) {
        long id = ContentUris.parseId(uri);

        StringBuilder where = new StringBuilder();
        where.append(MediaConsts.AudioPlaylistMember.PLAYLIST_ID).append(" = ")
                .append(Long.toString(id));
        if (selection != null && selection.length() > 0) {
            where.append(" AND ( ").append(selection).append(" )");
        }

        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        int ret = db.delete(TableConsts.TBNAME_PLAYLIST, AudioPlaylist._ID + " = ?", new String[] {
                Long.toString(id)
        });
        db.delete(TableConsts.TBNAME_PLAYLIST_AUDIO,
                where.toString(), selectionArgs);
        Logger.d("ret=" + ret);
        return ret;
    }

    private int deleteVideo(Uri uri, String selection, String[] selectionArgs) {
        return getDBHelper().delete(TableConsts.TBNAME_VIDEO, selection,
                selectionArgs);
    }

    private int deleteMedia(Uri uri, String selection, String[] selectionArgs) {
        return getDBHelper().delete(TableConsts.TBNAME_AUDIO, selection,
                selectionArgs);
    }

    private int deleteArtist(Uri uri, String selection, String[] selectionArgs) {
        return getDBHelper().delete(TableConsts.TBNAME_ARTIST, selection,
                selectionArgs);
    }

    private int deleteAlbums(Uri uri, String selection, String[] selectionArgs) {
        return getDBHelper().delete(TableConsts.TBNAME_ALBUM, selection,
                selectionArgs);
    }

    private int mediaIdUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        if (values != null && values.get(TableConsts.AUDIO_CACHE_FILE) != null) {
            SQLiteDatabase db = getDBHelper().getWritableDatabase();
            Cursor cur = null;
            boolean hasTransaction = false;
            boolean needNotify = false;
            try {
                long id = ContentUris.parseId(uri);
                String fname = values.getAsString(TableConsts.AUDIO_CACHE_FILE);
                cur = db.query(TableConsts.TBNAME_AUDIO,
                        new String[] {
                            AudioMedia.DURATION
                        }, "_ID = ?",
                        new String[] {
                            Long.toString(id)
                        }, null, null, null);
                if (cur != null && cur.moveToFirst()) {
                    long duration = cur.getLong(cur
                            .getColumnIndex(AudioMedia.DURATION));
                    if (duration <= 0) {
                        // データを取得しなおして設定する
                        File cacheFile = new File(SdCardAccessHelper.cachedMusicDir, fname);
                        Map<String, Object> ret = getID3TagInfo(cacheFile);

                        String album = (String) ret.get("album");
                        String artist = (String) ret.get("artist");
                        String title = (String) ret.get("title");
                        String track = (String) ret.get("track");
                        if (album != null && album.length() > 0) {
                            values.put(AudioMedia.ALBUM, album);
                        }
                        if (artist != null && artist.length() > 0) {
                            values.put(AudioMedia.ARTIST, artist);
                        }
                        if (title != null && title.length() > 0) {
                            values.put(AudioMedia.TITLE, title);
                        }
                        if (track != null && track.length() > 0) {
                            values.put(AudioMedia.TRACK, track);
                        }
                        values.put(AudioMedia.DURATION,
                                (Long) ret.get("duration"));

                        // アルバムも更新しておく?うーんやめとく
                        needNotify = true;
                    }
                }

                db.beginTransaction();
                hasTransaction = true;
                db.update(TableConsts.TBNAME_AUDIO, values, AudioMedia._ID
                        + " = ?", new String[] {
                        Long.toString(id)
                });
                db.setTransactionSuccessful();
                return 1;
            } finally {
                if (hasTransaction) {
                    if (needNotify) {
                        getContext().getContentResolver().notifyChange(
                                MEDIA_CONTENT_URI, null);
                    }
                    db.endTransaction();
                }
            }
        } else {
            SQLiteDatabase db = getDBHelper().getWritableDatabase();
            try {
                long id = ContentUris.parseId(uri);
                db.beginTransaction();
                db.update(TableConsts.TBNAME_AUDIO, values, AudioMedia._ID
                        + " = ?", new String[] {
                        Long.toString(id)
                });
                db.setTransactionSuccessful();
                return 1;
            } finally {
                db.endTransaction();
            }
        }
    }

    public int updateMedia(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        return getDBHelper().update(TableConsts.TBNAME_AUDIO, values,
                selection, selectionArgs);
    }

    public int updateAlbum(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        return getDBHelper().update(TableConsts.TBNAME_ALBUM, values,
                selection, selectionArgs);
    }

    public int updateArtist(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        return getDBHelper().update(TableConsts.TBNAME_ARTIST, values,
                selection, selectionArgs);
    }

    public int updatePlaylist(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        return getDBHelper().update(TableConsts.TBNAME_PLAYLIST, values,
                selection, selectionArgs);
    }

    public int updatePlaylistmember(Uri uri, ContentValues values,
            String selection, String[] selectionArgs) {
        SQLiteDatabase db = null;
        Cursor cur = null;
        try {
            db = getDBHelper().getWritableDatabase();
            cur = db.query(TableConsts.TBNAME_PLAYLIST_AUDIO,
                    new String[] {
                        MediaConsts.AudioPlaylistMember.AUDIO_ID
                    },
                    selection, selectionArgs, null, null, null);
            if (cur != null && cur.moveToFirst()) {
                return db.update(TableConsts.TBNAME_PLAYLIST_AUDIO, values,
                        selection, selectionArgs);
            } else {
                long id = db.insert(TableConsts.TBNAME_PLAYLIST_AUDIO, null,
                        values);
                return 1;
            }
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public int updateGenres(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        return getDBHelper().update(TableConsts.TBNAME_GENRES, values,
                selection, selectionArgs);
    }

    public Uri insertDownload(Uri uri, ContentValues values) {
        return ContentUris.withAppendedId(DOWNLOAD_CONTENT_URI, getDBHelper()
                .insert(TableConsts.TBNAME_DOWNLOAD, values));
    }

    public Uri insertMedia(Uri uri, ContentValues values) {
        return ContentUris.withAppendedId(MEDIA_CONTENT_URI, getDBHelper()
                .insert(TableConsts.TBNAME_AUDIO, values));
    }

    public Uri insertAlbum(Uri uri, ContentValues values) {
        return ContentUris.withAppendedId(ALBUM_CONTENT_URI, getDBHelper()
                .insert(TableConsts.TBNAME_ALBUM, values));
    }

    public Uri insertArtist(Uri uri, ContentValues values) {
        return ContentUris.withAppendedId(ARTIST_CONTENT_URI, getDBHelper()
                .insert(TableConsts.TBNAME_ARTIST, values));
    }

    public Uri insertPlaylist(Uri uri, ContentValues values) {
        return ContentUris.withAppendedId(PLAYLIST_CONTENT_URI, getDBHelper()
                .insert(TableConsts.TBNAME_PLAYLIST, values));
    }

    public Uri insertPlaylistmember(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        long id = ContentUris.parseId(uri);
        long media_id = values.getAsLong(AudioPlaylistMember.AUDIO_ID);
        int order = values.getAsInteger(AudioPlaylistMember.PLAY_ORDER);

        Cursor cur = null;
        boolean hasTransaction = false;
        try {
            SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
            qb.setTables(TableConsts.TBNAME_AUDIO);
            cur = qb.query(db, MEDIA_FIELDS, AudioMedia._ID + " = ?",
                    new String[] {
                        Long.toString(media_id)
                    }, null, null, null);
            if (cur != null && cur.moveToFirst()) {
                String media_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.MEDIA_KEY));
                String title = cur.getString(cur
                        .getColumnIndex(AudioMedia.TITLE));
                String title_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.TITLE_KEY));
                long duration = cur.getLong(cur
                        .getColumnIndex(AudioMedia.DURATION));
                String artist = cur.getString(cur
                        .getColumnIndex(AudioMedia.ARTIST));
                String artist_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.ARTIST_KEY));
                String album = cur.getString(cur
                        .getColumnIndex(AudioMedia.ALBUM));
                String album_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.ALBUM_KEY));
                String data = cur
                        .getString(cur.getColumnIndex(AudioMedia.DATA));
                int tarck = cur.getInt(cur.getColumnIndex(AudioMedia.TRACK));
                long date_added = cur.getLong(cur
                        .getColumnIndex(AudioMedia.DATE_ADDED));
                long date_modified = cur.getLong(cur
                        .getColumnIndex(AudioMedia.DATE_MODIFIED));
                String tags = cur.getString(cur
                        .getColumnIndex(TableConsts.GENRES_TAGS));
                String year = cur.getString(cur
                        .getColumnIndex(AudioPlaylistMember.YEAR));
                int point = cur.getInt(cur
                        .getColumnIndex(TableConsts.FAVORITE_POINT));

                db.beginTransaction();
                hasTransaction = true;
                ContentValues val = new ContentValues();
                val.put(TableConsts.PLAYLIST_INIT_FLG, 1);
                db.update(TableConsts.TBNAME_PLAYLIST, val, AudioPlaylist._ID
                        + "=?", new String[] {
                        Long.toString(id)
                });

                ContentValues dvalues_pl = new ContentValues();
                dvalues_pl.put(AudioPlaylistMember._ID, media_key);
                dvalues_pl.put(AudioPlaylistMember.PLAY_ORDER, order);
                dvalues_pl.put(AudioPlaylistMember.AUDIO_ID, media_id);
                dvalues_pl.put(AudioPlaylistMember.MEDIA_KEY, media_key);
                dvalues_pl.put(AudioPlaylistMember.PLAYLIST_ID, id);
                dvalues_pl.put(AudioPlaylistMember.TITLE, title);
                dvalues_pl.put(AudioPlaylistMember.TITLE_KEY, title_key);
                dvalues_pl.put(AudioPlaylistMember.DURATION, duration);
                dvalues_pl.put(AudioPlaylistMember.ARTIST, artist);
                dvalues_pl.put(AudioPlaylistMember.ARTIST_KEY, artist_key);
                dvalues_pl.put(AudioPlaylistMember.ALBUM, album);
                dvalues_pl.put(AudioPlaylistMember.ALBUM_KEY, album_key);
                dvalues_pl.put(AudioPlaylistMember.DATA, data);
                dvalues_pl.put(AudioPlaylistMember.TRACK, tarck);
                dvalues_pl.put(AudioPlaylistMember.DATE_ADDED, date_added);
                dvalues_pl
                        .put(AudioPlaylistMember.DATE_MODIFIED, date_modified);
                dvalues_pl.put(TableConsts.GENRES_TAGS, tags);
                dvalues_pl.put(AudioPlaylistMember.YEAR, year);
                dvalues_pl.put(TableConsts.FAVORITE_POINT, point);
                long ret_id = db.replace(TableConsts.TBNAME_PLAYLIST_AUDIO,
                        null, dvalues_pl);

                db.setTransactionSuccessful();
                return ContentUris.withAppendedId(
                        MediaConsts.PLAYLIST_MEMBER_CONTENT_URI, ret_id);
            }
        } finally {
            if (cur != null) {
                cur.close();
            }
            if (hasTransaction) {
                db.endTransaction();
            }
        }
        return null;
    }

    public Uri insertGenres(Uri uri, ContentValues values) {
        return ContentUris.withAppendedId(GENRES_CONTENT_URI, getDBHelper()
                .insert(TableConsts.TBNAME_GENRES, values));
    }

    public Uri genresmemberInsert(Uri uri, ContentValues values) {
        return null;
    }

    public Cursor queryMediaId(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        long id = ContentUris.parseId(uri);

        return getDBHelper().query(TableConsts.TBNAME_AUDIO, projection,
                MediaConsts.AudioMedia._ID + " = ?",
                new String[] {
                    Long.toString(id)
                }, sortOrder);
    }

    public Cursor queryMedia(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        return getDBHelper().query(TableConsts.TBNAME_AUDIO, projection,
                selection, selectionArgs, sortOrder);
    }

    public Cursor queryArtist(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        return getDBHelper().query(TableConsts.TBNAME_ARTIST, projection,
                selection, selectionArgs, sortOrder);
    }

    public Cursor queryPlaylist(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        return getDBHelper().query(TableConsts.TBNAME_PLAYLIST, projection,
                selection, selectionArgs, sortOrder);
    }

    public Hashtable<String, String> getMedia(SQLiteDatabase db,
            String[] projection, long mediaId) {
        Hashtable<String, String> tbl = new Hashtable<String, String>();
        Cursor cur = null;
        try {
            String[] prj = projection;
            if (prj == null) {
                prj = new String[] {
                        AudioMedia.ALBUM, AudioMedia.ALBUM_KEY,
                        AudioMedia.ARTIST, AudioMedia.TITLE,
                        AudioMedia.DURATION
                };
            }
            cur = db.query(TableConsts.TBNAME_AUDIO, prj,
                    MediaConsts.AudioMedia._ID + " = ?",
                    new String[] {
                        Long.toString(mediaId)
                    }, null, null, null);
            if (cur != null && cur.moveToFirst()) {
                for (int i = 0; i < prj.length; i++) {
                    String value = cur.getString(cur.getColumnIndex(prj[i]));
                    if (value != null) {
                        tbl.put(prj[i], value);
                    }
                }
            }

            return tbl;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public Cursor queryPlaylistmember(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = null;
        long playlist_id = ContentUris.parseId(uri);
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_PLAYLIST_AUDIO);
        qb.appendWhere(AudioPlaylistMember.PLAYLIST_ID + " = " + playlist_id);
        Cursor cur = null;
        try {
            db = getDBHelper().getWritableDatabase();

            // PlaylistMemberからデータを取得し、合致するMedia情報をもとに、返事をかえす
            MatrixCursor ret_cursor = new MatrixCursor(playlist_audio_cols);
            cur = qb.query(db, new String[] {
                    BaseColumns._ID,
                    AudioPlaylistMember.AUDIO_ID,
                    AudioPlaylistMember.PLAY_ORDER
            }, selection, selectionArgs,
                    null, null, sortOrder);
            if (cur != null && cur.moveToFirst()) {
                do {
                    long mediaId = cur.getLong(cur
                            .getColumnIndex(AudioPlaylistMember.AUDIO_ID));
                    Hashtable<String, String> tbl = getMedia(db, audio_cols,
                            mediaId);
                    if (!tbl.isEmpty()) {
                        ret_cursor
                                .addRow(new Object[] {
                                        // Long.parseLong(tbl.get(BaseColumns._ID)),
                                        cur.getLong(cur
                                                .getColumnIndex(BaseColumns._ID)),
                                        tbl.get(AudioMedia._ID),
                                        tbl.get(AudioMedia.TITLE),
                                        tbl.get(AudioMedia.TITLE_KEY),
                                        tbl.get(AudioMedia.DATA),
                                        Funcs.getLong(tbl
                                                .get(AudioMedia.DURATION)),
                                        tbl.get(AudioMedia.ARTIST),
                                        tbl.get(AudioMedia.ARTIST_KEY),
                                        tbl.get(AudioMedia.ALBUM),
                                        tbl.get(AudioMedia.ALBUM_KEY),
                                        // tbl.get(AudioMedia.ALBUM_ART),
                                        cur.getInt(cur
                                                .getColumnIndex(AudioPlaylistMember.PLAY_ORDER)),
                                });
                    }
                } while (cur.moveToNext());
            }

            return ret_cursor;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public Cursor queryGenres(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        return getDBHelper().query(TableConsts.TBNAME_GENRES, projection,
                selection, selectionArgs, sortOrder);
    }

    public Cursor queryGenresmember(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        String[] sel = selection.split("=");
        String genres_key = null;
        for (int i = 0; i < sel.length; i++) {
            if (sel[i].indexOf(AudioGenresMember.GENRE_ID) != -1) {
                genres_key = selectionArgs[i];
                break;
            }
        }

        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        SQLiteQueryBuilder qb2 = new SQLiteQueryBuilder();
        qb2.setTables(TableConsts.TBNAME_AUDIO);
        String limit = null;

        StringBuilder where = new StringBuilder();
        where.append(TableConsts.GENRES_TAGS).append(
                " like '%/" + genres_key + "/%'");

        int pos = selection != null ? selection.indexOf("AND LIMIT") : -1;
        if (pos != -1) {
            // 途中から取得
            String offset = selectionArgs[selectionArgs.length - 1];
            String size = selectionArgs[selectionArgs.length - 2];
            selection = selection.substring(0, pos);
            if (Build.VERSION.SDK_INT < 9) {
                String[] tmp = new String[selectionArgs.length - 2];
                for (int i = 0; i < tmp.length; i++) {
                    tmp[i] = selectionArgs[i];
                }
                selectionArgs = tmp;
            } else {
                selectionArgs = ArrayUtils.fastArrayCopy(selectionArgs,
                        selectionArgs.length - 2);
            }
        }
        // 最初から取得
        Cursor cur = qb2.query(db, MEDIA_FIELDS, where.toString(), null, null,
                null, sortOrder, null);
        return cur;
    }

    public Cursor queryAlbum(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        return getDBHelper().query(TableConsts.TBNAME_ALBUM, projection,
                selection, selectionArgs, sortOrder);
    }

    public Uri insertOrderAudio(Uri uri, ContentValues values) {
        return ContentUris.withAppendedId(PLAYORDER_CONTENT_URI, getDBHelper()
                .insert(TableConsts.TBNAME_ORDERLIST, values));
    }

    public Uri insertFavorite(Uri uri, ContentValues values) {
        long id = values.getAsLong(FAVORITE_ID);
        int point = values.getAsInteger(FAVORITE_POINT);
        String type = values.getAsString(FAVORITE_TYPE);
        if (type.equals(FAVORITE_TYPE_SONG)) {
            ContentValues s_values = new ContentValues();
            s_values.put(TableConsts.FAVORITE_POINT, point);
            getDBHelper()
                    .update(TableConsts.TBNAME_AUDIO, s_values,
                            AudioMedia._ID + " = ?",
                            new String[] {
                                Long.toString(id)
                            });
        } else if (type.equals(FAVORITE_TYPE_ALBUM)) {
            ContentValues s_values = new ContentValues();
            s_values.put(TableConsts.FAVORITE_POINT, point);
            getDBHelper()
                    .update(TableConsts.TBNAME_ALBUM, s_values,
                            AudioAlbum._ID + " = ?",
                            new String[] {
                                Long.toString(id)
                            });
        } else if (type.equals(FAVORITE_TYPE_ARTIST)) {
            ContentValues s_values = new ContentValues();
            s_values.put(TableConsts.FAVORITE_POINT, point);
            getDBHelper().update(TableConsts.TBNAME_ARTIST, s_values,
                    AudioArtist._ID + " = ?",
                    new String[] {
                        Long.toString(id)
                    });
        }
        return ContentUris.withAppendedId(MediaConsts.FAVORITE_CONTENT_URI, id);
    }

    public Uri insertVideo(Uri uri, ContentValues values) {
        return ContentUris.withAppendedId(VIDEO_CONTENT_URI, getDBHelper()
                .insert(TableConsts.TBNAME_VIDEO, values));
    }

    public int updateFavorite(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        long id = Long.parseLong(selectionArgs[0]);
        int point = values.getAsInteger(FAVORITE_POINT);
        String type = selectionArgs[1];
        if (type.equals(FAVORITE_TYPE_SONG)) {
            ContentValues s_values = new ContentValues();
            s_values.put(TableConsts.FAVORITE_POINT, point);
            getDBHelper()
                    .update(TableConsts.TBNAME_AUDIO, s_values,
                            AudioMedia._ID + " = ?",
                            new String[] {
                                Long.toString(id)
                            });
        } else if (type.equals(FAVORITE_TYPE_ALBUM)) {
            ContentValues s_values = new ContentValues();
            s_values.put(TableConsts.FAVORITE_POINT, point);
            getDBHelper()
                    .update(TableConsts.TBNAME_ALBUM, s_values,
                            AudioAlbum._ID + " = ?",
                            new String[] {
                                Long.toString(id)
                            });
        } else if (type.equals(FAVORITE_TYPE_ARTIST)) {
            ContentValues s_values = new ContentValues();
            s_values.put(TableConsts.FAVORITE_POINT, point);
            getDBHelper().update(TableConsts.TBNAME_ARTIST, s_values,
                    AudioArtist._ID + " = ?",
                    new String[] {
                        Long.toString(id)
                    });
        }
        return 1;
    }

    public int updateVideo(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        return getDBHelper().update(TableConsts.TBNAME_VIDEO, values,
                selection, selectionArgs);
    }

    public int updateVideoId(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        return getDBHelper().update(TableConsts.TBNAME_VIDEO, values,
                BaseColumns._ID + " = ?",
                new String[] {
                    Long.toString(ContentUris.parseId(uri))
                });
    }

    public int updateDownload(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        return getDBHelper().update(TableConsts.TBNAME_DOWNLOAD, values,
                selection, selectionArgs);
    }

    private Cursor queryOrderAudio(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        return getDBHelper().query(TableConsts.TBNAME_ORDERLIST, projection,
                selection, selectionArgs, sortOrder);
    }

    public Cursor queryOrderAudioId(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        return getDBHelper().query(TableConsts.TBNAME_ORDERLIST, projection,
                BaseColumns._ID + " = ?",
                new String[] {
                    Long.toString(ContentUris.parseId(uri))
                },
                sortOrder);
    }

    private Cursor queryDownload(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        return getDBHelper().query(TableConsts.TBNAME_DOWNLOAD, projection,
                selection, selectionArgs, sortOrder);
    }

    public Cursor queryFavorite(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        MatrixCursor ret = new MatrixCursor(new String[] {
                BaseColumns._ID,
                TableConsts.FAVORITE_POINT
        });
        SQLiteDatabase db = getDBHelper().getReadableDatabase();
        Cursor cursor = null;
        try {
            long id = Long.parseLong(selectionArgs[0]);
            String type = selectionArgs[1];
            if (type.equals(FAVORITE_TYPE_SONG)) {
                cursor = db.query(TableConsts.TBNAME_AUDIO, new String[] {
                        BaseColumns._ID, TableConsts.FAVORITE_POINT
                },
                        AudioMedia._ID + " = ?",
                        new String[] {
                            Long.toString(id)
                        }, null, null, null);
            } else if (type.equals(FAVORITE_TYPE_ALBUM)) {
                cursor = db.query(TableConsts.TBNAME_ALBUM, new String[] {
                        BaseColumns._ID, TableConsts.FAVORITE_POINT
                },
                        AudioAlbum._ID + " = ?",
                        new String[] {
                            Long.toString(id)
                        }, null, null, null);
            } else if (type.equals(FAVORITE_TYPE_ARTIST)) {
                cursor = db.query(TableConsts.TBNAME_ARTIST, new String[] {
                        BaseColumns._ID, TableConsts.FAVORITE_POINT
                },
                        AudioArtist._ID + " = ?",
                        new String[] {
                            Long.toString(id)
                        }, null, null, null);
            }
            if (cursor != null && cursor.moveToFirst()) {
                ret.addRow(new Object[] {
                        id,
                        cursor.getInt(cursor
                                .getColumnIndex(TableConsts.FAVORITE_POINT))
                });
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return ret;
    }

    public Cursor queryVideo(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        return getDBHelper().query(TableConsts.TBNAME_VIDEO, projection,
                selection, selectionArgs, sortOrder);
    }

    public Cursor queryVideoId(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        return getDBHelper().query(TableConsts.TBNAME_VIDEO, projection,
                BaseColumns._ID + " = ?",
                new String[] {
                    Long.toString(ContentUris.parseId(uri))
                },
                sortOrder);
    }

    private String makePath(String rootPath, String name) {
        if (rootPath != null && rootPath.length() > 0) {
            return rootPath + "/" + name;
        }
        return name;
    }

    public Cursor queryFile(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        String find_path = mPreference.getString("root_path", "");
        if (selection != null && selection.indexOf(FileMedia.DATA) != -1) {
            String[] sel = selection.split("=");
            String path = null;
            boolean keepPath = false;
            for (int i = 0; i < sel.length; i++) {
                if (sel[i].indexOf(FileMedia.DATA) != -1) {
                    path = selectionArgs[i];
                } else if (sel[i].indexOf(FileMedia.KEEPPATH) != -1) {
                    keepPath = true;
                }
            }
            if (path != null && !path.equals(find_path)) {
                if (!path.equals("..")) {
                    find_path = path;
                } else {
                    find_path = new File(find_path).getParent();
                    // 親がない場合
                    if (find_path == null || find_path.length() == 0) {
                        find_path = "";
                    }
                }
                if (!keepPath) {
                    Editor editor = mPreference.edit();
                    editor.putString("root_path", find_path);
                    editor.commit();
                }
            }
        }

        MatrixCursor cursor = new MatrixCursor(new String[] {
                FileMedia._ID,
                FileMedia.TITLE, FileMedia.TYPE, FileMedia.DATE_MODIFIED,
                FileMedia.SIZE, FileMedia.DATA
        });

        if (find_path.length() > 0) {
            cursor.addRow(new Object[] {
                    "..".hashCode(), "..",
                    FileType.FOLDER, 0, 0, ".."
            });
        }

        // FolderInfoを取得する
        List<PlaylistMetaInfo> playlists = new ArrayList<PlaylistMetaInfo>();
        try {
            long folderId = 0L;
            if (find_path.length() > 0) {
                BoxFileInfo boxinfo = findBoxFileInfo(find_path);
                if (boxinfo != null) {
                    folderId = boxinfo.BOXNETID;
                }
            }
            Box box = MyApp.getInstance().getBoxnet();
            AccountTreeResponseParser response = BoxSynchronous.getInstance(
                    box.getApiKey()).getAccountTree(
                    getBoxnetHelper().getAuthToken(), folderId,
                    new String[] {
                        Box.PARAM_ONELEVEL
                    });
            if (response != null) {
                BoxFolder boxFolder = response.getFolder();
                String status = response.getStatus();
                if (status.equals(GetAccountTreeListener.STATUS_LISTING_OK)) {
                    BoxFileInfo boxfile = findBoxFileInfo(folderId);
                    if (boxfile == null) {
                        registerBoxFileInfo(boxFolder);
                    } else if (boxfile.DATE_MODIFIED != boxFolder.getUpdated()) {
                        updateBoxFileInfo(boxFolder);
                    }

                    // Folder
                    Iterator<? extends BoxFolder> foldersIterator = boxFolder
                            .getFoldersInFolder().iterator();
                    while (foldersIterator.hasNext()) {
                        BoxFolder subfolder = foldersIterator.next();
                        BoxFileInfo subboxfile = findBoxFileInfo(subfolder
                                .getId());
                        if (subboxfile == null) {
                            registerBoxFileInfo(subfolder);
                        } else if (subboxfile.DATE_MODIFIED != subfolder
                                .getUpdated()) {
                            updateBoxFileInfo(subfolder);
                        }

                        cursor.addRow(new Object[] {
                                subfolder.getId(),
                                subfolder.getFolderName(), FileType.FOLDER,
                                subfolder.getUpdated(), subfolder.getSize(),
                                makePath(find_path, subfolder.getFolderName())
                        });
                    }

                    // File
                    Iterator<? extends BoxFile> filesIterator = boxFolder
                            .getFilesInFolder().iterator();
                    while (filesIterator.hasNext()) {
                        BoxFile boxFile = filesIterator.next();
                        Logger.d("fname = " + boxFile.getFileName());
                        if (isMedia(boxFile.getFileName(),
                                getMediaExtensionList())) {

                            cursor.addRow(new Object[] {
                                    boxFile.getId(),
                                    boxFile.getFileName(), FileType.AUDIO,
                                    boxFile.getUpdated(), boxFile.getSize(),
                                    makePath(find_path, boxFile.getFileName())
                            });
                            BoxFileInfo mediaboxfile = findBoxFileInfo(boxFile
                                    .getId());
                            if (mediaboxfile == null) {
                                registerBoxFileInfo(find_path, boxFile);
                            } else {
                                updateBoxFileInfo(find_path, boxFile);
                            }
                        } else if (isMedia(boxFile.getFileName(),
                                getPlaylistExtensionList())) {
                            PlaylistMetaInfo ret = new PlaylistMetaInfo();
                            ret.folderId = boxFile.getFolderId();
                            ret.fileId = boxFile.getId();
                            ret.name = boxFile.getFileName();
                            ret.folder = find_path;
                            ret.filepath = find_path + "/" + boxFile.getFileName();
                            ret.lastModifiedDate = System.currentTimeMillis();
                            playlists.add(ret);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            for (PlaylistMetaInfo meta : playlists) {
                registPlaylistInfo(meta);
            }
        }

        return cursor;
    }

    public Cursor queryFileId(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        MatrixCursor ret_cursor = new MatrixCursor(MEDIA_PROJECTION);
        if (selection != null && selection.indexOf(FileMedia.DATA) != -1) {
            String[] sel = selection.split("=");
            String path = null;
            String type = null;
            for (int i = 0; i < sel.length; i++) {
                if (sel[i].indexOf(FileMedia.DATA) != -1) {
                    path = selectionArgs[i];
                } else if (sel[i].indexOf(FileMedia.TYPE) != -1) {
                    type = selectionArgs[i];
                }
            }
            if (path != null && !path.equals("..")) {
                if (FileType.AUDIO.equals(type)) {
                    Cursor media_cursor = null;
                    SQLiteDatabase db = null;
                    long fileId = 0;

                    SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
                    qb.setTables(TableConsts.TBNAME_AUDIO);
                    try {
                        db = getDBHelper().getReadableDatabase();

                        BoxFileInfo boxinfo = findBoxFileInfo(path);
                        if (boxinfo != null) {
                            fileId = boxinfo.BOXNETID;
                        }

                        media_cursor = qb.query(db, MEDIA_PROJECTION,
                                AudioMedia.DATA + " = ?",
                                new String[] {
                                    Long.toString(fileId)
                                }, null,
                                null, sortOrder);
                        if (media_cursor != null && media_cursor.moveToFirst()) {
                            long media_id = media_cursor.getLong(media_cursor
                                    .getColumnIndex(AudioMedia._ID));
                            String album = media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.ALBUM));
                            String album_key = media_cursor
                                    .getString(media_cursor
                                            .getColumnIndex(AudioMedia.ALBUM_KEY));
                            String artist = media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.ARTIST));
                            String title = media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.TITLE));
                            long duration = media_cursor.getLong(media_cursor
                                    .getColumnIndex(AudioMedia.DURATION));
                            String data = media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.DATA));
                            ret_cursor.addRow(new Object[] {
                                    media_id, album,
                                    album_key, artist, title, duration, data
                            });
                        } else {
                            // ない場合はまだスレッドが動いてる可能性がある
                            String[] split_name = path.split("/");
                            if (split_name != null && split_name.length > 1) {
                                long media_id = Funcs.makeSubstansId(path);
                                String album = split_name[split_name.length - 2];
                                String album_key = null;
                                String artist = null;
                                String title = split_name[split_name.length - 1];
                                long duration = 0;// metadata.get("xmpDM:album");
                                String data = path;

                                ret_cursor.addRow(new Object[] {
                                        media_id,
                                        album, album_key, artist, title,
                                        duration, data
                                });
                            }
                        }
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } finally {
                        if (media_cursor != null) {
                            media_cursor.close();
                        }
                    }
                } else if (FileType.VIDEO.equals(type)) {

                } else {
                    // ZIP
                }
            }
        }

        return ret_cursor;
    }

    public Map<String, Object> getID3TagInfo(File fname) {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        try {
            Map<String, Object> ret = new HashMap<String, Object>();
            mmr.setDataSource(fname.getPath());
            ret.put("album", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM));
            ret.put("title", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
            ret.put("artist",
                    mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
            ret.put("year", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR));
            ret.put("track",
                    mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_CD_TRACK_NUMBER));
            ret.put("genre", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE));
            ret.put("duration",
                    Funcs.parseLong(mmr
                            .extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)));

            return ret;
        } catch (IllegalArgumentException e) {
            Logger.e("不正な引数", e);
        }
        return null;
    }

    public BoxFileInfo findBoxFileInfo(long folderId) {
        Cursor cursor = null;
        try {
            cursor = getDBHelper().query(
                    BoxnetFile.TBNAME,
                    new String[] {
                            BoxnetFile._ID, BoxnetFile.PARENTID,
                            BoxnetFile.BOXNETID, BoxnetFile.TITLE,
                            BoxnetFile.TYPE, BoxnetFile.PATH, BoxnetFile.SIZE,
                            BoxnetFile.DATE_MODIFIED
                    },
                    MediaConsts.BoxnetFile.BOXNETID + " = ?",
                    new String[] {
                        Long.toString(folderId)
                    }, null);
            if (cursor != null && cursor.moveToFirst()) {
                BoxFileInfo inf = new BoxFileInfo();
                inf._ID = cursor.getLong(0);
                inf.PARENTID = cursor.getLong(1);
                inf.BOXNETID = cursor.getLong(2);
                inf.TITLE = cursor.getString(3);
                inf.TYPE = cursor.getInt(4);
                inf.PATH = cursor.getString(5);
                inf.SIZE = cursor.getLong(6);
                inf.DATE_MODIFIED = cursor.getLong(7);
                return inf;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    public BoxFileInfo findBoxFileInfo(String path) {
        Cursor cursor = null;
        try {
            cursor = getDBHelper().query(
                    BoxnetFile.TBNAME,
                    new String[] {
                            BoxnetFile._ID, BoxnetFile.PARENTID,
                            BoxnetFile.BOXNETID, BoxnetFile.TITLE,
                            BoxnetFile.TYPE, BoxnetFile.PATH, BoxnetFile.SIZE,
                            BoxnetFile.DATE_MODIFIED
                    },
                    MediaConsts.BoxnetFile.PATH + " = ?",
                    new String[] {
                        path
                    }, null);
            if (cursor != null && cursor.moveToFirst()) {
                BoxFileInfo inf = new BoxFileInfo();
                inf._ID = cursor.getLong(0);
                inf.PARENTID = cursor.getLong(1);
                inf.BOXNETID = cursor.getLong(2);
                inf.TITLE = cursor.getString(3);
                inf.TYPE = cursor.getInt(4);
                inf.PATH = cursor.getString(5);
                inf.SIZE = cursor.getLong(6);
                inf.DATE_MODIFIED = cursor.getLong(7);
                return inf;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    public void registerBoxFileInfo(BoxFolder boxFolder) {
        ContentValues value = new ContentValues();
        value.put(BoxnetFile.PARENTID, boxFolder.getParentFolderId());
        value.put(BoxnetFile.BOXNETID, boxFolder.getId());
        value.put(BoxnetFile.TITLE, boxFolder.getFolderName());
        value.put(BoxnetFile.TYPE, 1);
        value.put(BoxnetFile.PATH, boxFolder.getPath());
        value.put(BoxnetFile.SIZE, boxFolder.getSize());
        value.put(BoxnetFile.DATE_MODIFIED, boxFolder.getUpdated());
        getDBHelper().insert(BoxnetFile.TBNAME, value);
    }

    public void updateBoxFileInfo(BoxFolder boxFolder) {
        ContentValues value = new ContentValues();
        value.put(BoxnetFile.PARENTID, boxFolder.getParentFolderId());
        value.put(BoxnetFile.TITLE, boxFolder.getFolderName());
        value.put(BoxnetFile.PATH, boxFolder.getPath());
        value.put(BoxnetFile.SIZE, boxFolder.getSize());
        value.put(BoxnetFile.DATE_MODIFIED, boxFolder.getUpdated());
        getDBHelper().update(BoxnetFile.TBNAME, value,
                BoxnetFile.BOXNETID + " = ?",
                new String[] {
                    Long.toString(boxFolder.getId())
                });
    }

    public void registerBoxFileInfo(String filepath, BoxFile boxFolder) {
        ContentValues value = new ContentValues();
        value.put(BoxnetFile.PARENTID, boxFolder.getFolderId());
        value.put(BoxnetFile.BOXNETID, boxFolder.getId());
        value.put(BoxnetFile.TITLE, boxFolder.getFileName());
        value.put(BoxnetFile.TYPE, 1);
        value.put(BoxnetFile.PATH, filepath + "/" + boxFolder.getFileName());
        value.put(BoxnetFile.SIZE, boxFolder.getSize());
        value.put(BoxnetFile.DATE_MODIFIED, boxFolder.getUpdated());
        getDBHelper().insert(BoxnetFile.TBNAME, value);
    }

    public void updateBoxFileInfo(String filepath, BoxFile boxFolder) {
        ContentValues value = new ContentValues();
        value.put(BoxnetFile.PARENTID, boxFolder.getFolderId());
        value.put(BoxnetFile.TITLE, boxFolder.getFileName());
        value.put(BoxnetFile.PATH, filepath + "/" + boxFolder.getFileName());
        value.put(BoxnetFile.SIZE, boxFolder.getSize());
        value.put(BoxnetFile.DATE_MODIFIED, boxFolder.getUpdated());
        getDBHelper().update(BoxnetFile.TBNAME, value,
                BoxnetFile.BOXNETID + " = ?",
                new String[] {
                    Long.toString(boxFolder.getId())
                });
    }

    public static boolean isMedia(String fname, String[] exts) {
        int s1 = fname.lastIndexOf('.');
        if (s1 > 0) {
            String ext = fname.substring(s1);
            for (int i = 0; i < exts.length; i++) {
                if (ext.toLowerCase().equals(exts[i])) {
                    return true;
                }
            }
        }
        return false;
    }

    private String[] getMediaExtensionList() {
        if (mediaextensions == null) {
            String editMediaExt = mPreference.getString("editMediaExt",
                    SystemConsts.MEDIA_EXT);
            if (editMediaExt.trim().length() > 0) {
                String[] ext = editMediaExt.split(",");
                if (ext != null && ext.length > 0) {
                    String[] ret = new String[ext.length];
                    for (int i = 0; i < ext.length; i++) {
                        ret[i] = ext[i].trim().toLowerCase();
                    }
                    mediaextensions = ret;
                    return mediaextensions;
                }
            }
            mediaextensions = SystemConsts.MEDIA_EXT.split(",");
        }
        return mediaextensions;
    }

    private String[] getPlaylistExtensionList() {
        if (playlistextensions == null) {
            String editPlaylistExt = mPreference.getString(SystemConsts.CNF_EDITPLAYLISTEXT,
                    SystemConsts.PLAYLIST_EXT);
            if (editPlaylistExt.trim().length() > 0) {
                String[] ext = editPlaylistExt.split(",");
                if (ext != null && ext.length > 0) {
                    String[] ret = new String[ext.length];
                    for (int i = 0; i < ext.length; i++) {
                        ret[i] = ext[i].trim().toLowerCase();
                    }
                    playlistextensions = ret;
                    return playlistextensions;
                }
            }
            playlistextensions = SystemConsts.PLAYLIST_EXT.split(",");
        }
        return playlistextensions;
    }

    private void registPlaylistInfo(PlaylistMetaInfo metainf) {
        long playlsitId;
        // 楽曲情報の登録
        playlsitId = findRegisterdPlaylist(metainf.filepath);
        if (playlsitId == -1) {
            long playlistid = insertPlaylist(metainf);
        } else {
            long playlistid = updatePlaylist(playlsitId, metainf);
        }
    }

    private long findRegisterdPlaylist(String path) {
        // 有無をチェック
        Cursor cur = null;
        try {
            cur = getDBHelper().query(
                    TableConsts.TBNAME_PLAYLIST_AUDIO,
                    new String[] {
                        AudioPlaylist._ID
                    },
                    AudioPlaylist.DATA + " = ?",
                    new String[] {
                        path
                    }, null);
            if (cur != null && cur.moveToFirst()) {
                return cur.getLong(0);
            }
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return -1;
    }

    private long insertPlaylist(PlaylistMetaInfo metainf) {
        Calendar cal = Calendar.getInstance();

        M3UParser parser = new M3UParser(getBoxnetHelper());

        Cursor cur = null;
        try {
            parser.processPlayList(metainf);

            ContentValues values = new ContentValues();
            values.put(AudioPlaylist.NAME, metainf.name);
            values.put(AudioPlaylist.DATA, metainf.filepath);
            values.put(AudioPlaylist.DATE_ADDED, cal.getTimeInMillis());
            values.put(AudioPlaylist.DATE_MODIFIED, cal.getTimeInMillis());

            long playlistId = getDBHelper().insert(TableConsts.TBNAME_PLAYLIST, values);

            Uri playlisturi = ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI,
                    playlistId);

            int index = 0;
            for (PlaylistEntry entry : parser.getPlaylistEntries()) {
                long audioId = findAudioId(metainf.folder, entry.fname);
                Logger.d("add new playlist audio " + audioId);
                if (audioId != -1) {
                    ContentValues values2 = new ContentValues();
                    values2.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, audioId);
                    values2.put(MediaStore.Audio.Playlists.Members.PLAYLIST_ID, playlistId);
                    values2.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, index + 1);
                    getDBHelper().insert(TableConsts.TBNAME_PLAYLIST_AUDIO, values2);
                    index++;
                }
            }
            return playlistId;
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return -1;
    }

    private long updatePlaylist(long playlistId, PlaylistMetaInfo metainf) {
        M3UParser parser = new M3UParser(getBoxnetHelper());

        Cursor cur = null;
        try {
            parser.processPlayList(metainf);

            getDBHelper().delete(TableConsts.TBNAME_PLAYLIST,
                    AudioPlaylistMember.PLAYLIST_ID + " = ?",
                    new String[] {
                        Long.toString(playlistId)
                    });
            Uri playlisturi = ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI,
                    playlistId);

            int index = 0;
            for (PlaylistEntry entry : parser.getPlaylistEntries()) {
                long audioId = findAudioId(metainf.folder, entry.fname);
                if (audioId != -1) {
                    ContentValues values2 = new ContentValues();
                    values2.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, audioId);
                    values2.put(MediaStore.Audio.Playlists.Members.PLAYLIST_ID, playlistId);
                    values2.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, index + 1);
                    getDBHelper().insert(TableConsts.TBNAME_PLAYLIST_AUDIO, values2);
                    index++;
                }
            }
            return playlistId;
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return -1;
    }

    private long findAudioId(String path, String fname) {
        Cursor cursor = null;
        try {
            cursor = getDBHelper().query(
                    BoxnetFile.TBNAME,
                    new String[] {
                        BoxnetFile.BOXNETID
                    },
                    BoxnetFile.PATH + " = ?",
                    new String[] {
                        path + "/" + fname
                    }, null);
            if (cursor != null && cursor.moveToFirst()) {
                return cursor.getLong(0);
            }
        } finally {

        }
        return -1;
    }
}
