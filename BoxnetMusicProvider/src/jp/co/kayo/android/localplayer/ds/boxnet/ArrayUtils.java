package jp.co.kayo.android.localplayer.ds.boxnet;

import java.util.Arrays;

public class ArrayUtils {
    public static String[] fastArrayCopy(String[] selectionArgs, int length){
        return Arrays.copyOf(selectionArgs, length);
    }
}
