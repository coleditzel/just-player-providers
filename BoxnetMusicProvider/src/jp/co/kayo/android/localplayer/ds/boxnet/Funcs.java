package jp.co.kayo.android.localplayer.ds.boxnet;

public class Funcs {
    public static long makeSubstansId(String name) {
        return (name.hashCode() & 0xffffffffL) * -1;
    }

    public static long parseLong(String text) {
        if (text != null && text.length() > 0) {
            try {
                return Long.parseLong(text);
            } catch (Exception e) {

            }
        }
        return 0;
    }
    
    public static Long getLong(String s) {
        try {
            if (s != null) {
                return Long.parseLong(s);
            }
        } catch (Exception e) {
        }
        return null;
    }

    
    public static int parseInt(String text) {
        if (text != null && text.length() > 0) {
            try {
                return Integer.parseInt(text);
            } catch (Exception e) {

            }
        }
        return 0;
    }
}
