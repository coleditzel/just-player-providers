package jp.co.kayo.android.localplayer.ds.dropbox;

import jp.co.kayo.android.localplayer.consts.SystemConsts;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;

public class PrefActivity extends PreferenceActivity {
    EditTextPreference editTargetdir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        
        addPreferencesFromResource(R.xml.pref);
        
        editTargetdir = (EditTextPreference)findPreference(SystemConsts.CNF_TARGETDIR);
        if(editTargetdir!=null){
            editTargetdir.setSummary(getString(R.string.pref_target_summary)+editTargetdir.getSharedPreferences().getString(SystemConsts.CNF_TARGETDIR, "/"));
            editTargetdir.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    editTargetdir.setSummary(getString(R.string.pref_target_summary)+(String)newValue);
                    return true;
                }
            });
        }
    }

}
