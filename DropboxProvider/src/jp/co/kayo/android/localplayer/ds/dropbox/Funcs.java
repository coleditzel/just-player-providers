package jp.co.kayo.android.localplayer.ds.dropbox;

import java.io.UnsupportedEncodingException;


public class Funcs {
    
    public static void dump(String s){
        StringBuilder buf = new StringBuilder();
        for(byte b : s.getBytes()){
            if(buf.length()>0) buf.append(" ");
            buf.append(Byte.toString(b));
        }
        Logger.d("s = "+ buf.toString());
    }
    
    
    public static long createHashId(String path){
        if(path!=null && path.length()>1 && path.charAt(path.length()-1) == '/'){
            return path.substring(0, path.length()-1).hashCode();
        }else {
            return path.hashCode();
        }
    }

    
    public static long makeSubstansId(String name) {
        return (name.hashCode() & 0xffffffffL) * -1;
    }

    public static long parseLong(String text) {
        if (text != null && text.length() > 0) {
            try {
                return Long.parseLong(text);
            } catch (Exception e) {

            }
        }
        return 0;
    }
    
    public static int parseInt(String text) {
        if (text != null && text.length() > 0) {
            try {
                return Integer.parseInt(text);
            } catch (Exception e) {

            }
        }
        return 0;
    }
}
