//  Copyright 2011 Box.net.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

package jp.co.kayo.android.localplayer.ds.dropbox;

import jp.co.kayo.android.localplayer.consts.MediaConsts;

import com.dropbox.client2.DropboxAPI.Account;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.session.TokenPair;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private TextView statusText;
    private Button homeButton;
    private Button authenticateButton;

    DropboxHelper dropboxHelper;
    boolean mLoggedIn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dropboxHelper = new DropboxHelper(this);

        setContentView(R.layout.splash);

        statusText = (TextView) findViewById(R.id.statusText);

        authenticateButton = (Button) findViewById(R.id.authenticateButton);
        authenticateButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                dropboxHelper.getSession().unlink();
                mLoggedIn = false;
                dropboxHelper.clearKeys();
                getContentResolver().query(MediaConsts.RESET_CONTENT_URI, null, null, null, null);
                dropboxHelper.getSession().startAuthentication(MainActivity.this);
            }
        });

        homeButton = (Button) findViewById(R.id.homeButton);
        homeButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Dashboard.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        mLoggedIn = dropboxHelper.isLoggdIn();
        
        if(mLoggedIn){
            AyncGetAccount task = new AyncGetAccount();
            task.execute();
        }
        else{
            statusText.setText(getResources().getString(R.string.checking_login_status));
            homeButton.setVisibility(View.GONE);
            authenticateButton.setVisibility(View.GONE);
            if (dropboxHelper.getSession().authenticationSuccessful()) {
                try {
                    dropboxHelper.getSession().finishAuthentication();
    
                    TokenPair tokens = dropboxHelper.getSession()
                            .getAccessTokenPair();
                    dropboxHelper.storeKeys(tokens.key, tokens.secret);
                    mLoggedIn = true;
                    
                    AyncGetAccount task = new AyncGetAccount();
                    task.execute();
                    
                } catch (IllegalStateException e) {
                    Logger.e("Error authenticating", e);
                    onNotLoggedIn();
                }
            }
            else{
                onNotLoggedIn();
            }
        }
    }

    private void onNotLoggedIn() {
        statusText.setText(getString(R.string.txt_notloogdin));
        homeButton.setVisibility(View.GONE);
        authenticateButton.setText(getString(R.string.log_in));
        authenticateButton.setVisibility(View.VISIBLE);
    }

    private class AyncGetAccount extends AsyncTask<Void, Void, Account> {

        @Override
        protected Account doInBackground(Void... params) {
            try {
                Account acc = dropboxHelper.getAccountInfo();

                return acc;
            } catch (DropboxException e) {

                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Account acc) {
            if(acc != null){
                mLoggedIn = false;
                statusText.setText(getString(R.string.txt_loggedin)+"\n" + acc.displayName);
                homeButton.setVisibility(View.VISIBLE);
                authenticateButton.setText(getString(R.string.txt_different_user));
                authenticateButton.setVisibility(View.VISIBLE);
            }
            else{
                mLoggedIn = false;
                onNotLoggedIn();
            }
        }

        @Override
        protected void onCancelled() {
            mLoggedIn = false;
            onNotLoggedIn();
        }

    }

}
