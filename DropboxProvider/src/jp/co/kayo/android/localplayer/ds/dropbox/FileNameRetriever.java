package jp.co.kayo.android.localplayer.ds.dropbox;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import jp.co.kayo.android.localplayer.util.SdCardAccessHelper;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.media.MediaMetadataRetriever;
import android.os.Build;

import com.dropbox.client2.DropboxAPI.Entry;

public class FileNameRetriever {
    
    public String getTitleWithoutExtention(String fname){
        int pos = fname.lastIndexOf('.');
        if(pos!=-1){
            return fname.substring(0, pos-1);
        }
        return null;
    }
    
    public String getExtention(String fname){
        int pos = fname.lastIndexOf('.');
        if(pos!=-1){
            return fname.substring(pos+1);
        }
        return null;
    }

    private String getFnameWithoutExt(String fname){
        int pos = fname.lastIndexOf(".");
        if(pos>0){
            return fname.substring(0, pos);
        }
        else{
            return fname;
        }
    }
    
    private static String trackToIntegerString(String s){
        if(s!=null && s.length()>0){
            StringBuilder buf = new StringBuilder();
            for(int i=0; i<s.length(); i++){
                char c = s.charAt(i);
                if(c>='0' && c<='9'){
                    buf.append(c);
                }
            }
            if(buf.length()>0){
                return buf.toString();
            }
        }
        return "";
    }
    
    public static Map<String, Object> getID3TagInfo(File fname) {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        try {
            Map<String, Object> ret = new HashMap<String, Object>();
            mmr.setDataSource(fname.getPath());
            ret.put("album", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM));
            ret.put("title", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
            ret.put("artist",
                    mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
            ret.put("year", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR));
            ret.put("track",trackToIntegerString(
                    mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_CD_TRACK_NUMBER)));
            ret.put("genre", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE));
            ret.put("duration",
                    Funcs.parseLong(mmr
                            .extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)));

            return ret;
        } catch (Exception e) {
            Logger.e("不正な引数", e);
        }
        return null;
    }
    
    @SuppressLint("NewApi")
    public static Map<String, Object> getID3TagInfo(String url, HashMap<String, String> params) {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        try {
            Map<String, Object> ret = new HashMap<String, Object>();
            mmr.setDataSource(url, params);
            ret.put("album", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM));
            ret.put("title", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
            ret.put("artist",
                    mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
            ret.put("year", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR));
            ret.put("track",trackToIntegerString(
                    mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_CD_TRACK_NUMBER)));
            ret.put("genre", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE));
            ret.put("duration",
                    Funcs.parseLong(mmr
                            .extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)));

            return ret;
        } catch (Exception e) {
            Logger.e("不正な引数", e);
        }
        return null;
    }
    
    @TargetApi(14)
    public FileMetaInfo get(DropboxHelper dropboxHelper, Entry entry, boolean useId3Tag){
        Map<String, Object> mmrvalues = null;
        try {
            FileMetaInfo ret = new FileMetaInfo();
            ret.path = entry.parentPath();
            ret.filepath = entry.path;
            ret.name = entry.fileName();
            
            if(useId3Tag){
                File cacheFile = new File(SdCardAccessHelper.cachedMusicDir, "media" + Integer.toString(ret.filepath.hashCode())+".dat");
                if(cacheFile.exists()){
                    ret.cacheFile = cacheFile.getName();
                    mmrvalues = getID3TagInfo(cacheFile);
                }
                else{
                    if(Build.VERSION.SDK_INT>=14){
                        mmrvalues = getID3TagInfo(dropboxHelper.media(entry.path).url, new HashMap<String, String>());
                    }
                }
                if(mmrvalues!=null){
                    ret.album = (String)mmrvalues.get("album");
                    ret.title = (String)mmrvalues.get("title");
                    ret.artist = (String)mmrvalues.get("artist");
                    ret.year = (String)mmrvalues.get("year");
                    ret.track = (String)mmrvalues.get("track");
                    ret.genre = (String)mmrvalues.get("genre");
                    ret.duration = (Long)mmrvalues.get("duration");
                }
            }
            
            if(ret.artist == null || ret.artist.trim().length() == 0){
                File parent = new File(ret.filepath).getParentFile();
                if(parent != null && parent.getName().length()>1){
                    if(ret.album == null || ret.album.trim().length() == 0){
                        ret.album = parent.getName();
                    }
                    File parent_parent = parent.getParentFile();
                    if(parent_parent != null && parent_parent.getName().length()>1){
                        ret.artist = parent_parent.getName();
                    }
                    else{
                        ret.artist = dropboxHelper.getContext().getString(R.string.txt_unknown_artist);
                    }
                }
                else{
                    ret.album = dropboxHelper.getContext().getString(R.string.txt_unknown_album);
                    ret.artist = dropboxHelper.getContext().getString(R.string.txt_unknown_artist);
                }
            }
            
            if(ret.album == null || ret.album.trim().length() == 0){
                File parent = new File(ret.filepath).getParentFile();
                if(parent != null){
                    ret.album = parent.getName();
                }
            	else{
	            	ret.album = dropboxHelper.getContext().getString(R.string.txt_unknown_album);
            	}
            }
            
            if(ret.title == null || ret.title.trim().length() == 0){
                ret.title = getFnameWithoutExt(ret.name);
            }
            return ret;
        } catch (Exception e) {
            Logger.e("不正な音楽データ", e);
        }
        return null;
    }
}
