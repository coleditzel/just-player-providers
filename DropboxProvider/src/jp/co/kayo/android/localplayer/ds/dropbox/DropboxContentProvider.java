package jp.co.kayo.android.localplayer.ds.dropbox;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.ZipEntry;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.ds.dropbox.MediaFile.MediaFileType;
import jp.co.kayo.android.localplayer.util.M3UParser;
import jp.co.kayo.android.localplayer.util.M3UParser.PlaylistEntry;
import jp.co.kayo.android.localplayer.util.SdCardAccessHelper;
import android.content.ContentProvider;
import android.content.ContentProviderClient;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.provider.MediaStore;

import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.RESTUtility;
import com.dropbox.client2.exception.DropboxException;

/***
 * このクラスはMediaProvider（端末の音楽データ）のラッパーです
 * 
 * @author yokmama
 */
public class DropboxContentProvider extends ContentProvider implements
        MediaConsts {
    private static final int MAX_QUEUE = 10;

    private UriMatcher uriMatcher = null;

    private SharedPreferences mPreference;
    private DropboxDatabaseHelper dbhelper;
    private DropboxHelper dropboxHelper;
    private ConnectivityManager mConnectionMgr;

    public final String FAVORITE_ID = "media_id";
    public final String FAVORITE_TYPE = "type";
    public final String FAVORITE_POINT = "point";
    public final String FAVORITE_TYPE_SONG = "song";
    public final String FAVORITE_TYPE_ALBUM = "album";
    public final String FAVORITE_TYPE_ARTIST = "artist";

    private ExecutorService executor = Executors.newFixedThreadPool(MAX_QUEUE);

    public static final String[] MEDIA_FIELDS = new String[] {
            AudioMedia._ID,
            AudioMedia.TITLE, AudioMedia.MEDIA_KEY, AudioMedia.TITLE_KEY,
            AudioMedia.DURATION, AudioMedia.DATA, AudioMedia.ARTIST,
            AudioMedia.ARTIST_KEY, AudioMedia.ALBUM,
            AudioMedia.ALBUM_KEY,
            // AudioMedia.ALBUM_ART,
            AudioMedia.TRACK, AudioMedia.YEAR, AudioMedia.DATE_ADDED,
            AudioMedia.DATE_MODIFIED, TableConsts.AUDIO_CACHE_FILE,
            TableConsts.GENRES_TAGS, TableConsts.FAVORITE_POINT
    };

    private synchronized DropboxHelper getDropboxHelper() {
        if (dropboxHelper == null) {
            dropboxHelper = new DropboxHelper(getContext());
        }

        return dropboxHelper;
    }

    private boolean isWifiConnected() {
        if (mConnectionMgr == null) {
            mConnectionMgr = (ConnectivityManager) getContext()
                    .getApplicationContext().getSystemService(
                            Context.CONNECTIVITY_SERVICE);
        }
        NetworkInfo info = mConnectionMgr.getActiveNetworkInfo();
        if (info != null) {
            return ((info.getType() == ConnectivityManager.TYPE_WIFI) && (info
                    .isConnected()));
        }

        return false;
    }

    private boolean canUseNetwork() {
        return true;
        /*
        if (mPreference.getBoolean(SystemConsts.CNF_WIFI_ONLY, true)) {
            return isWifiConnected();
        } else {
            return true;
        }*/
    }

    private synchronized DropboxDatabaseHelper getDBHelper() {
        if (dbhelper == null) {
            if (dbhelper != null) {
                dbhelper.close();
                dbhelper = null;
            }
            dbhelper = new DropboxDatabaseHelper(getContext());
        }
        return dbhelper;
    }

    @Override
    public boolean onCreate() {
        mPreference = PreferenceManager
                .getDefaultSharedPreferences(getContext()
                        .getApplicationContext());

        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/media", CODE_MEDIA);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/media/#", CODE_MEDIA_ID);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/albums", CODE_ALBUMS);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/albums/#", CODE_ALBUMS_ID);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/artist", CODE_ARTIST);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/artist/#", CODE_ARTIST_ID);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/albumart", AUDIO_ALBUMART);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/albumart/#",
                AUDIO_ALBUMART_ID);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/media/#/albumart",
                AUDIO_ALBUMART_FILE_ID);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/playlist", CODE_PLAYLIST);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/playlist/#",
                CODE_PLAYLIST_ID);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/playlistmember",
                CODE_PLAYLISTMEMBER);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/playlistmember/#",
                CODE_PLAYLISTMEMBER_ID);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/favorite", CODE_FAVORITE);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/favorite/#",
                CODE_FAVORITE_ID);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/file", CODE_FILE);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/file/#", CODE_FILE_ID);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "video/media", CODE_VIDEO);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "video/media/#", CODE_VIDEO_ID);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "order/audio", CODE_ORDER_AUDIO);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "order/audio/#",
                CODE_ORDER_AUDIO_ID);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "download/media", CODE_DOWNLOAD);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/genres", CODE_GENRES);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/genres/#", CODE_GENRES_ID);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/genresmember",
                CODE_GENRESMEMBER);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "audio/genresmember/#",
                CODE_GENRESMEMBER_ID);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "config/auth", CODE_AUTH);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "config/clear", CODE_CLEAR);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "config/reset", CODE_RESET);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "config/ping", CODE_PING);
        uriMatcher.addURI(DROPBOX_AUTHORITY, "config/url", CODE_URL);

        return true;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case CODE_ORDER_AUDIO: {
                return orderAudioDelete(uri, selection, selectionArgs);
            }
            case CODE_PLAYLIST: {
                return playlistDelete(uri, selection, selectionArgs);
            }
            case CODE_PLAYLIST_ID: {
                return playlistIdDelete(uri, selection, selectionArgs);
            }
            case CODE_PLAYLISTMEMBER: {
                return playlistMemberDelete(uri, selection, selectionArgs);
            }
            case CODE_ALBUMS: {
                return albumsDelete(uri, selection, selectionArgs);
            }
            case CODE_ARTIST: {
                return artistDelete(uri, selection, selectionArgs);
            }
            case CODE_MEDIA: {
                return mediaDelete(uri, selection, selectionArgs);
            }
            case CODE_VIDEO: {
                return videoDelete(uri, selection, selectionArgs);
            }

        }
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int match = uriMatcher.match(uri);
        Logger.d("insert " + uri);
        switch (match) {
            case CODE_MEDIA: {
                return mediaInsert(uri, values);
            }
            case CODE_ALBUMS: {
                return albumInsert(uri, values);
            }
            case CODE_ARTIST: {
                return artistInsert(uri, values);
            }
            case CODE_PLAYLIST: {
                return playlistInsert(uri, values);
            }
            case CODE_PLAYLIST_ID: {
                return playlistmemberInsert(uri, values);
            }
            case CODE_FAVORITE: {
                return favoriteInsert(uri, values);
            }
            case CODE_VIDEO: {
                return videoInsert(uri, values);
            }
            case CODE_ORDER_AUDIO: {
                return orderAudioInsert(uri, values);
            }
            case CODE_DOWNLOAD: {
                return downloadInsert(uri, values);
            }
            case CODE_GENRES: {
                return genresInsert(uri, values);
            }
            case CODE_GENRES_ID: {
                return genresmemberInsert(uri, values);
            }
        }
        return null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        // ここでやるべきこと
        // JustPlayer内で定義された各フィールドの名前、これはSDKのMediaProviderと異なる可能性があるので、
        // 変換して検索を行い、また画像データやふりがなは独自のデータベースから取得するようにする
        if (Build.VERSION.SDK_INT > 10) {
            StrictHelper.registStrictMode();
        }

        int match = uriMatcher.match(uri);
        switch (match) {
            case CODE_MEDIA: {
                return mediaQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_MEDIA_ID: {
                return mediaIdQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_ARTIST: {
                // 移植済
                return artistQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_PLAYLIST: {
                // 移植済
                return playlistQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_PLAYLIST_ID: {
                return playlistmemberQuery(uri, projection, selection,
                        selectionArgs, sortOrder);
            }
            case CODE_FAVORITE: {
                // 移植済
                return favoriteQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_VIDEO: {
                // 移植済
                return videoQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_VIDEO_ID: {
                return videoIdQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_ALBUMS: {
                // 移植済
                return albumQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_GENRES: {
                // 移植済
                return genresQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_GENRESMEMBER: {
                return genresmemberQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_ORDER_AUDIO: {
                return orderAudioQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_ORDER_AUDIO_ID: {
                return orderAudioIdQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_DOWNLOAD: {
                return downloadQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_PING: {
                if (selection != null && "force".equals(selection)) {
                    dropboxHelper = null;
                }
                MatrixCursor cursor = null;
                boolean b = getDropboxHelper().isLoggdIn();
                if (b == true) {
                    cursor = new MatrixCursor(new String[] {
                            Auth._ID, Auth.AUTH_KEY
                    });
                    cursor.addRow(new Object[] {
                            0, "hassession"
                    });

                    // 更新処理
                    Intent i = new Intent(getContext(), MediaScanner.class);
                    i.setAction("sync");
                    getContext().startService(i);
                }
                return cursor;
            }
            case CODE_AUTH:
            case CODE_URL: {
                MatrixCursor cursor = new MatrixCursor(new String[] {
                        Auth._ID,
                        Auth.AUTH_URL
                });
                String url = getDropboxHelper().getAuthrizedURL(selectionArgs[0]);
                cursor.addRow(new Object[] {
                        0, url
                });
                return cursor;
            }
            case CODE_RESET: {
                if (selection != null && selection.equals("unlink")) {
                    dropboxHelper = null;
                }
                else {
                    getDBHelper().reset(getDBHelper().getWritableDatabase());
                    Editor editor = mPreference.edit();
                    editor.remove("mediaSum");
                    editor.commit();

                    dropboxHelper = null;
                }
            }
                break;
            case CODE_FILE: {
                return fileQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_FILE_ID: {
                return fileIdQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
        }
        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case CODE_MEDIA: {
                return mediaUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_MEDIA_ID: {
                return mediaIdUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_ALBUMS: {
                return albumUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_ALBUMS_ID: {
                return albumIdUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_ARTIST: {
                return artistUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_ARTIST_ID: {
                return artistIdUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_PLAYLIST: {
                return playlistUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_PLAYLISTMEMBER: {
                return playlistmemberUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_FAVORITE: {
                return favoriteUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_VIDEO: {
                return videoUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_VIDEO_ID: {
                return videoIdUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_DOWNLOAD: {
                return downloadUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_GENRES: {
                return genresUpdate(uri, values, selection, selectionArgs);
            }
        }
        return 0;
    }

    private int artistIdUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        if (values != null && values.containsKey(AudioMedia.ENCODING)) {
            // Artist Update
            long id = ContentUris.parseId(uri);
            if (id >= 0) {
                String enc = values.getAsString(AudioMedia.ENCODING);
                SQLiteDatabase db = getDBHelper().getWritableDatabase();
                Cursor cur = null;
                try {
                    db.beginTransaction();
                    cur = db.query(TableConsts.TBNAME_ARTIST,
                            new String[] {
                                AudioArtist.ARTIST
                            },
                            AudioArtist._ID + " = ?",
                            new String[] {
                                Long.toString(id)
                            }, null, null, null);
                    if (cur != null && cur.moveToFirst()) {
                        byte[] bufs = getBytes(cur.getString(cur
                                .getColumnIndex(AudioArtist.ARTIST)));
                        if (bufs != null && bufs.length > 0) {
                            ContentValues updateValues = new ContentValues();
                            updateValues.put(AudioMedia.ARTIST, new String(bufs, enc));

                            int ret = db.update(TableConsts.TBNAME_ARTIST, updateValues,
                                    AudioMedia._ID + " = ?",
                                    new String[] {
                                        Long.toString(id)
                                    });
                            db.setTransactionSuccessful();
                            return ret;
                        }
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } finally {
                    if (cur != null) {
                        cur.close();
                    }
                    db.endTransaction();
                    getContext().getContentResolver().notifyChange(
                            ARTIST_CONTENT_URI, null);
                }
            }
            return 0;
        } else {
            SQLiteDatabase db = getDBHelper().getWritableDatabase();
            Cursor cur = null;
            try {
                long id = ContentUris.parseId(uri);
                db.beginTransaction();
                db.update(TableConsts.TBNAME_ARTIST, values, AudioArtist._ID
                        + " = ?", new String[] {
                        Long.toString(id)
                });

                cur = db.query(TableConsts.TBNAME_ARTIST,
                        new String[] {
                            AudioArtist.ARTIST_KEY
                        },
                        AudioArtist._ID + " = ?",
                        new String[] {
                            Long.toString(id)
                        }, null, null, null);
                if (cur != null && cur.moveToFirst()) {
                    String artistKey = cur.getString(cur
                            .getColumnIndex(AudioArtist.ARTIST_KEY));
                    ContentValues audioValues = new ContentValues();
                    if (values.containsKey(TableConsts.ARTIST_INIT_FLG)
                            && values.getAsInteger(TableConsts.ARTIST_INIT_FLG) == 0) {
                        // Mediaのキャッシュファイルを削除
                        audioValues.put(TableConsts.AUDIO_CACHE_FILE,
                                (String) null);
                    } else {
                        if (values.containsKey(AudioArtist.ARTIST)) {
                            audioValues.put(AudioMedia.ARTIST,
                                    values.getAsString(AudioAlbum.ARTIST));
                        }
                    }
                    if (audioValues.size() > 0) {
                        db.update(TableConsts.TBNAME_AUDIO, audioValues,
                                AudioMedia.ARTIST_KEY + " = ?",
                                new String[] {
                                    artistKey
                                });
                    }
                }

                db.setTransactionSuccessful();
                return 1;
            } finally {
                if (cur != null) {
                    cur.close();
                }
                db.endTransaction();
                getContext().getContentResolver().notifyChange(
                        MEDIA_CONTENT_URI, null);
                getContext().getContentResolver().notifyChange(
                        ARTIST_CONTENT_URI, null);
            }
        }
    }

    private int albumIdUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        if (values != null && values.containsKey(AudioMedia.ENCODING)) {
            long id = ContentUris.parseId(uri);
            if (id >= 0) {
                // Album update
                String enc = values.getAsString(AudioMedia.ENCODING);
                SQLiteDatabase db = getDBHelper().getWritableDatabase();
                String albumkey = null;
                Cursor cur = null;
                try {
                    db.beginTransaction();
                    cur = db.query(TableConsts.TBNAME_ALBUM,
                            new String[] {
                                    AudioAlbum.ALBUM,
                                    AudioAlbum.ALBUM_KEY
                            },
                            AudioAlbum._ID + " = ?",
                            new String[] {
                                Long.toString(id)
                            }, null, null, null);
                    if (cur != null && cur.moveToFirst()) {
                        albumkey = cur.getString(cur.getColumnIndex(AudioAlbum.ALBUM_KEY));
                        byte[] bufs = getBytes(cur.getString(cur.getColumnIndex(AudioAlbum.ALBUM)));
                        if (bufs != null && bufs.length > 0) {
                            ContentValues updateValues = new ContentValues();
                            updateValues.put(AudioAlbum.ALBUM, new String(bufs, enc));

                            int ret = db.update(TableConsts.TBNAME_ALBUM, updateValues,
                                    AudioAlbum._ID + " = ?",
                                    new String[] {
                                        Long.toString(id)
                                    });
                            db.setTransactionSuccessful();
                        }
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } finally {
                    if (cur != null) {
                        cur.close();
                        cur = null;
                    }
                    db.endTransaction();
                    getContext().getContentResolver().notifyChange(
                            ALBUM_CONTENT_URI, null);
                }
                // Media update
                if (albumkey != null) {
                    cur = getContext().getContentResolver().query(
                            MediaConsts.MEDIA_CONTENT_URI,
                            new String[] {
                                    AudioMedia._ID, AudioMedia.TITLE,
                                    AudioMedia.ALBUM, AudioMedia.ARTIST
                            },
                            AudioMedia.ALBUM_KEY + " = ?",
                            new String[] {
                                albumkey
                            }, null);
                    if (cur != null && cur.moveToFirst()) {
                        return setId3Tag(cur, enc);
                    }
                }
            }
            return 0;
        } else {
            SQLiteDatabase db = getDBHelper().getWritableDatabase();
            Cursor cur = null;
            try {
                long id = ContentUris.parseId(uri);
                db.beginTransaction();
                db.update(TableConsts.TBNAME_ALBUM, values, AudioAlbum._ID
                        + " = ?", new String[] {
                        Long.toString(id)
                });

                // 関連する音楽の更新
                cur = db.query(TableConsts.TBNAME_ALBUM,
                        new String[] {
                            AudioAlbum.ALBUM_KEY
                        }, AudioAlbum._ID
                                + " = ?", new String[] {
                            Long.toString(id)
                        },
                        null, null, null);
                if (cur != null && cur.moveToFirst()) {
                    String albumKey = cur.getString(cur
                            .getColumnIndex(AudioAlbum.ALBUM_KEY));
                    ContentValues audioValues = new ContentValues();
                    if (values.containsKey(TableConsts.ALBUM_INIT_FLG)
                            && values.getAsInteger(TableConsts.ALBUM_INIT_FLG) == 0) {
                        // Albumの初期化フラグをOff
                        audioValues.put(TableConsts.AUDIO_CACHE_FILE,
                                (String) null);
                    } else {
                        if (values.containsKey(AudioAlbum.ALBUM)) {
                            audioValues.put(AudioMedia.ALBUM,
                                    values.getAsString(AudioAlbum.ALBUM));
                        }
                        if (values.containsKey(AudioAlbum.ARTIST)) {
                            audioValues.put(AudioMedia.ARTIST,
                                    values.getAsString(AudioAlbum.ARTIST));
                        }
                    }
                    if (audioValues.size() > 0) {
                        db.update(TableConsts.TBNAME_AUDIO, audioValues,
                                AudioMedia.ALBUM_KEY + " = ?",
                                new String[] {
                                    albumKey
                                });
                    }
                }

                db.setTransactionSuccessful();
                return 1;
            } finally {
                if (cur != null) {
                    cur.close();
                }
                db.endTransaction();
                getContext().getContentResolver().notifyChange(
                        MEDIA_CONTENT_URI, null);
                getContext().getContentResolver().notifyChange(
                        ALBUM_CONTENT_URI, null);
            }
        }
    }

    private int setId3Tag(Cursor cur, String enc) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        boolean hasTransaction = false;
        int count = 0;
        try {
            do {
                ContentValues updateValues = new ContentValues();
                byte[] bufs = getBytes(cur.getString(cur
                        .getColumnIndex(AudioMedia.TITLE)));
                if (bufs != null) {
                    updateValues.put(AudioMedia.TITLE, new String(bufs, enc));
                }
                bufs = getBytes(cur.getString(cur
                        .getColumnIndex(AudioMedia.ARTIST)));
                if (bufs != null) {
                    updateValues.put(AudioMedia.ARTIST, new String(bufs, enc));
                }
                bufs = getBytes(cur.getString(cur
                        .getColumnIndex(AudioMedia.ALBUM)));
                if (bufs != null) {
                    updateValues.put(AudioMedia.ALBUM, new String(bufs, enc));
                }
                if (updateValues.size() > 0) {
                    if (hasTransaction != true) {
                        hasTransaction = true;
                        db.beginTransaction();
                    }
                    long id = cur.getLong(cur.getColumnIndex(AudioMedia._ID));
                    db.update(TableConsts.TBNAME_AUDIO, updateValues,
                            AudioMedia._ID + " = ?",
                            new String[] {
                                Long.toString(id)
                            });
                    count++;
                }
            } while (cur.moveToNext());
            return count;
        } catch (UnsupportedEncodingException e) {
        } finally {
            if (hasTransaction) {
                db.setTransactionSuccessful();
                db.endTransaction();
            }
        }
        return count;
    }

    private Map<String, String> getAlbumPath(String albumKey) {
        Cursor cur = null;
        try {
            SQLiteDatabase db = getDBHelper().getWritableDatabase();
            cur = db.query(TableConsts.TBNAME_ALBUM,
                    new String[] {
                            AudioAlbum._ID, AudioAlbum.DATA
                    }, AudioAlbum.ALBUM_KEY
                            + " = ?", new String[] {
                        albumKey
                    }, null,
                    null, null);
            if (cur != null && cur.moveToFirst()) {
                Map<String, String> map = new HashMap<String, String>();
                map.put(AudioAlbum._ID, cur.getString(cur
                        .getColumnIndex(AudioAlbum._ID)));
                map.put(AudioAlbum._ID, cur.getString(cur
                        .getColumnIndex(AudioAlbum.DATA)));
                return map;
            }

        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return null;
    }

    public int orderAudioDelete(Uri uri, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            int n =  db
                    .delete(TableConsts.TBNAME_ORDERLIST, selection, selectionArgs);
            db.setTransactionSuccessful();
            return n;
        }
        finally{
            db.endTransaction();
        }
    }

    public int playlistDelete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            int ret = db.delete(TableConsts.TBNAME_PLAYLIST, selection,
                    selectionArgs);
            // リストのメンバーも削除する
            db.delete(TableConsts.TBNAME_PLAYLIST_AUDIO,
                    MediaConsts.AudioPlaylistMember.PLAYLIST_ID + " = ?",
                    selectionArgs);
            db.setTransactionSuccessful();
            return ret;
        }
        finally{
            db.endTransaction();
        }
    }

    public int playlistIdDelete(Uri uri, String selection,
            String[] selectionArgs) {
        long id = ContentUris.parseId(uri);

        StringBuilder where = new StringBuilder();
        where.append(MediaConsts.AudioPlaylistMember.PLAYLIST_ID).append(" = ")
                .append(Long.toString(id));
        if (selection != null && selection.length() > 0) {
            where.append(" AND ( ").append(selection).append(" )");
        }

        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            int ret = db.delete(TableConsts.TBNAME_PLAYLIST, AudioPlaylist._ID + " = ?", new String[] {
                    Long.toString(id)
            });
            db.delete(TableConsts.TBNAME_PLAYLIST_AUDIO,
                    where.toString(),
                    selectionArgs);
            Logger.d("ret=" + ret);
            db.setTransactionSuccessful();
            return ret;
        }
        finally{
            db.endTransaction();
        }
    }
    
    public int playlistMemberDelete(Uri uri, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            int ret = db.delete(TableConsts.TBNAME_PLAYLIST_AUDIO,
                    selection,
                    selectionArgs);
            Logger.d("ret=" + ret);
            db.setTransactionSuccessful();
            return ret;
        }
        finally{
            db.endTransaction();
        }
    }

    private int videoDelete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            
            int ret = db.delete(TableConsts.TBNAME_VIDEO, selection, selectionArgs);
            Logger.d("ret=" + ret);
            db.setTransactionSuccessful();
            return ret;
        }
        finally{
            db.endTransaction();
        }
    }

    private int mediaDelete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            int ret = db.delete(TableConsts.TBNAME_AUDIO, selection, selectionArgs);
            Logger.d("ret=" + ret);
            db.setTransactionSuccessful();
            return ret;
        }
        finally{
            db.endTransaction();
        }
    }

    private int artistDelete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            int ret = db
                    .delete(TableConsts.TBNAME_ARTIST, selection, selectionArgs);
            Logger.d("ret=" + ret);
            db.setTransactionSuccessful();
            return ret;
        }
        finally{
            db.endTransaction();
        }
    }

    private int albumsDelete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            int ret = db.delete(TableConsts.TBNAME_ALBUM, selection, selectionArgs);
            Logger.d("ret=" + ret);
            db.setTransactionSuccessful();
            return ret;
        }
        finally{
            db.endTransaction();
        }
    }

    private byte[] getBytes(String s) {
        int slen = s.length();
        byte bytes[] = new byte[slen];
        for (int i = 0; i < slen; i++) {
            char c = s.charAt(i);
            if (c >= 0x100) {
                // Byte列になっていない
                return null;
            }
            bytes[i] = (byte) c;
        }
        return bytes;
    }

    private int mediaIdUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        if (values != null && values.containsKey(AudioMedia.ENCODING)) {
            long id = ContentUris.parseId(uri);
            if (id >= 0) {
                String enc = values.getAsString(AudioMedia.ENCODING);
                SQLiteDatabase db = getDBHelper().getWritableDatabase();
                Cursor cur = null;
                try {
                    cur = db.query(TableConsts.TBNAME_AUDIO, new String[] {
                            AudioMedia._ID, AudioMedia.TITLE, AudioMedia.ALBUM,
                            AudioMedia.ARTIST
                    }, "_ID = ?",
                            new String[] {
                                Long.toString(id)
                            }, null, null,
                            null);
                    if (cur != null && cur.moveToFirst()) {
                        return setId3Tag(cur, enc);
                    }
                } finally {
                    if (cur != null) {
                        cur.close();
                    }
                    getContext().getContentResolver().notifyChange(
                            MEDIA_CONTENT_URI, null);
                }
            }
            return 0;
        } else if (values != null
                && values.get(TableConsts.AUDIO_CACHE_FILE) != null) {
            SQLiteDatabase db = getDBHelper().getWritableDatabase();
            Cursor cur = null;
            boolean hasTransaction = false;
            boolean needNotify = false;
            try {
                long id = ContentUris.parseId(uri);
                String fname = values.getAsString(TableConsts.AUDIO_CACHE_FILE);
                String albumKey = null;
                ContentValues albumvalues = null;
                cur = db.query(TableConsts.TBNAME_AUDIO,
                        new String[] {
                                AudioMedia.ALBUM_KEY,
                                AudioMedia.DURATION
                        }, "_ID = ?",
                        new String[] {
                            Long.toString(id)
                        }, null, null, null);
                if (cur != null && cur.moveToFirst()) {
                    long duration = cur.getLong(cur
                            .getColumnIndex(AudioMedia.DURATION));
                    albumKey = cur.getString(cur
                            .getColumnIndex(AudioMedia.ALBUM_KEY));
                    if (duration <= 0) {
                        // データを取得しなおして設定する
                        File cacheFile = new File(SdCardAccessHelper.cachedMusicDir, fname);
                        Map<String, Object> ret = FileNameRetriever.getID3TagInfo(cacheFile);

                        String album = (String) ret.get("album");
                        String artist = (String) ret.get("artist");
                        String title = (String) ret.get("title");
                        String track = (String) ret.get("track");
                        if (album != null && album.length() > 0) {
                            values.put(AudioMedia.ALBUM, album);
                            if (albumvalues == null)
                                albumvalues = new ContentValues();
                            albumvalues.put(AudioAlbum.ALBUM, album);
                        }
                        if (artist != null && artist.length() > 0) {
                            values.put(AudioMedia.ARTIST, artist);
                            // if(albumvalues==null) albumvalues = new
                            // ContentValues();
                            // albumvalues.put(AudioAlbum.ARTIST, artist);
                        }
                        if (title != null && title.length() > 0) {
                            values.put(AudioMedia.TITLE, title);
                        }
                        if (track != null && track.length() > 0) {
                            values.put(AudioMedia.TRACK, track);
                        }
                        values.put(AudioMedia.DURATION,
                                (Long) ret.get("duration"));

                        needNotify = true;
                    }
                }

                db.beginTransaction();
                hasTransaction = true;
                db.update(TableConsts.TBNAME_AUDIO, values, AudioMedia._ID
                        + " = ?", new String[] {
                        Long.toString(id)
                });
                if (albumvalues != null) {
                    // Albumも更新
                    db.update(TableConsts.TBNAME_ALBUM, albumvalues, AudioAlbum.ALBUM_KEY
                            + " = ?", new String[] {
                            albumKey
                    });
                }
                db.setTransactionSuccessful();
                return 1;
            } finally {
                if (hasTransaction) {
                    if (needNotify) {
                        getContext().getContentResolver().notifyChange(
                                MEDIA_CONTENT_URI, null);
                    }
                    db.endTransaction();
                }
            }
        } else {
            SQLiteDatabase db = getDBHelper().getWritableDatabase();
            try {
                long id = ContentUris.parseId(uri);
                db.beginTransaction();
                db.update(TableConsts.TBNAME_AUDIO, values, AudioMedia._ID
                        + " = ?", new String[] {
                        Long.toString(id)
                });
                db.setTransactionSuccessful();
                return 1;
            } finally {
                db.endTransaction();
            }
        }
    }

    public int mediaUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            int n =  db.update(TableConsts.TBNAME_AUDIO, values, selection,
                    selectionArgs);
            db.setTransactionSuccessful();
            return n;
        }
        finally{
            db.endTransaction();
        }
    }

    public int albumUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            int n =  db.update(TableConsts.TBNAME_ALBUM, values, selection,
                    selectionArgs);
            db.setTransactionSuccessful();
            return n;
        }
        finally{
            db.endTransaction();
        }
    }

    public int artistUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            int n = db.update(TableConsts.TBNAME_ARTIST, values, selection,
                    selectionArgs);
            db.setTransactionSuccessful();
            return n;
        }
        finally{
            db.endTransaction();
        }
    }

    public int playlistUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            int n =  db.update(TableConsts.TBNAME_PLAYLIST, values, selection,
                    selectionArgs);
            db.setTransactionSuccessful();
            return n;
        }
        finally{
            db.endTransaction();
        }
    }

    public int playlistmemberUpdate(Uri uri, ContentValues values,
            String selection, String[] selectionArgs) {
        SQLiteDatabase db = null;
        Cursor cur = null;
        try {
            db = getDBHelper().getWritableDatabase();
            db.beginTransaction();
            cur = db.query(TableConsts.TBNAME_PLAYLIST_AUDIO,
                    new String[] {
                        MediaConsts.AudioPlaylistMember.AUDIO_ID
                    },
                    selection, selectionArgs, null, null, null);
            int n;
            if (cur != null && cur.moveToFirst()) {
                n = db.update(TableConsts.TBNAME_PLAYLIST_AUDIO, values,
                        selection, selectionArgs);
                cur.close();
                cur = null;
            } else {
                long id = db.insert(TableConsts.TBNAME_PLAYLIST_AUDIO, null,
                        values);
                n = 1;
            }
            db.setTransactionSuccessful();
            return n;
        } finally {
            db.endTransaction();
            if (cur != null) {
                cur.close();
            }
        }
    }

    public int genresUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            int n = db.update(TableConsts.TBNAME_GENRES, values, selection,
                    selectionArgs);
            db.setTransactionSuccessful();
            return n;
        }
        finally{
            db.endTransaction();
        }
    }

    public Uri downloadInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            long id = db.insert(TableConsts.TBNAME_DOWNLOAD, null, values);
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(MEDIA_CONTENT_URI, id);
        }
        finally{
            db.endTransaction();
        }
    }

    public Uri mediaInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            long id = db.insert(TableConsts.TBNAME_AUDIO, null, values);
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(MEDIA_CONTENT_URI, id);
        }
        finally{
            db.endTransaction();
        }
    }

    public Uri albumInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            long id = db.insert(TableConsts.TBNAME_ALBUM, null, values);
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(ALBUM_CONTENT_URI, id);
        }
        finally{
            db.endTransaction();
        }
    }

    public Uri artistInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            long id = db.insert(TableConsts.TBNAME_ARTIST, null, values);
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(ARTIST_CONTENT_URI, id);
        }
        finally{
            db.endTransaction();
        }
    }

    public Uri playlistInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            long id = db.insert(TableConsts.TBNAME_PLAYLIST, null, values);
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(PLAYLIST_CONTENT_URI, id);
        }
        finally{
            db.endTransaction();
        }
    }

    public Uri playlistmemberInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        long id = ContentUris.parseId(uri);
        long media_id = values.getAsLong(AudioPlaylistMember.AUDIO_ID);
        int order = values.getAsInteger(AudioPlaylistMember.PLAY_ORDER);

        Cursor cur = null;
        boolean hasTransaction = false;
        try {
            SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
            qb.setTables(TableConsts.TBNAME_AUDIO);
            cur = qb.query(db, MEDIA_FIELDS, AudioMedia._ID + " = ?",
                    new String[] {
                        Long.toString(media_id)
                    }, null, null, null);
            if (cur != null && cur.moveToFirst()) {
                String media_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.MEDIA_KEY));
                String title = cur.getString(cur
                        .getColumnIndex(AudioMedia.TITLE));
                String title_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.TITLE_KEY));
                long duration = cur.getLong(cur
                        .getColumnIndex(AudioMedia.DURATION));
                String artist = cur.getString(cur
                        .getColumnIndex(AudioMedia.ARTIST));
                String artist_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.ARTIST_KEY));
                String album = cur.getString(cur
                        .getColumnIndex(AudioMedia.ALBUM));
                String album_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.ALBUM_KEY));
                String data = cur
                        .getString(cur.getColumnIndex(AudioMedia.DATA));
                int tarck = cur.getInt(cur.getColumnIndex(AudioMedia.TRACK));
                long date_added = cur.getLong(cur
                        .getColumnIndex(AudioMedia.DATE_ADDED));
                long date_modified = cur.getLong(cur
                        .getColumnIndex(AudioMedia.DATE_MODIFIED));
                String tags = cur.getString(cur
                        .getColumnIndex(TableConsts.GENRES_TAGS));
                String year = cur.getString(cur
                        .getColumnIndex(AudioPlaylistMember.YEAR));
                int point = cur.getInt(cur
                        .getColumnIndex(TableConsts.FAVORITE_POINT));

                db.beginTransaction();
                hasTransaction = true;
                ContentValues val = new ContentValues();
                val.put(TableConsts.PLAYLIST_INIT_FLG, 1);
                db.update(TableConsts.TBNAME_PLAYLIST, val, AudioPlaylist._ID
                        + "=?", new String[] {
                        Long.toString(id)
                });

                ContentValues dvalues_pl = new ContentValues();
                dvalues_pl.put(AudioPlaylistMember._ID, media_key);
                dvalues_pl.put(AudioPlaylistMember.PLAY_ORDER, order);
                dvalues_pl.put(AudioPlaylistMember.AUDIO_ID, media_id);
                dvalues_pl.put(AudioPlaylistMember.MEDIA_KEY, media_key);
                dvalues_pl.put(AudioPlaylistMember.PLAYLIST_ID, id);
                dvalues_pl.put(AudioPlaylistMember.TITLE, title);
                dvalues_pl.put(AudioPlaylistMember.TITLE_KEY, title_key);
                dvalues_pl.put(AudioPlaylistMember.DURATION, duration);
                dvalues_pl.put(AudioPlaylistMember.ARTIST, artist);
                dvalues_pl.put(AudioPlaylistMember.ARTIST_KEY, artist_key);
                dvalues_pl.put(AudioPlaylistMember.ALBUM, album);
                dvalues_pl.put(AudioPlaylistMember.ALBUM_KEY, album_key);
                dvalues_pl.put(AudioPlaylistMember.DATA, data);
                dvalues_pl.put(AudioPlaylistMember.TRACK, tarck);
                dvalues_pl.put(AudioPlaylistMember.DATE_ADDED, date_added);
                dvalues_pl
                        .put(AudioPlaylistMember.DATE_MODIFIED, date_modified);
                dvalues_pl.put(TableConsts.GENRES_TAGS, tags);
                dvalues_pl.put(AudioPlaylistMember.YEAR, year);
                dvalues_pl.put(TableConsts.FAVORITE_POINT, point);
                long ret_id = db.replace(TableConsts.TBNAME_PLAYLIST_AUDIO,
                        null, dvalues_pl);

                db.setTransactionSuccessful();
                return ContentUris.withAppendedId(
                        MediaConsts.PLAYLIST_MEMBER_CONTENT_URI, ret_id);
            }
        } finally {
            if (cur != null) {
                cur.close();
            }
            if (hasTransaction) {
                db.endTransaction();
            }
        }
        return null;
    }

    public Uri genresInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            long id = db.insert(TableConsts.TBNAME_GENRES, null, values);
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(GENRES_CONTENT_URI, id);
        }
        finally{
            db.endTransaction();
        }
    }

    public Uri genresmemberInsert(Uri uri, ContentValues values) {
        return null;
    }

    public Cursor mediaIdQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        long id = ContentUris.parseId(uri);

        SQLiteDatabase db = null;

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_AUDIO);
        try {
            db = getDBHelper().getWritableDatabase();

            Cursor cur = qb.query(db, projection, MediaConsts.AudioMedia._ID
                    + " = ?", new String[] {
                    Long.toString(id)
            }, null, null,
                    sortOrder);
            return cur;
        } finally {
        }
    }

    public Cursor mediaQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        boolean canUseNetwork = canUseNetwork();

        SQLiteDatabase db = null;

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_AUDIO);
        if (!canUseNetwork) {
            qb.appendWhere(TableConsts.AUDIO_CACHE_FILE + " is not null");
        }
        try {
            db = getDBHelper().getWritableDatabase();
            Cursor cur = qb.query(db, projection, selection, selectionArgs,
                    null, null, sortOrder);
            return cur;
        } finally {
        }
    }

    public Cursor artistQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        SQLiteDatabase db = null;

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_ARTIST);
        try {
            db = getDBHelper().getWritableDatabase();

            Cursor cur = qb.query(db, projection, selection, selectionArgs,
                    null, null, sortOrder);
            return cur;
        } finally {
        }
    }

    public Cursor playlistQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        SQLiteDatabase db = null;

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_PLAYLIST);
        try {
            db = getDBHelper().getWritableDatabase();

            Cursor cur = qb.query(db, projection, selection, selectionArgs,
                    null, null, sortOrder);
            return cur;
        } finally {
        }
    }

    String[] audio_cols = new String[] {
            BaseColumns._ID, AudioMedia.TITLE,
            AudioMedia.TITLE_KEY, AudioMedia.DATA, AudioMedia.DURATION,
            AudioMedia.ARTIST, AudioMedia.ARTIST_KEY, AudioMedia.ALBUM,
            AudioMedia.ALBUM_KEY,
            // AudioMedia.ALBUM_ART,
            AudioMedia.TRACK
    };

    String[] playlist_audio_cols = new String[] {
            BaseColumns._ID,
            AudioPlaylistMember.AUDIO_ID, AudioMedia.TITLE,
            AudioMedia.TITLE_KEY, AudioMedia.DATA, AudioMedia.DURATION,
            AudioMedia.ARTIST, AudioMedia.ARTIST_KEY, AudioMedia.ALBUM,
            AudioMedia.ALBUM_KEY,
            // AudioMedia.ALBUM_ART,
            AudioMedia.TRACK
    };

    public Hashtable<String, String> getMedia(Context contxt,
            String[] projection, long mediaId) {
        Hashtable<String, String> tbl = new Hashtable<String, String>();
        Cursor cur = null;
        try {
            String[] prj = projection;
            if (prj == null) {
                prj = new String[] {
                        AudioMedia.ALBUM, AudioMedia.ALBUM_KEY,
                        AudioMedia.ARTIST, AudioMedia.TITLE,
                        AudioMedia.DURATION
                };
            }
            cur = contxt.getContentResolver().query(
                    ContentUris.withAppendedId(MEDIA_CONTENT_URI, mediaId),
                    prj, MediaConsts.AudioMedia._ID + " = ?", null, null);
            if (cur != null && cur.moveToFirst()) {
                for (int i = 0; i < prj.length; i++) {
                    String value = cur.getString(cur.getColumnIndex(prj[i]));
                    if (value != null) {
                        tbl.put(prj[i], value);
                    }
                }
            }

            return tbl;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public Cursor playlistmemberQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = null;
        long playlist_id = ContentUris.parseId(uri);
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_PLAYLIST_AUDIO);
        qb.appendWhere(AudioPlaylistMember.PLAYLIST_ID + " = " + playlist_id);
        Cursor cur = null;
        try {
            db = getDBHelper().getWritableDatabase();

            // PlaylistMemberからデータを取得し、合致するMedia情報をもとに、返事をかえす
            MatrixCursor ret_cursor = new MatrixCursor(playlist_audio_cols);
            cur = qb.query(db, new String[] {
                    BaseColumns._ID,
                    AudioPlaylistMember.AUDIO_ID,
                    AudioPlaylistMember.PLAY_ORDER
            }, selection, selectionArgs,
                    null, null, sortOrder);
            if (cur != null && cur.moveToFirst()) {
                do {
                    long mediaId = cur.getLong(cur
                            .getColumnIndex(AudioPlaylistMember.AUDIO_ID));
                    Hashtable<String, String> tbl = getMedia(getContext(),
                            audio_cols, mediaId);
                    if (!tbl.isEmpty()) {
                        ret_cursor
                                .addRow(new Object[] {
                                        // Long.parseLong(tbl.get(BaseColumns._ID)),
                                        cur.getLong(cur
                                                .getColumnIndex(BaseColumns._ID)),
                                        tbl.get(AudioMedia._ID),
                                        tbl.get(AudioMedia.TITLE),
                                        tbl.get(AudioMedia.TITLE_KEY),
                                        tbl.get(AudioMedia.DATA),
                                        parseLong(tbl.get(AudioMedia.DURATION)),
                                        tbl.get(AudioMedia.ARTIST),
                                        tbl.get(AudioMedia.ARTIST_KEY),
                                        tbl.get(AudioMedia.ALBUM),
                                        tbl.get(AudioMedia.ALBUM_KEY),
                                        // tbl.get(AudioMedia.ALBUM_ART),
                                        cur.getInt(cur
                                                .getColumnIndex(AudioPlaylistMember.PLAY_ORDER)),
                                });
                    }
                } while (cur.moveToNext());
            }

            return ret_cursor;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public Cursor genresQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        SQLiteDatabase db = null;

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_GENRES);
        try {
            db = getDBHelper().getWritableDatabase();

            Cursor cur = qb.query(db, projection, selection, selectionArgs,
                    null, null, sortOrder);
            return cur;
        } finally {
        }
    }

    String[] genres_audio_cols = new String[] {
            BaseColumns._ID,
            AudioGenresMember.AUDIO_ID, AudioMedia.TITLE, AudioMedia.TITLE_KEY,
            AudioMedia.DATA, AudioMedia.DURATION, AudioMedia.ARTIST,
            AudioMedia.ARTIST_KEY, AudioMedia.ALBUM, AudioMedia.ALBUM_KEY,
            // AudioMedia.ALBUM_ART,
            AudioMedia.TRACK,
    };

    public Cursor genresmemberQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        String[] sel = selection.split("=");
        String genres_key = null;
        for (int i = 0; i < sel.length; i++) {
            if (sel[i].indexOf(AudioGenresMember.GENRE_ID) != -1) {
                genres_key = selectionArgs[i];
                break;
            }
        }

        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        SQLiteQueryBuilder qb2 = new SQLiteQueryBuilder();
        qb2.setTables(TableConsts.TBNAME_AUDIO);
        String limit = null;

        StringBuilder where = new StringBuilder();
        where.append(TableConsts.GENRES_TAGS).append(
                " like '%/" + genres_key + "/%'");

        int pos = selection != null ? selection.indexOf("AND LIMIT") : -1;
        if (pos != -1) {
            // 途中から取得
            String offset = selectionArgs[selectionArgs.length - 1];
            String size = selectionArgs[selectionArgs.length - 2];
            selection = selection.substring(0, pos);
            if (Build.VERSION.SDK_INT < 9) {
                String[] tmp = new String[selectionArgs.length - 2];
                for (int i = 0; i < tmp.length; i++) {
                    tmp[i] = selectionArgs[i];
                }
                selectionArgs = tmp;
            } else {
                selectionArgs = ArrayUtils.fastArrayCopy(selectionArgs,
                        selectionArgs.length - 2);
            }
        }
        // 最初から取得
        Cursor cur = qb2.query(db, MEDIA_FIELDS, where.toString(), null, null,
                null, sortOrder, null);
        return cur;
    }

    private static Long parseLong(String s) {
        try {
            if (s != null) {
                return Long.parseLong(s);
            }
        } catch (Exception e) {
        }
        return null;
    }

    private static Integer parseInt(String s) {
        try {
            if (s != null) {
                return Integer.parseInt(s);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public Cursor albumQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = null;

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_ALBUM);
        try {
            db = getDBHelper().getWritableDatabase();

            Cursor cur = qb.query(db, projection, selection, selectionArgs,
                    null, null, sortOrder);
            return cur;
        } finally {
        }
    }

    public Uri orderAudioInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try{
            db.beginTransaction();
            long id = db.insert(TableConsts.TBNAME_ORDERLIST, null, values);
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(PLAYORDER_CONTENT_URI, id);
        }
        finally{
            db.endTransaction();
        }
    }

    public Uri favoriteInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try {
            db.beginTransaction();
            long id = values.getAsLong(FAVORITE_ID);
            int point = values.getAsInteger(FAVORITE_POINT);
            String type = values.getAsString(FAVORITE_TYPE);
            if (type.equals(FAVORITE_TYPE_SONG)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_AUDIO, s_values, AudioMedia._ID
                        + " = ?", new String[] {
                        Long.toString(id)
                });
            } else if (type.equals(FAVORITE_TYPE_ALBUM)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_ALBUM, s_values, AudioAlbum._ID
                        + " = ?", new String[] {
                        Long.toString(id)
                });
            } else if (type.equals(FAVORITE_TYPE_ARTIST)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_ARTIST, s_values, AudioArtist._ID
                        + " = ?", new String[] {
                        Long.toString(id)
                });
            }
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(MediaConsts.FAVORITE_CONTENT_URI,
                    id);
        } finally {
            db.endTransaction();
        }
    }

    public Uri videoInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = null;
        try {
            db = getDBHelper().getWritableDatabase();
            db.beginTransaction();

            long id = db.insert(TableConsts.TBNAME_VIDEO, null, values);
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(VIDEO_CONTENT_URI, id);
        } finally {
            db.endTransaction();
        }
    }

    public int favoriteUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try {
            db.beginTransaction();
            long id = Long.parseLong(selectionArgs[0]);
            int point = values.getAsInteger(FAVORITE_POINT);
            String type = selectionArgs[1];
            if (type.equals(FAVORITE_TYPE_SONG)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_AUDIO, s_values, AudioMedia._ID
                        + " = ?", new String[] {
                        Long.toString(id)
                });
            } else if (type.equals(FAVORITE_TYPE_ALBUM)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_ALBUM, s_values, AudioAlbum._ID
                        + " = ?", new String[] {
                        Long.toString(id)
                });
            } else if (type.equals(FAVORITE_TYPE_ARTIST)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_ARTIST, s_values, AudioArtist._ID
                        + " = ?", new String[] {
                        Long.toString(id)
                });
            }
            db.setTransactionSuccessful();
            return 1;
        } finally {
            db.endTransaction();
        }
    }

    public int videoUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = null;
        try {
            db = getDBHelper().getWritableDatabase();
            db.beginTransaction();

            int n =  db.update(TableConsts.TBNAME_VIDEO, values, selection,
                    selectionArgs);
            db.setTransactionSuccessful();
            return n;
        } finally {
            db.endTransaction();
        }
    }

    public int videoIdUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = null;
        try {
            long id = ContentUris.parseId(uri);
            db = getDBHelper().getWritableDatabase();
            db.beginTransaction();
            int n = db.update(TableConsts.TBNAME_VIDEO, values, BaseColumns._ID
                    + " = ?", new String[] {
                    Long.toString(id)
            });
            db.setTransactionSuccessful();
            return n;
        } finally {
            db.endTransaction();
        }
    }

    public int downloadUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = null;
        try {
            db = getDBHelper().getWritableDatabase();
            db.beginTransaction();

            int n= db.update(TableConsts.TBNAME_DOWNLOAD, values, selection,
                    selectionArgs);
            db.setTransactionSuccessful();
            return n;
        } finally {
            db.endTransaction();
        }
    }

    private Cursor orderAudioQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        return db.query(TableConsts.TBNAME_ORDERLIST, projection, selection,
                selectionArgs, null, null, sortOrder);
    }

    public Cursor orderAudioIdQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        long id = ContentUris.parseId(uri);
        Cursor cur = db.query(TableConsts.TBNAME_ORDERLIST, projection,
                BaseColumns._ID + " = ?", new String[] {
                    Long.toString(id)
                },
                null, null, sortOrder);
        return cur;
    }

    private Cursor downloadQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        return db.query(TableConsts.TBNAME_DOWNLOAD, projection, selection,
                selectionArgs, null, null, sortOrder);
    }

    public Cursor favoriteQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        MatrixCursor ret = new MatrixCursor(new String[] {
                BaseColumns._ID,
                TableConsts.FAVORITE_POINT
        });
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        Cursor cursor = null;
        try {
            long id = Long.parseLong(selectionArgs[0]);
            String type = selectionArgs[1];
            if (type.equals(FAVORITE_TYPE_SONG)) {
                cursor = db.query(TableConsts.TBNAME_AUDIO, new String[] {
                        BaseColumns._ID, TableConsts.FAVORITE_POINT
                },
                        AudioMedia._ID + " = ?",
                        new String[] {
                            Long.toString(id)
                        }, null, null, null);
            } else if (type.equals(FAVORITE_TYPE_ALBUM)) {
                cursor = db.query(TableConsts.TBNAME_ALBUM, new String[] {
                        BaseColumns._ID, TableConsts.FAVORITE_POINT
                },
                        AudioAlbum._ID + " = ?",
                        new String[] {
                            Long.toString(id)
                        }, null, null, null);
            } else if (type.equals(FAVORITE_TYPE_ARTIST)) {
                cursor = db.query(TableConsts.TBNAME_ARTIST, new String[] {
                        BaseColumns._ID, TableConsts.FAVORITE_POINT
                },
                        AudioArtist._ID + " = ?",
                        new String[] {
                            Long.toString(id)
                        }, null, null, null);
            }
            if (cursor != null && cursor.moveToFirst()) {
                ret.addRow(new Object[] {
                        id,
                        cursor.getInt(cursor
                                .getColumnIndex(TableConsts.FAVORITE_POINT))
                });
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return ret;
    }

    public Cursor videoQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = null;
        try {
            db = getDBHelper().getWritableDatabase();
            Cursor cur = db.query(TableConsts.TBNAME_VIDEO, projection,
                    selection, selectionArgs, null, null, sortOrder);
            return cur;
        } finally {
        }
    }

    public Cursor videoIdQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = null;
        try {
            long id = ContentUris.parseId(uri);
            db = getDBHelper().getWritableDatabase();
            Cursor cur = db.query(TableConsts.TBNAME_VIDEO, projection,
                    BaseColumns._ID + " = ?",
                    new String[] {
                        Long.toString(id)
                    }, null, null, sortOrder);
            return cur;
        } finally {

        }
    }

    // 妥当性をチェックして存在するならそのまま、ないなら消しちゃう
    private void reset() {
        /*
         * // Media SQLiteDatabase db = null; SQLiteQueryBuilder qb = new
         * SQLiteQueryBuilder(); qb.setTables(TableConsts.TBNAME_AUDIO); Cursor
         * cur = null; Map<String, String> album_deletemap = new HashMap<String,
         * String>(); Map<String, String> artist_deletemap = new HashMap<String,
         * String>(); try { db = getDBHelper().getWritableDatabase(); //
         * db.beginTransaction(); cur = qb.query(db, new String[] {
         * AudioMedia._ID, AudioMedia.DATA, AudioMedia.ALBUM_KEY,
         * AudioMedia.ARTIST_KEY, AudioMedia.ARTIST }, null, null, null, null,
         * null); if (cur != null && cur.moveToFirst()) { int colnum1 =
         * cur.getColumnIndex(AudioMedia._ID); int colnum2 =
         * cur.getColumnIndex(AudioMedia.DATA); int colnum3 =
         * cur.getColumnIndex(AudioMedia.ALBUM_KEY); int colnum4 =
         * cur.getColumnIndex(AudioMedia.ARTIST_KEY); int colnum5 =
         * cur.getColumnIndex(AudioMedia.ARTIST); // Audio Reset do { long id =
         * cur.getLong(colnum1); String path = cur.getString(colnum2); String
         * album_key = cur.getString(colnum3); String artist_key =
         * cur.getString(colnum4); String artist = cur.getString(colnum5); try {
         * Entry entry = getDropboxHelper().metadata(path, 0, null, true, null);
         * if (entry == null || entry.isDeleted) {
         * db.delete(TableConsts.TBNAME_AUDIO, AudioMedia._ID + " = ?", new
         * String[] { Long.toString(id) }); album_deletemap.put(new
         * File(entry.path).getParent(), album_key);
         * artist_deletemap.put(artist_key, artist); } } catch (DropboxException
         * e) { } } while (cur.moveToNext()); // Album Reset for
         * (Iterator<String> ite = album_deletemap.keySet().iterator();
         * ite.hasNext();) { String path = ite.next(); Entry album_entry; try {
         * album_entry = getDropboxHelper().metadata( path, 0, null, true,
         * null); if (album_entry == null || album_entry.isDeleted ||
         * album_entry.contents == null || album_entry.contents.size() < 1) {
         * db.delete(TableConsts.TBNAME_ALBUM, AudioAlbum.ALBUM_KEY + " = ?",
         * new String[] { album_deletemap.get(path) }); } } catch
         * (DropboxException e) { e.printStackTrace(); } } // Artist Reset for
         * (Iterator<String> ite = artist_deletemap.keySet().iterator();
         * ite.hasNext();) { String artist_key = ite.next(); String artist =
         * artist_deletemap.get(artist_key); long cnt = getCountByArtist(db,
         * artist); if (cnt < 1) { db.delete(TableConsts.TBNAME_ARTIST,
         * AudioArtist.ARTIST_KEY + " = ?", new String[] { artist_key }); } } }
         * } finally { // db.setTransactionSuccessful(); if (cur != null) {
         * cur.close(); } }
         */
    }

    private long getCountByArtist(SQLiteDatabase db, String artist) {
        Cursor cur = null;
        try {
            cur = db.rawQuery("select count(*) from " + TableConsts.TBNAME_AUDIO + " where "
                    + AudioMedia.ARTIST + " = ?", new String[] {
                    artist
            });
            if (cur != null && cur.moveToFirst()) {
                long count = cur.getLong(0);
                return count;
            }
            return 0;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    private boolean _zipuse = false;

    public Cursor fileQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        String find_path = mPreference.getString("root_path", "/");
        if (selection != null && selection.indexOf(FileMedia.DATA) != -1) {
            String[] sel = selection.split("=");
            String path = null;
            boolean keepPath = false;
            for (int i = 0; i < sel.length; i++) {
                if (sel[i].indexOf(FileMedia.DATA) != -1) {
                    path = selectionArgs[i];
                } else if (sel[i].indexOf(FileMedia.KEEPPATH) != -1) {
                    keepPath = true;
                }
            }
            if (path != null && !path.equals(find_path)) {
                if (!path.equals("..")) {
                    find_path = path;
                } else {
                    find_path = new File(find_path).getParent();
                    // 親がない場合
                    if (find_path == null || find_path.length() == 0) {
                        find_path = "/";
                    }
                }
                if (!keepPath) {
                    Editor editor = mPreference.edit();
                    editor.putString("root_path", find_path);
                    editor.commit();
                }
            }
        }

        MatrixCursor cursor = new MatrixCursor(new String[] {
                FileMedia._ID,
                FileMedia.TITLE, FileMedia.TYPE, FileMedia.DATE_MODIFIED,
                FileMedia.SIZE, FileMedia.DATA
        });

        if (!find_path.equals("/")) {
            cursor.addRow(new Object[] {
                    "..".hashCode(), "..",
                    FileType.FOLDER, 0, 0, ".."
            });
        }

        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        List<PlaylistMetaInfo> playlists = new ArrayList<PlaylistMetaInfo>();
        try {
            Entry entry = getDropboxHelper().metadata(find_path, 0, null, true,
                    null);
            if (entry.contents != null && entry.isDir) {
                String albumArt = null;
                for (Entry child : entry.contents) {
                    String type = getType(child);
                    if (type != null) {
                        // ID3タグ読み取りをつかって情報を作成し返却する
                        if (FileType.AUDIO.equals(type)) {
                            // TODO これはスレッドにするべき
                            boolean hasRegist = isRegisterdSong(
                                    db,
                                    (child.parentPath() + "/" + child
                                            .fileName()).hashCode());
                            if (!hasRegist) {
                                Logger.d("path=" + child.path);
                                if (albumArt == null) {
                                    albumArt = MediaScanner.scanAlbumArt(entry);
                                }
                                executor.execute(new GetMeia(child, albumArt));
                            }
                        } else if (FileType.VIDEO.equals(type)) {
                            boolean hasRegist = isRegisterdVideo(
                                    db,
                                    (child.parentPath() + "/" + child
                                            .fileName()).hashCode());
                            if (!hasRegist) {
                                Logger.d("path=" + child.path);
                                // executor.execute(new GetMeia(child,
                                // albumArt));
                            }
                        } else if (FileType.PLAYLIST.equals(type)){
                            PlaylistMetaInfo ret = new PlaylistMetaInfo();
                            ret.path = child.parentPath();
                            ret.filepath = child.path;
                            ret.name = child.fileName();
                            ret.lastModifiedDate = System.currentTimeMillis();
                            playlists.add(ret);
                        }

                        cursor.addRow(new Object[] {
                                child.path.hashCode(),
                                child.fileName(),
                                type,
                                RESTUtility.parseDate(child.modified).getTime(),
                                child.bytes, child.path
                        });
                    }

                }
            } else if (_zipuse) {
                // no implemented
            }

        } catch (DropboxException e1) {
            Logger.e("Dropbox file query error", e1);
        } finally {
            for(PlaylistMetaInfo meta : playlists){
                registPlaylistInfo(db, meta);
            }
        }

        return cursor;
    }

    private void registPlaylistInfo(SQLiteDatabase db, PlaylistMetaInfo metainf) {
        long playlsitId;
        // 楽曲情報の登録
        playlsitId = findRegisterdPlaylist(db, metainf.filepath);
        if (playlsitId == -1) {
            long playlistid = insertPlaylist(db, metainf);
        } else {
            long playlistid = updatePlaylist(db, playlsitId, metainf);
        }
    }

    final String[] MEDIA_PROJECTION = new String[] {
            AudioMedia._ID,
            AudioMedia.ALBUM, AudioMedia.ALBUM_KEY, AudioMedia.ARTIST,
            AudioMedia.TITLE, AudioMedia.DURATION, AudioMedia.DATA
    };

    public Cursor fileIdQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        MatrixCursor ret_cursor = new MatrixCursor(MEDIA_PROJECTION);
        if (selection != null && selection.indexOf(FileMedia.DATA) != -1) {
            String[] sel = selection.split("=");
            String path = null;
            String type = null;
            for (int i = 0; i < sel.length; i++) {
                if (sel[i].indexOf(FileMedia.DATA) != -1) {
                    path = selectionArgs[i];
                } else if (sel[i].indexOf(FileMedia.TYPE) != -1) {
                    type = selectionArgs[i];
                }
            }
            if (path != null && !path.equals("..")) {
                if (FileType.AUDIO.equals(type)) {
                    Cursor media_cursor = null;
                    SQLiteDatabase db = null;

                    SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
                    qb.setTables(TableConsts.TBNAME_AUDIO);
                    String albumArt = null;
                    try {
                        db = getDBHelper().getWritableDatabase();

                        media_cursor = qb.query(db, MEDIA_PROJECTION,
                                AudioMedia.DATA + " = ?",
                                new String[] {
                                    path
                                }, null, null, sortOrder);
                        if (media_cursor != null && media_cursor.moveToFirst()) {
                            long media_id = media_cursor.getLong(media_cursor
                                    .getColumnIndex(AudioMedia._ID));
                            String album = media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.ALBUM));
                            String album_key = media_cursor
                                    .getString(media_cursor
                                            .getColumnIndex(AudioMedia.ALBUM_KEY));
                            String artist = media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.ARTIST));
                            String title = media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.TITLE));
                            long duration = media_cursor.getLong(media_cursor
                                    .getColumnIndex(AudioMedia.DURATION));
                            String data = media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.DATA));
                            ret_cursor.addRow(new Object[] {
                                    media_id, album,
                                    album_key, artist, title, duration, data
                            });
                        } else {
                            // ない場合はまだスレッドが動いてる可能性がある
                            String[] split_name = path.split("/");
                            if (split_name != null && split_name.length > 1) {
                                long media_id = Funcs.makeSubstansId(path);
                                String album = split_name[split_name.length - 2];
                                String album_key = null;
                                String artist = null;
                                String title = split_name[split_name.length - 1];
                                long duration = 0;// metadata.get("xmpDM:album");
                                String data = path;

                                ret_cursor.addRow(new Object[] {
                                        media_id,
                                        album, album_key, artist, title,
                                        duration, data
                                });
                            }
                        }
                    } catch (IllegalArgumentException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        /*
                         * } catch (DropboxException e) { // TODO Auto-generated
                         * catch block e.printStackTrace();
                         */
                    } finally {
                        if (media_cursor != null) {
                            media_cursor.close();
                        }
                    }
                } else if (FileType.VIDEO.equals(type)) {

                } else {
                    // ZIP
                    /*
                     * ZipFile zipfile=null; InputStream in=null; try { zipfile
                     * = new ZipFile(zipfname); ZipEntry entry =
                     * zipfile.getEntry(fname); in =
                     * zipfile.getInputStream(entry); ContentHandler handler =
                     * new DefaultHandler(); Metadata metadata = new Metadata();
                     * Mp3Parser parser = new Mp3Parser(); ParseContext parseCtx
                     * = new ParseContext(); parser.parse(in, handler, metadata,
                     * parseCtx); long media_id = -1; String album =
                     * metadata.get("xmpDM:album"); String album_key = "";
                     * String artist = metadata.get("xmpDM:artist"); String
                     * title = metadata.get("title"); long duration =0;//
                     * metadata.get("xmpDM:album"); String data = path;
                     * ret_cursor.addRow(new Object[] { media_id, album,
                     * album_key, artist, title, duration, data }); } catch
                     * (IOException e) { e.printStackTrace(); } catch
                     * (SAXException e) { e.printStackTrace(); } catch
                     * (TikaException e) { e.printStackTrace(); } finally{
                     * if(in!=null){ try { in.close(); } catch (IOException e) {
                     * } } if(zipfile!=null){ try { zipfile.close(); } catch
                     * (IOException e) {} } }
                     */
                }
            }
        }

        return ret_cursor;
    }

    private boolean isRegisterdSong(SQLiteDatabase db, long id) {
        // 有無をチェック
        Cursor cur = null;
        try {
            cur = db.query(TableConsts.TBNAME_AUDIO,
                    new String[] {
                        AudioMedia.MEDIA_KEY
                    },
                    AudioMedia.MEDIA_KEY + " = ?",
                    new String[] {
                        Long.toString(id)
                    }, null, null, null);
            if (cur != null && cur.moveToFirst()) {
                return true;
            }
            return false;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    private boolean isRegisterdVideo(SQLiteDatabase db, long id) {
        // 有無をチェック
        Cursor cur = null;
        try {
            cur = db.query(
                    TableConsts.TBNAME_VIDEO,
                    new String[] {
                        AudioMedia.MEDIA_KEY
                    },
                    VideoMedia.MEDIA_KEY + " = ?",
                    new String[] {
                        Long.toString(id)
                    }, null, null, null);
            if (cur != null && cur.moveToFirst()) {
                return true;
            }
            return false;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    private String getType(Entry file) {
        if (file.isDir) {
            return FileType.FOLDER;
        }
        MediaFileType type = MediaFile.getFileType(file.fileName());
        if (type != null) {
            if (MediaFile.isAudioFileType(type.fileType)) {
                return FileType.AUDIO;
            } else if (_zipuse && MediaFile.FILE_TYPE_ZIP == type.fileType) {
                return FileType.ZIP;
            } else if (MediaFile.isVideoFileType(type.fileType)) {
                return FileType.VIDEO;
            } else if (MediaFile.isPlayListFileType(type.fileType)) {
                return FileType.PLAYLIST;
            }
        }
        return null;
    }
    
    private long findRegisterdPlaylist(SQLiteDatabase db, String path) {
        // 有無をチェック
        Cursor cur = null;
        try {
            cur = db.query(
                    TableConsts.TBNAME_PLAYLIST_AUDIO,
                    new String[] {AudioPlaylist._ID},
                    AudioPlaylist.DATA + " = ?",
                    new String[] {
                            path
                    }, null, null, null);
            if (cur != null && cur.moveToFirst()) {
                return cur.getLong(0);
            }
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return -1;
    }
    
    private long insertPlaylist(SQLiteDatabase db, PlaylistMetaInfo metainf) {
        Calendar cal = Calendar.getInstance();

        M3UParser parser = new M3UParser(dropboxHelper);

        Cursor cur = null;
        try {
            parser.processPlayList(metainf);

            ContentValues values = new ContentValues();
            values.put(AudioPlaylist.NAME, metainf.name);
            values.put(AudioPlaylist.DATA, metainf.filepath);
            values.put(AudioPlaylist.DATE_ADDED, cal.getTimeInMillis());
            values.put(AudioPlaylist.DATE_MODIFIED, cal.getTimeInMillis());

            long playlistId = db.insert(TableConsts.TBNAME_PLAYLIST, null, values);
            
            Uri playlisturi = ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI, playlistId);

            int index = 0;
            for (PlaylistEntry entry : parser.getPlaylistEntries()) {
                long audioId = findAudioId(db, metainf.path, entry.fname);
                Logger.d("add new playlist audio "+audioId);
                if(audioId != -1){
                    ContentValues values2 = new ContentValues();
                    values2.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, audioId);
                    values2.put(MediaStore.Audio.Playlists.Members.PLAYLIST_ID, playlistId);
                    values2.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, index + 1);
                    db.insert(TableConsts.TBNAME_PLAYLIST_AUDIO, null, values2);
                    index++;
                }
            }
            return playlistId;
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return -1;
    }

    private long updatePlaylist(SQLiteDatabase db, long playlistId, PlaylistMetaInfo metainf) {
        M3UParser parser = new M3UParser(dropboxHelper);

        Cursor cur = null;
        try {
            parser.processPlayList(metainf);

            db.delete(TableConsts.TBNAME_PLAYLIST, AudioPlaylistMember.PLAYLIST_ID + " = ?",
                    new String[] {
                        Long.toString(playlistId)
                    });
            Uri playlisturi = ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI, playlistId);

            int index = 0;
            for (PlaylistEntry entry : parser.getPlaylistEntries()) {
                long audioId = findAudioId(db, metainf.path, entry.fname);
                if(audioId != -1){
                    ContentValues values2 = new ContentValues();
                    values2.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, audioId);
                    values2.put(MediaStore.Audio.Playlists.Members.PLAYLIST_ID, playlistId);
                    values2.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, index + 1);
                    db.insert(TableConsts.TBNAME_PLAYLIST_AUDIO, null, values2);
                    index++;
                }
            }
            return playlistId;
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return -1;
    }
    
    private long findAudioId(SQLiteDatabase db, String path, String fname) {
        Cursor cursor = null;
        try{
            String fullpath = path+fname;
            Logger.d("query audio key="+fullpath+" "+Funcs.createHashId(fullpath));
            cursor = db.query(TableConsts.TBNAME_AUDIO, new String[]{AudioMedia._ID}, AudioMedia.MEDIA_KEY + " = ?",
                    new String[]{Long.toString(Funcs.createHashId(fullpath))}, null, null, null);
            if(cursor!=null && cursor.moveToFirst()){
                return cursor.getLong(0);
            }
        }
        finally{
            
        }
        return -1;
    }

    private String getType(ZipEntry ze) {
        if (ze.isDirectory()) {
            return "folder";
        }
        MediaFileType type = MediaFile.getFileType(ze.getName());
        if (type != null) {
            if (MediaFile.isAudioFileType(type.fileType)) {
                return "audio";
            } else if (MediaFile.FILE_TYPE_ZIP == type.fileType) {
                return "zip";
            }
        }
        return null;
    }

    private class GetMeia implements Runnable {
        String albumArt;
        Entry entry;

        public GetMeia(Entry entry, String albumArt) {
            this.entry = entry;
            this.albumArt = albumArt;
        }

        @Override
        public void run() {
            FileMetaInfo ret = new FileNameRetriever().get(getDropboxHelper(),
                    entry, true);
            if (ret != null) {
                ret.albumArt = albumArt;
                ContentProviderClient client = null;
                try {
                    client = getDropboxHelper()
                            .getContext()
                            .getContentResolver()
                            .acquireContentProviderClient(MediaConsts.MEDIA_CONTENT_URI);
                    getDropboxHelper().registMetaInfo(client,ret);
                } finally {
                    if (client != null) {
                        client.release();
                    }
                }
                getContext().getContentResolver().notifyChange(MediaConsts.ALBUM_CONTENT_URI, null);
                getContext().getContentResolver()
                        .notifyChange(MediaConsts.ARTIST_CONTENT_URI, null);
                getContext().getContentResolver().notifyChange(MediaConsts.MEDIA_CONTENT_URI, null);
            }
        }
    }
}
