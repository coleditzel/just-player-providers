package jp.co.kayo.android.localplayer.ds.dropbox;

public class ValueRetriever {

    public static int getInt(String s){
        if(s!=null && s.length()>0){
            try{
                return Integer.parseInt(s);
            }
            catch(Exception e){}
        }
        return 0;
    }
    
    public static long getLong(String text) {
        if(text!=null && text.length()>0){
            try {
                return Long.parseLong(text);
            } catch (Exception e) {
    
            }
        }
        return 0;
    }   
    
}
