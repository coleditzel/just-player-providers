package jp.co.kayo.android.localplayer.consts;

public interface SystemConsts {

    public static final String CNF_WIFI_ONLY = "cnf.wifi_only";
    public static final String CNF_TARGETDIR = "key.targetdir";
    public static final String INF_SPACE_USED = "inf.space_used";
    public static final String CNF_EDITMEDIAEXT = "key.editmediaext";
    public static final String CNF_EDITVIDEOEXT = "key.editvideoext";
    public static final String CNF_EDITPLAYLISTEXT = "key.editplaylistext";
    public static final String CNF_SCANCOUNT = "key.scancountdir";
    
    public static final String VIDEO_EXT = ".mp4,.avi";
    public static final String MEDIA_EXT = ".mp3,.m4a,.ogg";
    public static final String PLAYLIST_EXT = ".m3u,.pls,.txt";
}
