package jp.co.kayo.android.localplayer.util;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;

import android.os.Environment;

public class SdCardAccessHelper {
    public static String MEDIA_CACHE = "data/jp.co.kayo.android.localplayer/cache/.mp3/";
    public static String VIDEO_CACHE = "data/jp.co.kayo.android.localplayer/cache/.video/";
    public static String IMAGE_CACHE = "data/jp.co.kayo.android.localplayer/cache/.albumart/";

    public static File cachedMusicDir = new File(
            getExternalStorageDirectory(), MEDIA_CACHE);
    public static File cachedVideoDir = new File(
            getExternalStorageDirectory(), VIDEO_CACHE);
    public static File cachedAlbumartDir = new File(
            getExternalStorageDirectory(), IMAGE_CACHE);
    private static String _path = null;

    public static String getExternalStorageDirectory() {
        if (_path != null) {
            return _path;
        }
        String path = null;
        try {
            File file;
            // F10D 対応
            path = System.getenv("EXTERNAL_SD_STORAGE");
            if (path != null) {
                file = new File(path);
                if (file.exists()) {
                    path = file.getPath();
                    return path;
                }
            }
            file = null;

            // MOTOROLA 対応
            path = System.getenv("EXTERNAL_ALT_STORAGE");
            if (path != null) {
                file = new File(path);
                if (file.exists()) {
                    path = file.getPath();
                    return path;
                }
            }

            // Sumsung 対応
            path = System.getenv("EXTERNAL_STORAGE2");
            if (path != null) {
                file = new File(path);
                if (file.exists()) {
                    path = file.getPath();
                    return path;
                }
            }

            // 旧 Sumsung + 標準 対応
            path = System.getenv("EXTERNAL_STORAGE");
            if (path == null)
                path = Environment.getExternalStorageDirectory().getPath();

            // HTC 対応
            file = new File(path + "/ext_sd");
            if (file.exists()) {
                path = file.getPath();
                return path;
            }

            file = new File(path + "/external_sd");
            if (file.exists()) {
                path = file.getPath();
                return path;
            }

            // その他個別 対応
            file = new File("/mnt/sdcard-ext");
            if (file.exists()) {
                path = file.getPath();
                return path;
            }

            file = new File("/mnt/ext_card");
            if (file.exists()) {
                path = file.getPath();
                return path;
            }

            file = new File("/sdcard/external_sd");
            if (file.exists()) {
                path = file.getPath();
                return path;
            }

            file = new File("/mnt/extSdCard");
            if (file.exists()) {
                path = file.getPath();
                return path;
            }

            file = new File("/mnt/sdcard2");
            if (file.exists()) {
                path = file.getPath();
                return path;
            }

            file = new File(" /mnt/sdcard/removable_sd");
            if (file.exists()) {
                path = file.getPath();
                return path;
            }

            // その他機種
            return path;
        } finally {
            _path = path;
        }
    }

    private static String getFilename(String uri) {
        if (uri != null) {
            String ret = uri.replaceAll("auth=[^&]+", "");
            if (ret.equals(uri)) {
                ret = uri.replaceAll("ssid=[^&]+", "");
            }

            String fname = "media" + Integer.toString(ret.hashCode()) + ".dat";
            // Logger.d("fname = "+ fname);
            return fname;
        } else {
            return null;
        }
    }

    public static File getCacheFile(String uri) {
        String f = getFilename(uri);
        if (f != null) {
            File cacheFile = new File(cachedMusicDir, f);
            return cacheFile;
        }
        return null;
    }

    public static boolean existCachFile(String uri) {
        String f = getFilename(uri);
        if (f != null) {
            File cacheFile = new File(cachedMusicDir, f);
            if (cacheFile != null && cacheFile.exists()) {
                return true;
            }
        }
        return false;
    }

    /**
     * ディレクトリの容量を調査
     * 
     * @param dir
     * @return サイズ
     */
    public long calcDirSize(File dir) {
        long size = 0L;
        if (dir.isDirectory()) {
            File files[] = dir.listFiles();
            if (files != null) {
                for (File f : files) {
                    size += calcDirSize(f);
                }
            }
        } else {
            size = dir.length();
        }

        return size;
    }

    /**
     * 指定したディレクトリとその中身をすべて削除
     * 
     * @param dir
     */
    public static void rmdir(File dir) {
        if (!dir.exists()) {
            return;
        }

        if (dir.isFile()) {
            dir.delete();
        }

        if (dir.isDirectory()) {
            File[] files = dir.listFiles();
            if (files != null) {
                for (File f : files) {
                    rmdir(f);
                }
                dir.delete();
            }
        }
    }
}
