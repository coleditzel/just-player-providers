package jp.co.kayo.android.localplayer.ds.ampache.util;

import java.lang.reflect.Method;

import android.os.Build;

public class StrictHelper {
    public static final boolean STRICTMODE = false;

    public static void registStrictMode() {
        if (Build.VERSION.SDK_INT >= 11) {
            try {
                Class strictModeClass = Class.forName("android.os.StrictMode");
                Class strictModeThreadPolicyClass = Class
                        .forName("android.os.StrictMode$ThreadPolicy");
                Object laxPolicy = strictModeThreadPolicyClass.getField("LAX")
                        .get(null);
                Method method_setThreadPolicy = strictModeClass.getMethod(
                        "setThreadPolicy", strictModeThreadPolicyClass);
                method_setThreadPolicy.invoke(null, laxPolicy);
            } catch (Exception e) {

            }
        }
    }

}
