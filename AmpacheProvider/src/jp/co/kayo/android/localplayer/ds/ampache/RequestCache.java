package jp.co.kayo.android.localplayer.ds.ampache;

import java.io.File;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

import jp.co.kayo.android.localplayer.ds.ampache.bean.Album;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Artist;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Playlist;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Song;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Tag;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Video;
import jp.co.kayo.android.localplayer.ds.ampache.consts.SystemConsts;
import jp.co.kayo.android.localplayer.ds.ampache.consts.TableConsts;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.AudioGenres;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.AudioPlaylist;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.AudioPlaylistMember;
import jp.co.kayo.android.localplayer.ds.ampache.util.Logger;
import jp.co.kayo.android.localplayer.ds.ampache.util.XMLUtils;

import org.xmlpull.v1.XmlPullParser;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Environment;

class RequestCache {
    private Context mContext;
    private SharedPreferences mPreference;
    private AmpacheHelper mAmpacheHelper;
    
    public static File cachedMusicDir = new File(
            Environment.getExternalStorageDirectory(),
            "data/jp.co.kayo.android.localplayer/cache/.mp3/");

    public static final String[] MEDIA_FIELDS = new String[] { AudioMedia._ID,
            AudioMedia.TITLE, AudioMedia.MEDIA_KEY, AudioMedia.TITLE_KEY,
            AudioMedia.DURATION, AudioMedia.DATA, AudioMedia.ARTIST,
            AudioMedia.ARTIST_KEY, AudioMedia.ALBUM, AudioMedia.ALBUM_KEY,
            AudioMedia.ALBUM_ART, AudioMedia.TRACK, AudioMedia.YEAR,
            AudioMedia.DATE_ADDED, AudioMedia.DATE_MODIFIED,
            TableConsts.AUDIO_CACHE_FILE, TableConsts.GENRES_TAGS,
            TableConsts.FAVORITE_POINT };

    public static final String[] ALBUM_FIELDS = new String[] { AudioAlbum._ID,
            AudioAlbum.ALBUM, AudioAlbum.ALBUM_KEY, AudioAlbum.ALBUM_ART,
            AudioAlbum.FIRST_YEAR, AudioAlbum.LAST_YEAR,
            AudioAlbum.NUMBER_OF_SONGS, AudioAlbum.ARTIST,
            AudioAlbum.DATE_ADDED, AudioAlbum.DATE_MODIFIED,
            TableConsts.ALBUM_INIT_FLG, TableConsts.ALBUM_DEL_FLG,
            TableConsts.GENRES_TAGS, TableConsts.FAVORITE_POINT, };

    public static final String[] ARTIST_FIELDS = new String[] {
            AudioArtist._ID, AudioArtist.ARTIST, AudioArtist.ARTIST_KEY,
            AudioArtist.NUMBER_OF_ALBUMS, AudioArtist.NUMBER_OF_TRACKS,
            AudioArtist.DATE_ADDED, AudioArtist.DATE_MODIFIED,
            TableConsts.ARTIST_INIT_FLG, TableConsts.ARTIST_DEL_FLG,
            TableConsts.GENRES_TAGS, TableConsts.FAVORITE_POINT, };

    public static final String[] GENRES_FIELDS = new String[] {
            AudioGenres._ID, AudioGenres.NAME, AudioGenres.GENRES_KEY,
            AudioGenres.DATE_ADDED, AudioGenres.DATE_MODIFIED,
            AudioGenres.NUMBER_OF_ALBUMS, AudioGenres.NUMBER_OF_TRACKS,
            AudioGenres.NUMBER_OF_ARTISTS, AudioGenres.NUMBER_OF_PLAYLISTS,
            AudioGenres.NUMBER_OF_VIDEOS, TableConsts.GENRES_INIT_FLG,
            TableConsts.GENRES_DEL_FLG };

    public static final String[] PLAYLIST_FIELDS = new String[] {
            AudioPlaylist._ID, AudioPlaylist.NAME, AudioPlaylist.PLAYLIST_KEY,
            AudioPlaylist.DATE_ADDED, AudioPlaylist.DATE_MODIFIED,
            TableConsts.PLAYLIST_DEL_FLG, TableConsts.PLAYLIST_INIT_FLG,
            TableConsts.GENRES_TAGS };

    public static final String[] PLAYLIST_MEMBER_FIELDS = new String[] {
            AudioMedia._ID, AudioMedia.TITLE, AudioMedia.MEDIA_KEY,
            AudioMedia.TITLE_KEY, AudioMedia.DURATION, AudioMedia.DATA,
            AudioMedia.ARTIST, AudioMedia.ARTIST_KEY, AudioMedia.ALBUM,
            AudioMedia.ALBUM_KEY, AudioMedia.ALBUM_ART, AudioMedia.TRACK,
            AudioMedia.YEAR, AudioMedia.DATE_ADDED, AudioMedia.DATE_MODIFIED,
            TableConsts.AUDIO_CACHE_FILE, TableConsts.GENRES_TAGS,
            TableConsts.FAVORITE_POINT, AudioPlaylistMember.AUDIO_ID,
            AudioPlaylistMember.PLAYLIST_ID, AudioPlaylistMember.PLAY_ORDER, };

    public RequestCache(Context context, SharedPreferences pref,
            AmpacheHelper helper) {
        this.mContext = context;
        this.mAmpacheHelper = helper;
        this.mPreference = pref;
    }

    String mFilterText;
    
    private static String getFilename(String uri) {
        if (uri != null) {
            String ret = uri.replaceAll("auth=[^&]+", "");
            if (ret.equals(uri)) {
                ret = uri.replaceAll("ssid=[^&]+", "");
            }

            String fname = "media" + Integer.toString(ret.hashCode()) + ".dat";
            return fname;
        } else {
            return null;
        }
    }
    
    public static File getCacheFile(String uri) {
        Logger.d("getCacheFile uri=" + uri);
        String s = getFilename(uri);
        if (s != null) {
            File cacheFile = new File(cachedMusicDir, s);
            Logger.d("getCacheFile trn=" + cacheFile.getPath());
            return cacheFile;
        }
        return null;
    }

    MatrixCursor mediaQuery(final SQLiteDatabase db, String[] projection,
            String selection, String[] selectionArgs, String sortOrder,
            String limit) {
        final String server = mPreference.getString(
                AmpacheHelper.AMPACHE_HOSTNAME, SystemConsts.DEFAULT_HOST);

        boolean search_artistsongs = selection != null ? selection
                .indexOf(AudioMedia.ARTIST_KEY) != -1 : false;
        final boolean search_albumsongs2 = selection != null ? selection
                .indexOf(AudioMedia.ALBUM) != -1 : false;
        final boolean search_artistsongs2 = selection != null ? selection
                .indexOf(AudioMedia.ARTIST) != -1 : false;
        boolean search_findsongs = selection != null ? selection
                .indexOf("like") != -1 : false;
        mFilterText = null;
        String body;
        if (search_artistsongs) {
            body = "server/xml.server.php?action=artist_songs&auth=";
        } else if (search_findsongs || search_albumsongs2
                || search_artistsongs2) {
            body = "server/xml.server.php?action=search_songs&auth=";
        } else {
            body = "server/xml.server.php?action=songs&auth=";
            if (selection == null || selection.length() == 0) {
                // あまりにもひどいのでそのままかえす
                return new MatrixCursor(MEDIA_FIELDS);
            }
        }

        StringBuilder params = new StringBuilder();
        if (selection != null && selection.length() > 0) {
            if (search_findsongs || search_albumsongs2 || search_artistsongs2) {
                if (selectionArgs == null || selectionArgs.length == 0) {
                    int p1 = selection.indexOf('%');
                    int p2 = selection.indexOf('%', p1 + 1);
                    mFilterText = selection.substring(p1 + 1, p2);
                } else {
                    mFilterText = selectionArgs[0];
                }
                params.append("&filter=")
                        .append(URLEncoder.encode(mFilterText));
            }
            String[] sel = selection.split("=");
            for (int i = 0; i < sel.length; i++) {
                if (sel[i].indexOf(AudioAlbum.DATE_ADDED) != -1) {
                    // 登録日時
                    long l = Long.parseLong(selectionArgs[i]);
                    Date date = new Date(l);
                    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
                    params.append("&add=").append(fmt.format(date));
                } else if (sel[i].indexOf(AudioMedia.ALBUM_KEY) != -1) {
                    params.append("&filter=").append(selectionArgs[i]);
                } else if (sel[i].indexOf(AudioMedia.ARTIST_KEY) != -1) {
                    params.append("&filter=").append(selectionArgs[i]);
                }
            }
        }
        if (limit != null) {
            String[] limit_args = limit.split(",");
            params.append("&limit=").append(limit_args[1]);
            params.append("&offset=").append(limit_args[0]);
        }

        final MatrixCursor cursor = new MatrixCursor(MEDIA_FIELDS);

        StringBuilder buf = new StringBuilder();
        AmpacheHelper.appendSubPath(buf, server, body);
        buf.append(mAmpacheHelper.getAuth(false));
        if (params.length() > 0) {
            buf.append(params.toString());
        }

        try {
            db.beginTransaction();
            mAmpacheHelper.parseXml(new URL(buf.toString()), new XMLHandler() {
                Song song = null;
                Calendar cal = Calendar.getInstance();

                @Override
                public boolean startTag(XmlPullParser parser) {
                    String tag = parser.getName();
                    if ("song".equals(tag)) {
                        song = new Song();
                        song.setId(XMLUtils.getAttributeValue(parser, "id"));
                    } else if (song != null) {
                        song.setValue(server, tag, parser);
                    }
                    return true;
                }

                @Override
                public boolean endTag(XmlPullParser parser) {
                    String tag = parser.getName();
                    if (tag.equals("song")) {
                        if (song != null && song.getTitle() != null) {
                            ContentValues dvalues = new ContentValues();
                            dvalues.put(AudioMedia._ID, song.getId());
                            dvalues.put(AudioMedia.MEDIA_KEY, song.getId());
                            dvalues.put(AudioMedia.TITLE, song.getTitle());
                            if (song.getTitle() != null) {
                                dvalues.put(AudioMedia.TITLE_KEY, song
                                        .getTitle().hashCode());
                            }
                            dvalues.put(AudioMedia.DURATION, song.getSongTime());
                            if (song.getArtist() != null) {
                                dvalues.put(AudioMedia.ARTIST, song.getArtist()
                                        .getName());
                                dvalues.put(AudioMedia.ARTIST_KEY, song
                                        .getArtist().getId());
                            }
                            if (song.getAlbum() != null) {
                                dvalues.put(AudioMedia.ALBUM, song.getAlbum()
                                        .getName());
                                dvalues.put(AudioMedia.ALBUM_KEY, song
                                        .getAlbum().getId());
                            }
                            // dvalues.put(AudioMedia.ALBUM_ART, song.getArt());
                            dvalues.put(AudioMedia.DATA, song.getUrl());
                            dvalues.put(AudioMedia.TRACK, song.getTrack());
                            dvalues.put(AudioMedia.DATE_ADDED,
                                    cal.getTimeInMillis());
                            dvalues.put(AudioMedia.DATE_MODIFIED,
                                    cal.getTimeInMillis());
                            dvalues.put(TableConsts.GENRES_TAGS,
                                    song.getTagString());
                            dvalues.put(AudioMedia.YEAR, song.getAlbum()
                                    .getYear());
                            dvalues.put(TableConsts.FAVORITE_POINT,
                                    song.getRating());
                            //キャッシュの有無
                            File cacheFile = RequestCache.getCacheFile(song.getUrl());
                            if(cacheFile!=null && cacheFile.exists()){
                                dvalues.put(TableConsts.AUDIO_CACHE_FILE, cacheFile.getName());
                            }
                            else{
                                dvalues.put(TableConsts.AUDIO_CACHE_FILE, (String)null);
                            }
                            long id = db.replace(TableConsts.TBNAME_AUDIO,
                                    null, dvalues);

                            boolean addReturn = true;
                            if (search_albumsongs2 && mFilterText != null) {
                                if (!song.getAlbum().getId()
                                        .equals(mFilterText)) {
                                    addReturn = false;
                                }
                            } else if (search_artistsongs2
                                    && mFilterText != null) {
                                if (!song.getArtist().getId()
                                        .equals(mFilterText)) {
                                    addReturn = false;
                                }
                            }
                            if (addReturn) {
                                cursor.addRow(new Object[] {
                                        new Integer((int) id),
                                        song.getTitle(),
                                        song.getId(),
                                        Integer.toString(song.getTitle()
                                                .hashCode()),
                                        new Long(song.getSongTime()),
                                        song.getUrl(),
                                        song.getArtist().getName(),
                                        song.getArtist().getId(),
                                        song.getAlbum().getName(),
                                        song.getAlbum().getId(), song.getArt(),
                                        new Integer(song.getTrack()),
                                        song.getAlbum().getYear(),
                                        cal.getTimeInMillis(),
                                        cal.getTimeInMillis(), 0,
                                        song.getTagString(), song.getRating() });
                            }
                        }
                        song = null;
                    }
                    return true;
                }
            });

            db.setTransactionSuccessful();
        } catch (Exception e) {
            Logger.e("getDocument.getDocument", e);
        } finally {
            db.endTransaction();
        }
        return cursor;
    }

    public MatrixCursor albumQuery(SQLiteDatabase db, String[] projection,
            String selection, String[] selectionArgs, String sortOrder,
            String limit) {
        final String server = mPreference.getString(
                AmpacheHelper.AMPACHE_HOSTNAME, SystemConsts.DEFAULT_HOST);
        final String body = "server/xml.server.php?action=albums&auth=";

        final boolean search_albumsongs2 = selection != null ? selection
                .indexOf(AudioAlbum.ALBUM) != -1 : false;
        final boolean search_artistsongs2 = selection != null ? selection
                .indexOf(AudioAlbum.ARTIST) != -1 : false;
        StringBuilder params = new StringBuilder();
        if (selection != null && selection.length() > 0) {
            if (search_albumsongs2 || search_artistsongs2) {
                if (selectionArgs == null || selectionArgs.length == 0) {
                    int p1 = selection.indexOf('%');
                    int p2 = selection.indexOf('%', p1 + 1);
                    mFilterText = selection.substring(p1 + 1, p2);
                } else {
                    mFilterText = selectionArgs[0];
                }
                params.append("&filter=")
                        .append(URLEncoder.encode(mFilterText));
            }
            String[] sel = selection.split("=");
            for (int i = 0; i < sel.length; i++) {
                if (sel[i].indexOf(AudioAlbum.DATE_ADDED) != -1) {
                    // 登録日時
                    long l = Long.parseLong(selectionArgs[i]);
                    Date date = new Date(l);
                    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
                    params.append("&add=").append(fmt.format(date));
                }
            }
        }
        if (limit != null) {
            String[] limit_args = limit.split(",");
            params.append("&limit=").append(limit_args[1]);
            params.append("&offset=").append(limit_args[0]);
        }

        final MatrixCursor cursor = new MatrixCursor(ALBUM_FIELDS);

        StringBuilder buf = new StringBuilder();
        AmpacheHelper.appendSubPath(buf, server, body);
        buf.append(mAmpacheHelper.getAuth(false));
        if (params.length() > 0) {
            buf.append(params.toString());
        }

        try {
            db.beginTransaction();
            mAmpacheHelper.parseXml(new URL(buf.toString()),
                    new AmpacheXMLHandler(server, new MyAmpacheCreator(db,
                            cursor)));

            db.setTransactionSuccessful();
        } catch (Exception e) {
            Logger.e("getDocument.getDocument", e);
        } finally {
            db.endTransaction();
        }
        return cursor;
    }

    public MatrixCursor artistQuery(SQLiteDatabase db, String[] projection,
            String selection, String[] selectionArgs, String sortOrder,
            String limit) {
        final String server = mPreference.getString(
                AmpacheHelper.AMPACHE_HOSTNAME, SystemConsts.DEFAULT_HOST);
        final String body = "server/xml.server.php?action=artists&auth=";
        final boolean search_artistsongs2 = selection != null ? selection
                .indexOf(AudioArtist.ARTIST) != -1 : false;
        StringBuilder params = new StringBuilder();
        if (selection != null && selection.length() > 0) {
            if (search_artistsongs2) {
                if (selectionArgs == null || selectionArgs.length == 0) {
                    int p1 = selection.indexOf('%');
                    int p2 = selection.indexOf('%', p1 + 1);
                    mFilterText = selection.substring(p1 + 1, p2);
                } else {
                    mFilterText = selectionArgs[0];
                }
                params.append("&filter=")
                        .append(URLEncoder.encode(mFilterText));
            }
            String[] sel = selection.split("=");
            for (int i = 0; i < sel.length; i++) {
                if (sel[i].indexOf(AudioAlbum.DATE_ADDED) != -1) {
                    // 登録日時
                    long l = Long.parseLong(selectionArgs[i]);
                    Date date = new Date(l);
                    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
                    params.append("&add=").append(fmt.format(date));
                }
            }
        }
        if (limit != null) {
            String[] limit_args = limit.split(",");
            params.append("&limit=").append(limit_args[1]);
            params.append("&offset=").append(limit_args[0]);
        }

        final MatrixCursor cursor = new MatrixCursor(ARTIST_FIELDS);

        StringBuilder buf = new StringBuilder();
        AmpacheHelper.appendSubPath(buf, server, body);
        buf.append(mAmpacheHelper.getAuth(false));
        if (params.length() > 0) {
            buf.append(params.toString());
        }

        try {
            db.beginTransaction();
            mAmpacheHelper.parseXml(new URL(buf.toString()),
                    new AmpacheXMLHandler(server, new MyAmpacheCreator(db,
                            cursor)));

            db.setTransactionSuccessful();
        } catch (Exception e) {
            Logger.e("getDocument.getDocument", e);
        } finally {
            db.endTransaction();
        }
        return cursor;
    }

    public MatrixCursor genresQuery(SQLiteDatabase db, String[] projection,
            String selection, String[] selectionArgs, String sortOrder,
            String limit) {
        final String server = mPreference.getString(
                AmpacheHelper.AMPACHE_HOSTNAME, SystemConsts.DEFAULT_HOST);
        final String body = "server/xml.server.php?action=tags&auth=";
        final boolean search_tag2 = selection != null ? selection
                .indexOf(AudioGenres.NAME) != -1 : false;
        StringBuilder params = new StringBuilder();
        if (selection != null && selection.length() > 0) {
            if (search_tag2) {
                if (selectionArgs == null || selectionArgs.length == 0) {
                    int p1 = selection.indexOf('%');
                    int p2 = selection.indexOf('%', p1 + 1);
                    mFilterText = selection.substring(p1 + 1, p2);
                } else {
                    mFilterText = selectionArgs[0];
                }
                params.append("&filter=")
                        .append(URLEncoder.encode(mFilterText));
            }
            String[] sel = selection.split("=");
            for (int i = 0; i < sel.length; i++) {
                if (sel[i].indexOf(AudioAlbum.DATE_ADDED) != -1) {
                    // 登録日時
                    long l = Long.parseLong(selectionArgs[i]);
                    Date date = new Date(l);
                    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
                    params.append("&add=").append(fmt.format(date));
                }
            }
        }
        if (limit != null) {
            String[] limit_args = limit.split(",");
            params.append("&limit=").append(limit_args[1]);
            params.append("&offset=").append(limit_args[0]);
        }

        final MatrixCursor cursor = new MatrixCursor(GENRES_FIELDS);

        StringBuilder buf = new StringBuilder();
        AmpacheHelper.appendSubPath(buf, server, body);
        buf.append(mAmpacheHelper.getAuth(false));
        if (params.length() > 0) {
            buf.append(params.toString());
        }

        try {
            db.beginTransaction();
            mAmpacheHelper.parseXml(new URL(buf.toString()),
                    new AmpacheXMLHandler(server, new MyAmpacheCreator(db,
                            cursor)));

            db.setTransactionSuccessful();
        } catch (Exception e) {
            Logger.e("getDocument.getDocument", e);
        } finally {
            db.endTransaction();
        }
        return cursor;
    }

    public MatrixCursor playlistQuery(SQLiteDatabase db, String[] projection,
            String selection, String[] selectionArgs, String sortOrder,
            String limit) {
        final String server = mPreference.getString(
                AmpacheHelper.AMPACHE_HOSTNAME, SystemConsts.DEFAULT_HOST);
        final String body = "server/xml.server.php?action=playlists&auth=";

        final boolean search_playlist2 = selection != null ? selection
                .indexOf(AudioPlaylist.NAME) != -1 : false;
        StringBuilder params = new StringBuilder();
        if (selection != null && selection.length() > 0) {
            if (search_playlist2) {
                if (selectionArgs == null || selectionArgs.length == 0) {
                    int p1 = selection.indexOf('%');
                    int p2 = selection.indexOf('%', p1 + 1);
                    mFilterText = selection.substring(p1 + 1, p2);
                } else {
                    mFilterText = selectionArgs[0];
                }
                params.append("&filter=")
                        .append(URLEncoder.encode(mFilterText));
            }
            String[] sel = selection.split("=");
            for (int i = 0; i < sel.length; i++) {
                if (sel[i].indexOf(AudioAlbum.DATE_ADDED) != -1) {
                    // 登録日時
                    long l = Long.parseLong(selectionArgs[i]);
                    Date date = new Date(l);
                    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
                    params.append("&add=").append(fmt.format(date));
                }
            }
        }
        if (limit != null) {
            String[] limit_args = limit.split(",");
            params.append("&limit=").append(limit_args[1]);
            params.append("&offset=").append(limit_args[0]);
        }

        final MatrixCursor cursor = new MatrixCursor(PLAYLIST_FIELDS);

        StringBuilder buf = new StringBuilder();
        AmpacheHelper.appendSubPath(buf, server, body);
        buf.append(mAmpacheHelper.getAuth(false));
        if (params.length() > 0) {
            buf.append(params.toString());
        }

        try {
            db.beginTransaction();
            mAmpacheHelper.parseXml(new URL(buf.toString()),
                    new AmpacheXMLHandler(server, new MyAmpacheCreator(db,
                            cursor)));

            db.setTransactionSuccessful();
        } catch (Exception e) {
            Logger.e("getDocument.getDocument", e);
        } finally {
            db.endTransaction();
        }
        return cursor;
    }

    public String getAttributeValue(XmlPullParser parser, String name) {
        for (int i = 0; i < parser.getAttributeCount(); i++) {
            if (parser.getAttributeName(i).equals(name)) {
                return parser.getAttributeValue(i);
            }
        }
        return null;
    }

    synchronized MatrixCursor mediaQueryAlbum(final SQLiteDatabase db,
            String album_key, Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        final String server = mPreference.getString(
                AmpacheHelper.AMPACHE_HOSTNAME, SystemConsts.DEFAULT_HOST);

        String body = "server/xml.server.php?action=album_songs&auth=";

        StringBuilder params = new StringBuilder();
        params.append("&filter=").append(album_key);

        final MatrixCursor cursor = new MatrixCursor(MEDIA_FIELDS);

        StringBuilder buf = new StringBuilder();
        AmpacheHelper.appendSubPath(buf, server, body);
        buf.append(mAmpacheHelper.getAuth(false));
        if (params.length() > 0) {
            buf.append(params.toString());
        }

        try {
            db.beginTransaction();

            mAmpacheHelper.parseXml(new URL(buf.toString()), new XMLHandler() {
                int track=0;
                Song song = null;
                Calendar cal = Calendar.getInstance();
                
                private int incTrack(String s){
                    track++;
                    if(s!=null && s.length()>0){
                        return Integer.parseInt(s);
                    }
                    else{
                        return track;
                    }
                }

                @Override
                public boolean startTag(XmlPullParser parser) {
                    String tag = parser.getName();
                    if ("song".equals(tag)) {
                        song = new Song();
                        song.setId(XMLUtils.getAttributeValue(parser, "id"));
                    } else if (song != null) {
                        song.setValue(server, tag, parser);
                    }
                    return true;
                }

                @Override
                public boolean endTag(XmlPullParser parser) {
                    String tag = parser.getName();
                    if (tag.equals("song")) {
                        if (song != null && song.getTitle() != null) {
                            ContentValues dvalues = new ContentValues();
                            dvalues.put(AudioMedia._ID, song.getId());
                            dvalues.put(AudioMedia.MEDIA_KEY, song.getId());
                            dvalues.put(AudioMedia.TITLE, song.getTitle());
                            if (song.getTitle() != null) {
                                dvalues.put(AudioMedia.TITLE_KEY, song
                                        .getTitle().hashCode());
                            }
                            dvalues.put(AudioMedia.DURATION, song.getSongTime());
                            if (song.getArtist() != null) {
                                dvalues.put(AudioMedia.ARTIST, song.getArtist()
                                        .getName());
                                dvalues.put(AudioMedia.ARTIST_KEY, song
                                        .getArtist().getId());
                            }
                            if (song.getAlbum() != null) {
                                dvalues.put(AudioMedia.ALBUM, song.getAlbum()
                                        .getName());
                                dvalues.put(AudioMedia.ALBUM_KEY, song
                                        .getAlbum().getId());
                            }
                            dvalues.put(AudioMedia.DATA, song.getUrl());
                            dvalues.put(AudioMedia.TRACK, song.getTrack());
                            dvalues.put(AudioMedia.DATE_ADDED,
                                    cal.getTimeInMillis());
                            dvalues.put(AudioMedia.DATE_MODIFIED,
                                    cal.getTimeInMillis());
                            dvalues.put(TableConsts.GENRES_TAGS,
                                    song.getTagString());
                            dvalues.put(AudioMedia.YEAR, song.getAlbum()
                                    .getYear());
                            dvalues.put(TableConsts.FAVORITE_POINT,
                                    song.getRating());
                            //キャッシュの有無
                            File cacheFile = RequestCache.getCacheFile(song.getUrl());
                            if(cacheFile!=null && cacheFile.exists()){
                                dvalues.put(TableConsts.AUDIO_CACHE_FILE, cacheFile.getName());
                            }
                            else{
                                dvalues.put(TableConsts.AUDIO_CACHE_FILE, (String)null);
                            }
                            long id = db.replace(TableConsts.TBNAME_AUDIO,
                                    null, dvalues);
                            cursor.addRow(new Object[] {
                                    new Integer((int) id),
                                    song.getTitle(),
                                    song.getId(),
                                    Integer.toString(song.getTitle().hashCode()),
                                    new Long(song.getSongTime()),
                                    song.getUrl(), song.getArtist().getName(),
                                    song.getArtist().getId(),
                                    song.getAlbum().getName(),
                                    song.getAlbum().getId(), song.getArt(),
                                    incTrack(song.getTrack()),
                                    song.getAlbum().getYear(),
                                    cal.getTimeInMillis(),
                                    cal.getTimeInMillis(), 0,
                                    song.getTagString(), song.getRating() });
                        }
                        song = null;
                    }
                    return true;
                }
            });
            if (cursor.getCount() > 0) {
                ContentValues val = new ContentValues();
                val.put(TableConsts.ALBUM_INIT_FLG, 1);
                db.update(TableConsts.TBNAME_ALBUM, val, AudioAlbum.ALBUM_KEY
                        + "=?", new String[] { album_key });
            }

            db.setTransactionSuccessful();
        } catch (Exception e) {
            Logger.e("getDocument.getDocument", e);
        } finally {
            db.endTransaction();
        }
        return cursor;
    }

    MatrixCursor genresmemberQuery(final SQLiteDatabase db,
            String[] projection, String selection, String[] selectionArgs,
            String sortOrder, String limit) {

        final String server = mPreference.getString(
                AmpacheHelper.AMPACHE_HOSTNAME, SystemConsts.DEFAULT_HOST);
        StringBuilder buf = new StringBuilder();
        AmpacheHelper.appendSubPath(buf, server,
                "server/xml.server.php?action=tag_songs&auth=");
        buf.append(mAmpacheHelper.getAuth(false));
        buf.append("&filter=").append(selectionArgs[0]);
        if (limit != null) {
            String[] limit_args = limit.split(",");
            buf.append("&limit=").append(limit_args[1]);
            buf.append("&offset=").append(limit_args[0]);
        }
        Logger.d("genres member url = " + buf.toString());

        final MatrixCursor cursor = new MatrixCursor(MEDIA_FIELDS);
        try {
            db.beginTransaction();
            mAmpacheHelper.parseXml(new URL(buf.toString()), new XMLHandler() {
                Song song = null;
                Calendar cal = Calendar.getInstance();

                @Override
                public boolean startTag(XmlPullParser parser) {
                    String tag = parser.getName();
                    Logger.d("start tag=" + tag);
                    if ("song".equals(tag)) {
                        song = new Song();
                        song.setId(XMLUtils.getAttributeValue(parser, "id"));
                    } else if (song != null) {
                        song.setValue(server, tag, parser);
                    }
                    return true;
                }

                @Override
                public boolean endTag(XmlPullParser parser) {
                    String tag = parser.getName();
                    if (tag.equals("song")) {
                        if (song != null && song.getTitle() != null) {
                            ContentValues dvalues = new ContentValues();
                            dvalues.put(AudioMedia._ID, song.getId());
                            dvalues.put(AudioMedia.MEDIA_KEY, song.getId());
                            dvalues.put(AudioMedia.TITLE, song.getTitle());
                            if (song.getTitle() != null) {
                                dvalues.put(AudioMedia.TITLE_KEY, song
                                        .getTitle().hashCode());
                            }
                            dvalues.put(AudioMedia.DURATION, song.getSongTime());
                            if (song.getArtist() != null) {
                                dvalues.put(AudioMedia.ARTIST, song.getArtist()
                                        .getName());
                                dvalues.put(AudioMedia.ARTIST_KEY, song
                                        .getArtist().getId());
                            }
                            if (song.getAlbum() != null) {
                                dvalues.put(AudioMedia.ALBUM, song.getAlbum()
                                        .getName());
                                dvalues.put(AudioMedia.ALBUM_KEY, song
                                        .getAlbum().getId());
                            }
                            // dvalues.put(AudioMedia.ALBUM_ART, song.getArt());
                            dvalues.put(AudioMedia.DATA, song.getUrl());
                            dvalues.put(AudioMedia.TRACK, song.getTrack());
                            dvalues.put(AudioMedia.DATE_ADDED,
                                    cal.getTimeInMillis());
                            dvalues.put(AudioMedia.DATE_MODIFIED,
                                    cal.getTimeInMillis());
                            dvalues.put(TableConsts.GENRES_TAGS,
                                    song.getTagString());
                            dvalues.put(AudioMedia.YEAR, song.getAlbum()
                                    .getYear());
                            dvalues.put(TableConsts.FAVORITE_POINT,
                                    song.getRating());
                            //キャッシュの有無
                            File cacheFile = RequestCache.getCacheFile(song.getUrl());
                            if(cacheFile!=null && cacheFile.exists()){
                                dvalues.put(TableConsts.AUDIO_CACHE_FILE, cacheFile.getName());
                            }
                            else{
                                dvalues.put(TableConsts.AUDIO_CACHE_FILE, (String)null);
                            }
                            long id = db.replace(TableConsts.TBNAME_AUDIO,
                                    null, dvalues);
                            cursor.addRow(new Object[] {
                                    new Integer((int) id),
                                    song.getTitle(),
                                    song.getId(),
                                    Integer.toString(song.getTitle().hashCode()),
                                    new Long(song.getSongTime()),
                                    song.getUrl(), song.getArtist().getName(),
                                    song.getArtist().getId(),
                                    song.getAlbum().getName(),
                                    song.getAlbum().getId(), song.getArt(),
                                    new Integer(song.getTrack()),
                                    song.getAlbum().getYear(),
                                    cal.getTimeInMillis(),
                                    cal.getTimeInMillis(), 0,
                                    song.getTagString(), song.getRating(), });
                        }
                        song = null;
                    }
                    return true;
                }
            });

            db.setTransactionSuccessful();
            return cursor;
        } catch (Exception e) {
            Logger.e("getDocument.getDocument", e);
        } finally {
            db.endTransaction();
        }
        return null;
    }

    MatrixCursor playlistmemberQuery(final SQLiteDatabase db,
            final String playlist_key, final long playlist_id) {

        final String server = mPreference.getString(
                AmpacheHelper.AMPACHE_HOSTNAME, SystemConsts.DEFAULT_HOST);
        StringBuilder buf = new StringBuilder();
        AmpacheHelper.appendSubPath(buf, server,
                "server/xml.server.php?action=playlist_songs&auth=");
        buf.append(mAmpacheHelper.getAuth(false));
        buf.append("&filter=").append(playlist_key);


        final MatrixCursor cursor = new MatrixCursor(PLAYLIST_MEMBER_FIELDS);
        try {
            db.beginTransaction();
            mAmpacheHelper.parseXml(new URL(buf.toString()), new XMLHandler() {
                Song song = null;
                Calendar cal = Calendar.getInstance();
                int i;

                @Override
                public boolean startTag(XmlPullParser parser) {
                    String tag = parser.getName();
                    if ("song".equals(tag)) {
                        song = new Song();
                        song.setId(XMLUtils.getAttributeValue(parser, "id"));
                    } else if (song != null) {
                        song.setValue(server, tag, parser);
                    }
                    return true;
                }

                @Override
                public boolean endTag(XmlPullParser parser) {
                    String tag = parser.getName();
                    if (tag.equals("song")) {
                        if (song != null && song.getTitle() != null) {
                            ContentValues dvalues = new ContentValues();
                            dvalues.put(AudioMedia._ID, song.getId());
                            dvalues.put(AudioMedia.MEDIA_KEY, song.getId());
                            dvalues.put(AudioMedia.TITLE, song.getTitle());
                            if (song.getTitle() != null) {
                                dvalues.put(AudioMedia.TITLE_KEY, song
                                        .getTitle().hashCode());
                            }
                            dvalues.put(AudioMedia.DURATION, song.getSongTime());
                            if (song.getArtist() != null) {
                                dvalues.put(AudioMedia.ARTIST, song.getArtist()
                                        .getName());
                                dvalues.put(AudioMedia.ARTIST_KEY, song
                                        .getArtist().getId());
                            }
                            if (song.getAlbum() != null) {
                                dvalues.put(AudioMedia.ALBUM, song.getAlbum()
                                        .getName());
                                dvalues.put(AudioMedia.ALBUM_KEY, song
                                        .getAlbum().getId());
                            }
                            dvalues.put(AudioMedia.DATA, song.getUrl());
                            dvalues.put(AudioMedia.TRACK, song.getTrack());
                            dvalues.put(AudioMedia.DATE_ADDED,
                                    cal.getTimeInMillis());
                            dvalues.put(AudioMedia.DATE_MODIFIED,
                                    cal.getTimeInMillis());
                            dvalues.put(TableConsts.GENRES_TAGS,
                                    song.getTagString());
                            dvalues.put(AudioMedia.YEAR, song.getAlbum()
                                    .getYear());
                            dvalues.put(TableConsts.FAVORITE_POINT,
                                    song.getRating());
                            //キャッシュの有無
                            File cacheFile = RequestCache.getCacheFile(song.getUrl());
                            if(cacheFile!=null && cacheFile.exists()){
                                dvalues.put(TableConsts.AUDIO_CACHE_FILE, cacheFile.getName());
                            }
                            else{
                                dvalues.put(TableConsts.AUDIO_CACHE_FILE, (String)null);
                            }
                            long med_id = db.replace(TableConsts.TBNAME_AUDIO,
                                    null, dvalues);

                            ContentValues dvalues_pl = new ContentValues();
                            dvalues_pl.put(AudioPlaylistMember._ID,
                                    song.getId());
                            dvalues_pl
                                    .put(AudioPlaylistMember.AUDIO_ID, med_id);
                            dvalues_pl.put(AudioPlaylistMember.MEDIA_KEY,
                                    song.getId());
                            dvalues_pl.put(AudioPlaylistMember.PLAYLIST_ID,
                                    playlist_key);
                            dvalues_pl.put(AudioPlaylistMember.PLAY_ORDER, i);
                            dvalues_pl.put(AudioPlaylistMember.TITLE,
                                    song.getTitle());
                            if (song.getTitle() != null) {
                                dvalues_pl.put(AudioPlaylistMember.TITLE_KEY,
                                        song.getTitle().hashCode());
                            }
                            dvalues_pl.put(AudioPlaylistMember.DURATION,
                                    song.getSongTime());
                            if (song.getArtist() != null) {
                                dvalues_pl.put(AudioPlaylistMember.ARTIST, song
                                        .getArtist().getName());
                                dvalues_pl.put(AudioPlaylistMember.ARTIST_KEY,
                                        song.getArtist().getId());
                            }
                            if (song.getAlbum() != null) {
                                dvalues_pl.put(AudioPlaylistMember.ALBUM, song
                                        .getAlbum().getName());
                                dvalues_pl.put(AudioPlaylistMember.ALBUM_KEY,
                                        song.getAlbum().getId());
                            }
                            dvalues_pl.put(AudioPlaylistMember.DATA,
                                    song.getUrl());
                            dvalues_pl.put(AudioPlaylistMember.TRACK,
                                    song.getTrack());
                            dvalues_pl.put(AudioPlaylistMember.DATE_ADDED,
                                    cal.getTimeInMillis());
                            dvalues_pl.put(AudioPlaylistMember.DATE_MODIFIED,
                                    cal.getTimeInMillis());
                            dvalues_pl.put(TableConsts.GENRES_TAGS,
                                    song.getTagString());
                            dvalues_pl.put(AudioPlaylistMember.YEAR, song
                                    .getAlbum().getYear());
                            dvalues_pl.put(TableConsts.FAVORITE_POINT,
                                    song.getRating());
                            long id = db.replace(
                                    TableConsts.TBNAME_PLAYLIST_AUDIO, null,
                                    dvalues_pl);

                            cursor.addRow(new Object[] {
                                    new Integer((int) id),
                                    song.getTitle(),
                                    song.getId(),
                                    Integer.toString(song.getTitle().hashCode()),
                                    new Long(song.getSongTime()),
                                    song.getUrl(), song.getArtist().getName(),
                                    song.getArtist().getId(),
                                    song.getAlbum().getName(),
                                    song.getAlbum().getId(), song.getArt(),
                                    new Integer(song.getTrack()),
                                    song.getAlbum().getYear(),
                                    cal.getTimeInMillis(),
                                    cal.getTimeInMillis(), 0,
                                    song.getTagString(), song.getRating(),
                                    song.getId(), playlist_id, i++ });
                        }
                        song = null;
                    }
                    return true;
                }
            });

            if (cursor.getCount() > 0) {
                ContentValues val = new ContentValues();
                val.put(TableConsts.PLAYLIST_INIT_FLG, 1);
                db.update(TableConsts.TBNAME_PLAYLIST, val,
                        AudioPlaylist.PLAYLIST_KEY + "=?",
                        new String[] { playlist_key });
            }

            db.setTransactionSuccessful();
            return cursor;
        } catch (Exception e) {
            Logger.e("getDocument.getDocument", e);
        } finally {
            db.endTransaction();
        }
        return cursor;
    }

    class MyAmpacheCreator implements Creator {
        int mOffset = 0;
        int mAlbumCount = 0;
        int mArtistCount = 0;
        int mTagCount = 0;
        int mPlaylistCount = 0;
        int mVideoCount = 0;
        int mSongCount = 0;
        SQLiteDatabase mDb;
        MatrixCursor mCursor;
        HashSet<String> albumkeys = new HashSet<String>();
        HashSet<String> artistkeys = new HashSet<String>();
        HashSet<String> tagkeys = new HashSet<String>();
        Calendar cal = Calendar.getInstance();

        MyAmpacheCreator(SQLiteDatabase db, MatrixCursor cursor) {
            mDb = db;
            mCursor = cursor;
        }

        @Override
        public void createAlbum(Album album) {
            long id = mAmpacheHelper.insertAlbum(mDb, album);
            mAlbumCount++;
            mCursor.addRow(new Object[] {
                    new Integer((int) id),
                    album.getName(),
                    album.getId(),
                    album.getArt(),
                    album.getYear(),
                    album.getYear(),
                    album.getNumTrack(),
                    album.getArtist() != null ? album.getArtist().getName()
                            : null, cal.getTimeInMillis(),
                    cal.getTimeInMillis(), 0, 0, null, album.getRating() });
        }

        @Override
        public void createArtist(Artist artist) {
            long id = mAmpacheHelper.insertArtist(mDb, artist);
            mArtistCount++;
            mCursor.addRow(new Object[] { new Integer((int) id),
                    artist.getName(), artist.getId(), artist.getNumAlbums(),
                    artist.getNumSong(), cal.getTimeInMillis(),
                    cal.getTimeInMillis(), 0, 0, null, artist.getRating() });
        }

        @Override
        public void createTag(Tag tag) {
            long id = mAmpacheHelper.insertGenres(mDb, tag);
            mTagCount++;
            mCursor.addRow(new Object[] { new Integer((int) id), tag.getName(),
                    tag.getId(), cal.getTimeInMillis(), cal.getTimeInMillis(),
                    tag.getNumAlbum(), tag.getNumArtist(), tag.getNumSong(),
                    tag.getNumPlaylist(), tag.getNumVideo(), 0, 0 });
        }

        @Override
        public void createPlaylist(Playlist playlist) {
            long id = mAmpacheHelper.insertPlaylist(mDb, playlist);
            mPlaylistCount++;
            mCursor.addRow(new Object[] { new Integer((int) id),
                    playlist.getName(), playlist.getId(),
                    cal.getTimeInMillis(), cal.getTimeInMillis(), 0, 0, null });
        }

        @Override
        public void createVideo(Video video) {
            mAmpacheHelper.insertVideo(mDb, video);
            mVideoCount++;
        }

        @Override
        public void createSong(Song song) {
            ContentValues dvalues = new ContentValues();
            dvalues.put(AudioMedia._ID, song.getId());
            dvalues.put(AudioMedia.MEDIA_KEY, song.getId());
            dvalues.put(AudioMedia.TITLE, song.getTitle());
            if (song.getTitle() != null) {
                dvalues.put(AudioMedia.TITLE_KEY, song.getTitle().hashCode());
            }
            dvalues.put(AudioMedia.DURATION, song.getSongTime());
            if (song.getArtist() != null) {
                dvalues.put(AudioMedia.ARTIST, song.getArtist().getName());
                dvalues.put(AudioMedia.ARTIST_KEY, song.getArtist().getId());
                artistkeys.add(song.getArtist().getId());
            }
            if (song.getAlbum() != null) {
                dvalues.put(AudioMedia.ALBUM, song.getAlbum().getName());
                dvalues.put(AudioMedia.ALBUM_KEY, song.getAlbum().getId());
                albumkeys.add(song.getAlbum().getId());
            }
            if (song.getTags() != null && song.getTags().size() > 0) {
                for (Tag t : song.getTags()) {
                    if (t.getId() != null && t.getId().length() > 0) {
                        tagkeys.add(t.getId());
                    }
                }
            }
            // dvalues.put(AudioMedia.ALBUM_ART,
            // song.getArt());
            dvalues.put(AudioMedia.DATA, song.getUrl());
            dvalues.put(AudioMedia.TRACK, song.getTrack());
            dvalues.put(AudioMedia.DATE_ADDED, cal.getTimeInMillis());
            dvalues.put(AudioMedia.DATE_MODIFIED, cal.getTimeInMillis());
            dvalues.put(TableConsts.GENRES_TAGS, song.getTagString());
            if (song.getAlbum() != null) {
                dvalues.put(AudioMedia.YEAR, song.getAlbum().getYear());
            }
            dvalues.put(TableConsts.FAVORITE_POINT, song.getRating());
            //キャッシュの有無
            File cacheFile = RequestCache.getCacheFile(song.getUrl());
            if(cacheFile!=null && cacheFile.exists()){
                dvalues.put(TableConsts.AUDIO_CACHE_FILE, cacheFile.getName());
            }
            else{
                dvalues.put(TableConsts.AUDIO_CACHE_FILE, (String)null);
            }
            long id = mDb.replace(TableConsts.TBNAME_AUDIO, null, dvalues);

            mSongCount++;
        }

    }
}
