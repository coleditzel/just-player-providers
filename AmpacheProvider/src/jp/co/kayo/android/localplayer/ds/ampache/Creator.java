package jp.co.kayo.android.localplayer.ds.ampache;

import jp.co.kayo.android.localplayer.ds.ampache.bean.Album;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Artist;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Playlist;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Song;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Tag;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Video;

public interface Creator {

    void createAlbum(Album album);

    void createArtist(Artist artist);

    void createTag(Tag tag);

    void createPlaylist(Playlist playlist);

    void createVideo(Video video);

    void createSong(Song song);

}
