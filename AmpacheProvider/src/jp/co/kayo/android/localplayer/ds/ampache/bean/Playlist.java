package jp.co.kayo.android.localplayer.ds.ampache.bean;

import java.util.ArrayList;

import jp.co.kayo.android.localplayer.ds.ampache.util.ValueRetriever;
import jp.co.kayo.android.localplayer.ds.ampache.util.XMLUtils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;

public class Playlist {
    private String mId;
    private String mName;
    private String mOwner;
    private String mItems;
    private ArrayList<Tag> mTags = new ArrayList<Tag>();
    private String mType;

    public void setValue(String server, String tag, XmlPullParser parser) {
        if (tag.equals("name")) {
            setName(XMLUtils.getTextValue(parser));
        } else if (tag.equals("owner")) {
            setOwner(XMLUtils.getTextValue(parser));
        } else if (tag.equals("items")) {
            setItems(XMLUtils.getTextValue(parser));
        } else if (tag.equals("tag")) {
            Tag tagvalue = new Tag();
            String id = XMLUtils.getAttributeValue(parser, "id");
            String count = XMLUtils.getAttributeValue(parser, "count");
            tagvalue.setId(id);
            tagvalue.setName(XMLUtils.getTextValue(parser));
            tagvalue.setSortOrder(ValueRetriever.getInt(count));
            getTags().add(tagvalue);
        } else if (tag.equals("type")) {
            setType(XMLUtils.getTextValue(parser));
        }
    }

    public void parse(Node node) {
        setId(((Element) node).getAttribute("id"));

        NodeList nodes = node.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node child = nodes.item(i);
            String tag = child.getNodeName();
            if (tag.equals("name")) {
                setName(XMLUtils.getTextValue(child));
            } else if (tag.equals("owner")) {
                setOwner(XMLUtils.getTextValue(child));
            } else if (tag.equals("items")) {
                setItems(XMLUtils.getTextValue(child));
            } else if (tag.equals("tag")) {
                Tag tagvalue = new Tag();
                tagvalue.setId(((Element) child).getAttribute("id"));
                tagvalue.setName(XMLUtils.getTextValue(child));
                tagvalue.setSortOrder(ValueRetriever.getInt(((Element) child)
                        .getAttribute("count")));
                getTags().add(tagvalue);
            } else if (tag.equals("type")) {
                setType(XMLUtils.getTextValue(child));
            }
        }
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setOwner(String owner) {
        this.mOwner = owner;
    }

    public String getOwner() {
        return mOwner;
    }

    public void setItems(String items) {
        this.mItems = items;
    }

    public String getItems() {
        return mItems;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public String getType() {
        return mType;
    }

    public ArrayList<Tag> getTags() {
        return mTags;
    }

    public void setTags(ArrayList<Tag> tags) {
        mTags = tags;
    }
}
