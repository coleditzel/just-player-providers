package jp.co.kayo.android.localplayer.ds.ampache;

public class ServerState {
    private boolean mSuccess;
    private String mAuthkey;
    private String mServerurl;
    // private boolean mUseSSL;

    private static volatile ServerState instance = new ServerState();

    protected ServerState() {
    }

    public static ServerState getInstance() {
        return instance;
    }

    public void setSuccess(boolean success) {
        this.mSuccess = success;
    }

    public boolean isSuccess() {
        return mSuccess;
    }

    public void setAuthkey(String authkey) {
        mAuthkey = authkey;
    }

    public String getAuthkey() {
        return mAuthkey;
    }

    public void setServerurl(String serverurl) {
        mServerurl = serverurl;
    }

    public String getServerurl() {
        return mServerurl;
    }

    /*
     * public void setUseSSL(boolean useSSL) { mUseSSL = useSSL; }
     * 
     * public boolean isUseSSL() { return mUseSSL; }
     */

}