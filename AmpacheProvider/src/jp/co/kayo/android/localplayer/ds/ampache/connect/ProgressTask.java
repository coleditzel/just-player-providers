package jp.co.kayo.android.localplayer.ds.ampache.connect;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;

public abstract class ProgressTask extends AsyncTask<Object, Object, Object>
        implements OnCancelListener {

    private Context mContext;
    private ProgressDialog mDialog;

    public ProgressTask(Context activityContext) {
        mContext = activityContext;

        mDialog = new ProgressDialog(mContext);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setCancelable(true);
    }

    @Override
    public void onPreExecute() {
        super.onPreExecute();

        mDialog.setOnCancelListener(this);
        mDialog.show();
    }

    @Override
    public abstract Object doInBackground(Object... params);

    @Override
    public void onPostExecute(Object result) {
        super.onPostExecute(result);
        mDialog.dismiss();
        mDialog = null;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        // ダイアログをキャンセルした場合
        this.cancel(false);
    }

    public void setMessage(String msg) {
        mDialog.setMessage(msg);
    }
}
