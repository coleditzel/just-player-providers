package jp.co.kayo.android.localplayer.ds.ampache;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;

public abstract class ShowProgressTask<Params, Progress, Result> extends
        AsyncTask<Params, Progress, Result> {

    private Dialog mProgressDialog;
    private Context mContext;
    private TextView mTxtProgress;
    private boolean mInProcess;

    public ShowProgressTask(Context context, Dialog dialog) {
        super();
        setProgressDialog(dialog);
        this.setTxtProgress((TextView) dialog.findViewById(R.id.txtProgress));
        this.getTxtProgress().setText(R.string.txt_progress);
        this.setContext(context);
    }

    public void onActivityPause() {
        dismisProgressDialog();
    }

    public void onActivityCreate(ProgressDialog progressDialog) {
        setProgressDialog(progressDialog);
        this.getTxtProgress().setText(R.string.txt_progress);
        // mProgressDialog.setMessage(context.getString(R.string.txt_progress));
        showProgressDialog();
    }

    public synchronized boolean isInProcess() {
        return mInProcess;
    }

    @Override
    protected void onPreExecute() {
        setInProcess(true);
        showProgressDialog();
    }

    @Override
    protected void onPostExecute(Result result) {
        dismisProgressDialog();
        setInProcess(false);
    }

    protected void showProgressDialog() {
        getProgressDialog().show();
    }

    protected void dismisProgressDialog() {
        if (getProgressDialog() != null && getProgressDialog().isShowing()) {
            getProgressDialog().dismiss();
        }
    }

    protected Dialog getProgressDialog() {
        return mProgressDialog;
    }

    private void setProgressDialog(Dialog progressDialog) {
        mProgressDialog = progressDialog;
    }

    protected void setContext(Context context) {
        mContext = context;
    }

    protected Context getContext() {
        return mContext;
    }

    private void setTxtProgress(TextView txtProgress) {
        mTxtProgress = txtProgress;
    }

    private TextView getTxtProgress() {
        return mTxtProgress;
    }

    private void setInProcess(boolean inProcess) {
        mInProcess = inProcess;
    }
}
