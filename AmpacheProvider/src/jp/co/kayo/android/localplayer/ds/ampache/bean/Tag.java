package jp.co.kayo.android.localplayer.ds.ampache.bean;

import jp.co.kayo.android.localplayer.ds.ampache.util.ValueRetriever;
import jp.co.kayo.android.localplayer.ds.ampache.util.XMLUtils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;

public class Tag {
    private String mId;
    private String mName;
    private int mNumAlbum;
    private int mNumArtist;
    private int mNumSong;
    private int mNumVideo;
    private int mNumPlaylist;
    private int mNumStream;
    private int mSortOrder;

    public void setValue(String server, String tag, XmlPullParser parser) {
        if (tag.equals("name")) {
            setName(XMLUtils.getTextValue(parser));
        } else if (tag.equals("albums")) {
            setNumAlbum(ValueRetriever.getInt(XMLUtils.getTextValue(parser)));
        } else if (tag.equals("artists")) {
            setNumArtist(ValueRetriever.getInt(XMLUtils.getTextValue(parser)));
        } else if (tag.equals("songs")) {
            setNumSong(ValueRetriever.getInt(XMLUtils.getTextValue(parser)));
        } else if (tag.equals("videos")) {
            setNumVideo(ValueRetriever.getInt(XMLUtils.getTextValue(parser)));
        } else if (tag.equals("playlists")) {
            setNumPlaylist(ValueRetriever.getInt(XMLUtils.getTextValue(parser)));
        } else if (tag.equals("stream")) {
            setNumStream(ValueRetriever.getInt(XMLUtils.getTextValue(parser)));
        }
    }

    public void parse(Node node) {
        setId(((Element) node).getAttribute("id"));

        NodeList nodes = node.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node child = nodes.item(i);
            String tag = child.getNodeName();
            if (tag.equals("name")) {
                setName(XMLUtils.getTextValue(child));
            } else if (tag.equals("albums")) {
                setNumAlbum(ValueRetriever.getInt(XMLUtils.getTextValue(child)));
            } else if (tag.equals("artists")) {
                setNumArtist(ValueRetriever
                        .getInt(XMLUtils.getTextValue(child)));
            } else if (tag.equals("songs")) {
                setNumSong(ValueRetriever.getInt(XMLUtils.getTextValue(child)));
            } else if (tag.equals("videos")) {
                setNumVideo(ValueRetriever.getInt(XMLUtils.getTextValue(child)));
            } else if (tag.equals("playlists")) {
                setNumPlaylist(ValueRetriever.getInt(XMLUtils
                        .getTextValue(child)));
            } else if (tag.equals("stream")) {
                setNumStream(ValueRetriever
                        .getInt(XMLUtils.getTextValue(child)));
            }
        }
    }

    public void setId(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setNumAlbum(int numAlbum) {
        mNumAlbum = numAlbum;
    }

    public int getNumAlbum() {
        return mNumAlbum;
    }

    public void setNumArtist(int numArtist) {
        mNumArtist = numArtist;
    }

    public int getNumArtist() {
        return mNumArtist;
    }

    public void setNumSong(int numSong) {
        mNumSong = numSong;
    }

    public int getNumSong() {
        return mNumSong;
    }

    public void setNumVideo(int numVideo) {
        mNumVideo = numVideo;
    }

    public int getNumVideo() {
        return mNumVideo;
    }

    public void setNumPlaylist(int numPlaylist) {
        mNumPlaylist = numPlaylist;
    }

    public int getNumPlaylist() {
        return mNumPlaylist;
    }

    public void setNumStream(int numStream) {
        mNumStream = numStream;
    }

    public int getNumStream() {
        return mNumStream;
    }

    public void setSortOrder(int sortOrder) {
        mSortOrder = sortOrder;
    }

    public int getSortOrder() {
        return mSortOrder;
    }
}
