package jp.co.kayo.android.localplayer.ds.ampache.bean;

import java.util.ArrayList;

import jp.co.kayo.android.localplayer.ds.ampache.util.ValueRetriever;
import jp.co.kayo.android.localplayer.ds.ampache.util.XMLUtils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;

public class Song {
    private String mId;
    private String mTitle;
    private Artist mArtist;
    private Album mAlbum;
    private ArrayList<Tag> mTags = new ArrayList<Tag>();
    private String mTrack;
    private long mSongTime;
    private String mUrl;
    private String mSize;
    private String mArt;
    private String mPreciserating;
    private int mRating;

    public void setValue(String server, String tag, XmlPullParser parser) {
        if ("title".equals(tag)) {
            setTitle(XMLUtils.getTextValue(parser));
        } else if ("artist".equals(tag)) {
            String id = XMLUtils.getAttributeValue(parser, "id");
            setArtist(new Artist());
            getArtist().setId(id);
            getArtist().setName(XMLUtils.getTextValue(parser));
        } else if (tag.equals("album")) {
            String id = XMLUtils.getAttributeValue(parser, "id");
            setAlbum(new Album());
            getAlbum().setId(id);
            getAlbum().setName(XMLUtils.getTextValue(parser));
        } else if (tag.equals("tag")) {
            String id = XMLUtils.getAttributeValue(parser, "id");
            String count = XMLUtils.getAttributeValue(parser, "count");
            Tag tagvalue = new Tag();
            tagvalue.setId(id);
            tagvalue.setName(XMLUtils.getTextValue(parser));
            tagvalue.setSortOrder(ValueRetriever.getInt(count));
            getTags().add(tagvalue);
        } else if (tag.equals("track")) {
            setTrack(XMLUtils.getTextValue(parser));
        } else if (tag.equals("time")) {
            setSongTime(getDURATION(XMLUtils.getTextValue(parser)));
        } else if (tag.equals("url")) {
            String u = XMLUtils.getTextValue(parser);

            String s1 = server.replaceFirst("[^/]*//[^/]+", "");
            String s2 = u.replaceFirst("[^/]*//[^/]+", "");
            setUrl(s1.equals("/") ? s2 : s2.replace(s1, ""));
            /*
             * String https = null; if (server != null &&
             * server.indexOf("https:") != -1) { https =
             * server.replace("https:", "http:"); } if (server == null) {
             * setUrl(u); } else if (https != null) { if (u.indexOf("http:") !=
             * -1) { setUrl(u.replaceFirst(https, "")); } else {
             * setUrl(u.replaceFirst(server, "")); } } else {
             * setUrl(u.replaceFirst(server, "")); }
             */
        } else if (tag.equals("size")) {
            setSize(XMLUtils.getTextValue(parser));
        } else if (tag.equals("art")) {
            String u = XMLUtils.getTextValue(parser);

            String s1 = server.replaceFirst("[^/]*//[^/]+", "");
            String s2 = u.replaceFirst("[^/]*//[^/]+", "");
            setArt(s1.equals("/") ? s2 : s2.replace(s1, ""));
            /*
             * String https = null; if (server != null &&
             * server.indexOf("https:") != -1) { https =
             * server.replace("https:", "http:"); } if (u != null) { if (server
             * == null) { setArt(u); } else if (https != null) { if
             * (u.indexOf("http:") != -1) { setArt(u.replaceFirst(https, "")); }
             * else { setArt(u.replaceFirst(server, "")); } } else {
             * setArt(u.replaceFirst(server, "")); } }
             */
        } else if (tag.equals("preciserating")) {
            setPreciserating(XMLUtils.getTextValue(parser));
        } else if (tag.equals("rating")) {
            setRating(ValueRetriever.getInt(XMLUtils.getTextValue(parser)));
        }
    }

    public void parse(String server, Node node) {
        setId(((Element) node).getAttribute("id"));

        /*
         * String https = null; if (server != null && server.indexOf("https:")
         * != -1) { https = server.replace("https:", "http:"); }
         */

        NodeList nodes = node.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node child = nodes.item(i);
            String tag = child.getNodeName();
            if (tag.equals("title")) {
                setTitle(XMLUtils.getTextValue(child));
            } else if (tag.equals("artist")) {
                setArtist(new Artist());
                getArtist().setId(((Element) child).getAttribute("id"));
                getArtist().setName(XMLUtils.getTextValue(child));
            } else if (tag.equals("album")) {
                setAlbum(new Album());
                getAlbum().setId(((Element) child).getAttribute("id"));
                getAlbum().setName(XMLUtils.getTextValue(child));
            } else if (tag.equals("tag")) {
                Tag tagvalue = new Tag();
                tagvalue.setId(((Element) child).getAttribute("id"));
                tagvalue.setName(XMLUtils.getTextValue(child));
                tagvalue.setSortOrder(ValueRetriever.getInt(((Element) child)
                        .getAttribute("count")));
                getTags().add(tagvalue);
            } else if (tag.equals("track")) {
                setTrack(XMLUtils.getTextValue(child));
            } else if (tag.equals("time")) {
                setSongTime(getDURATION(XMLUtils.getTextValue(child)));
            } else if (tag.equals("url")) {
                String u = XMLUtils.getTextValue(child);

                String s1 = server.replaceFirst("[^/]*//[^/]+", "");
                String s2 = u.replaceFirst("[^/]*//[^/]+", "");
                setUrl(s1.equals("/") ? s2 : s2.replace(s1, ""));
                /*
                 * if (server == null) { setUrl(u); } else if (https != null) {
                 * if (u.indexOf("http:") != -1) { setUrl(u.replaceFirst(https,
                 * "")); } else { setUrl(u.replaceFirst(server, "")); } } else {
                 * setUrl(u.replaceFirst(server, "")); }
                 */
            } else if (tag.equals("size")) {
                setSize(XMLUtils.getTextValue(child));
            } else if (tag.equals("art")) {
                String u = XMLUtils.getTextValue(child);
                if (u != null) {
                    String s1 = server.replaceFirst("[^/]*//[^/]+", "");
                    String s2 = u.replaceFirst("[^/]*//[^/]+", "");
                    setArt(s1.equals("/") ? s2 : s2.replace(s1, ""));
                }
                /*
                 * if (u != null) { if (server == null) { setArt(u); } else if
                 * (https != null) { if (u.indexOf("http:") != -1) {
                 * setArt(u.replaceFirst(https, "")); } else {
                 * setArt(u.replaceFirst(server, "")); } } else {
                 * setArt(u.replaceFirst(server, "")); } }
                 */
            } else if (tag.equals("preciserating")) {
                setPreciserating(XMLUtils.getTextValue(child));
            } else if (tag.equals("rating")) {
                setRating(ValueRetriever.getInt(XMLUtils.getTextValue(child)));
            }
        }
    }

    int getRATING() {
        int r = ValueRetriever.getInt(getPreciserating());
        return r;
    }

    public long getDURATION(String s) {
        try {
            return Long.parseLong(s) * 1000;
        } catch (Exception e) {

        }
        return 0;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setArtist(Artist artist) {
        mArtist = artist;
    }

    public Artist getArtist() {
        return mArtist;
    }

    public void setAlbum(Album album) {
        mAlbum = album;
    }

    public Album getAlbum() {
        return mAlbum;
    }

    public void setTags(ArrayList<Tag> tags) {
        mTags = tags;
    }

    public ArrayList<Tag> getTags() {
        return mTags;
    }

    public String getTagString() {
        StringBuilder buf = new StringBuilder();

        for (Tag tag : mTags) {
            buf.append("/").append(tag.getId()).append("/");
        }

        return buf.toString();
    }

    public void setTrack(String track) {
        mTrack = track;
    }

    public String getTrack() {
        return mTrack;
    }

    public void setSongTime(long songTime) {
        mSongTime = songTime;
    }

    public long getSongTime() {
        return mSongTime;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setSize(String size) {
        mSize = size;
    }

    public String getSize() {
        return mSize;
    }

    public void setArt(String art) {
        mArt = art;
    }

    public String getArt() {
        return mArt;
    }

    public void setPreciserating(String preciserating) {
        mPreciserating = preciserating;
    }

    public String getPreciserating() {
        return mPreciserating;
    }

    public void setRating(int rating) {
        mRating = rating;
    }

    public int getRating() {
        return mRating;
    }
}
