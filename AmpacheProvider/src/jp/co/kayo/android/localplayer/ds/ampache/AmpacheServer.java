package jp.co.kayo.android.localplayer.ds.ampache;

public class AmpacheServer {
    private String mUrl;
    private String mUsid;
    private String mPswd;
    private long mDateUpdate;
    private long mDateAdd;
    private long mDateClean;
    private int mNumSong;
    private int mNumAlbum;
    private int mNumArtist;
    private int mNumPlaylist;
    private int mNumVideo;
    private int mNumTag;
    private String mErrorText;
    private String mErrorCode;

    public void setUrl(String url) {
        this.mUrl = url;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUsid(String usid) {
        this.mUsid = usid;
    }

    public String getUsid() {
        return mUsid;
    }

    public void setPswd(String pswd) {
        this.mPswd = pswd;
    }

    public String getPswd() {
        return mPswd;
    }

    public void setDateUpdate(long update) {
        this.mDateUpdate = update;
    }

    public long getDateUpdate() {
        return mDateUpdate;
    }

    public void setDateAdd(long add) {
        this.mDateAdd = add;
    }

    public long getDateAdd() {
        return mDateAdd;
    }

    public void setDateClean(long clean) {
        this.mDateClean = clean;
    }

    public long getDateClean() {
        return mDateClean;
    }

    public void setNumSong(int songs) {
        this.mNumSong = songs;
    }

    public int getNumSong() {
        return mNumSong;
    }

    public void setNumAlbum(int albums) {
        this.mNumAlbum = albums;
    }

    public int getNumAlbum() {
        return mNumAlbum;
    }

    public void setNumArtist(int artists) {
        this.mNumArtist = artists;
    }

    public int getNumArtist() {
        return mNumArtist;
    }

    public void setNumPlaylist(int playlists) {
        this.mNumPlaylist = playlists;
    }

    public int getNumPlaylist() {
        return mNumPlaylist;
    }

    public void setNumVideo(int videos) {
        this.mNumVideo = videos;
    }

    public int getNumVideo() {
        return mNumVideo;
    }

    public void setNumTag(int tag) {
        this.mNumTag = tag;
    }

    public int getNumTag() {
        return mNumTag;
    }

    public String getErrorText() {
        return mErrorText;
    }

    public void setErrorText(String errorText) {
        mErrorText = errorText;
    }

    public String getErrorCode() {
        return mErrorCode;
    }

    public void setErrorCode(String errorCode) {
        mErrorCode = errorCode;
    }

}
