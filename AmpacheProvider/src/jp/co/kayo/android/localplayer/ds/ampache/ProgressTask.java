package jp.co.kayo.android.localplayer.ds.ampache;

import jp.co.kayo.android.localplayer.ds.ampache.util.ProgressUtils;
import android.content.Context;
import android.os.AsyncTask;

public abstract class ProgressTask extends AsyncTask<Void, Integer, Void> {
    private int mNumMax;
    private Context mContext;

    public ProgressTask(Context context, int max) {
        this.setNumMax(max);
        this.setContext(context);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected void onPostExecute(Void result) {
        ProgressUtils.stopProgress();
    }

    @Override
    protected void onPreExecute() {
        ProgressUtils.startProgress(getNumMax());
    }

    protected void setProgress(int pos) {
        publishProgress(new Integer(pos));
    }

    protected void toast(String s) {
        ProgressUtils.toast(s);
    }

    protected void title(String s) {
        ProgressUtils.title(s);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        ProgressUtils.moveProgress(values[0]);
    }

    protected void setNumMax(int numMax) {
        mNumMax = numMax;
    }

    protected int getNumMax() {
        return mNumMax;
    }

    protected void setContext(Context context) {
        mContext = context;
    }

    protected Context getContext() {
        return mContext;
    }

}
