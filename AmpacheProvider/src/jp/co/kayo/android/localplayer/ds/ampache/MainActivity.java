package jp.co.kayo.android.localplayer.ds.ampache;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

import jp.co.kayo.android.localplayer.ds.ampache.bean.Album;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Artist;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Playlist;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Song;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Tag;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Video;
import jp.co.kayo.android.localplayer.ds.ampache.connect.AmpacheConnector;
import jp.co.kayo.android.localplayer.ds.ampache.connect.ServerConfig;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts;
import jp.co.kayo.android.localplayer.ds.ampache.consts.SystemConsts;
import jp.co.kayo.android.localplayer.ds.ampache.consts.TableConsts;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.ds.ampache.util.Logger;
import jp.co.kayo.android.localplayer.ds.ampache.util.XMLUtils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class MainActivity extends Activity implements OnClickListener {
    //OwnCloud: /remote.php/ampache
    //Ampache: 
    
    public static final String CNF_PASS_LOOP = "chkPassLoop";
    public static final String CNF_WIFI_ONLY = "chkWIFIOnly";

    private final String KEY_CONFIG_HISTORY = "KEY_CONFIG_HISTORY";
    private final int MAX_INSTANCE = 5;

    private SharedPreferences mPref;
    private EditText mEditHost;
    private EditText mEditUsername;
    private EditText mEditPassword;
    // private CheckBox mChkUseSSL;
    private CheckBox mChkPassLoop;
    private CheckBox mChkWIFIOnly;
    private Spinner mSpinner;
    MakeAmpacheIndex mTask;
    private Button mBtnConnect;

    private void btnStateCheck() {
        mBtnConnect.setText(R.string.txt_connect);

        Button btnAllSync = (Button) findViewById(R.id.btnAllSync);
        Button btnMediaSync = (Button) findViewById(R.id.btnMediaSync);
        Button btnVideosSync = (Button) findViewById(R.id.btnVideosSync);

        btnMediaSync.setEnabled(false);
        btnVideosSync.setEnabled(false);
        btnAllSync.setEnabled(false);
    }

    private void btnStateOk() {
        mBtnConnect.setText(R.string.txt_connected);

        Button btnAllSync = (Button) findViewById(R.id.btnAllSync);
        Button btnMediaSync = (Button) findViewById(R.id.btnMediaSync);
        Button btnVideosSync = (Button) findViewById(R.id.btnVideosSync);

        btnMediaSync.setEnabled(true);
        btnVideosSync.setEnabled(true);
        btnAllSync.setEnabled(true);
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == AmpacheConnector.EVT_AMPACHE_ABORT) {
                Logger.d("handler, EVT_AMPACHE_ABORT");
                Toast.makeText(getApplicationContext(), R.string.txt_abort,
                        Toast.LENGTH_SHORT).show();
                btnStateCheck();
            } else if (msg.what == AmpacheConnector.EVT_SET_SERVER_INF) {
                Object[] args = (Object[]) msg.obj;
                AmpacheServer serv = null;
                ServerConfig config = null;
                if (args != null) {
                    serv = (AmpacheServer) args[0];
                    config = (ServerConfig) args[1];
                    setTextToTextView(R.id.textSongs,
                            Integer.toString(serv.getNumSong()));
                    setTextToTextView(R.id.textAlbums,
                            Integer.toString(serv.getNumAlbum()));
                    setTextToTextView(R.id.textArtists,
                            Integer.toString(serv.getNumArtist()));
                    setTextToTextView(R.id.textPlaylists,
                            Integer.toString(serv.getNumPlaylist()));
                    setTextToTextView(R.id.textVideos,
                            Integer.toString(serv.getNumVideo()));
                    setTextToTextView(R.id.textTag,
                            Integer.toString(serv.getNumTag()));
                }
                if (serv != null && serv.getErrorText() != null) {
                    Toast.makeText(getApplicationContext(),
                            serv.getErrorText(), Toast.LENGTH_SHORT).show();
                    btnStateCheck();
                } else if (config != null) {
                    config.save(mPref);
                    Toast.makeText(getApplicationContext(),
                            R.string.txt_connected_server, Toast.LENGTH_SHORT)
                            .show();
                    btnStateOk();
                    ArrayList<ServerConfig> saves = new ArrayList<ServerConfig>();
                    ArrayList<ServerConfig> configs = getHistory();
                    for (ServerConfig conf : configs) {
                        if (!conf.host.equals(config.host)) {
                            saves.add(conf);
                        }
                    }
                    saves.add(0, config);
                    saveHistory(saves);
                }
                updateState();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_main);
        getWindow().setSoftInputMode(
                LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mPref = PreferenceManager.getDefaultSharedPreferences(this);
        mEditHost = (EditText) findViewById(R.id.editHost);
        mEditUsername = (EditText) findViewById(R.id.editUsername);
        mEditPassword = (EditText) findViewById(R.id.editPassword);
        // mChkUseSSL = (CheckBox) findViewById(R.id.chkUseSSL);
        mChkPassLoop = (CheckBox) findViewById(R.id.chkPassLoop);
        mChkWIFIOnly = (CheckBox) findViewById(R.id.chkWIFIOnly);
        Button btnSet = (Button) findViewById(R.id.btnSet);
        Button btnAllSync = (Button) findViewById(R.id.btnAllSync);
        Button btnDbClear = (Button) findViewById(R.id.btnDbClear);
        Button btnMediaSync = (Button) findViewById(R.id.btnMediaSync);
        Button btnVideosSync = (Button) findViewById(R.id.btnVideosSync);

        mSpinner = (Spinner) findViewById(R.id.spinDatabaseName);
        mBtnConnect = (Button) findViewById(R.id.btnConnect);

        // load
        ServerConfig config = new ServerConfig();
        config.load(mPref);

        // set
        mEditHost.setText(config.host);
        mEditUsername.setText(config.user);
        mEditPassword.setText(config.pass);
        // mChkUseSSL.setChecked(config.useSSL);
        mChkPassLoop.setChecked(mPref.getBoolean(CNF_PASS_LOOP, false));
        mChkWIFIOnly.setChecked(mPref.getBoolean(CNF_WIFI_ONLY, true));

        btnSet.setOnClickListener(this);
        btnAllSync.setOnClickListener(this);
        btnDbClear.setOnClickListener(this);
        mBtnConnect.setOnClickListener(this);
        btnMediaSync.setOnClickListener(this);
        btnVideosSync.setOnClickListener(this);
        btnStateCheck();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item);
        for (int i = 0; i < MAX_INSTANCE; i++) {
            String dbname = AmpacheHelper.getDbName(i);

            adapter.add(dbname);
        }
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                    int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        mSpinner.setSelection(mPref.getInt(AmpacheHelper.AMPACHE_INDEX,
                SystemConsts.DEFAULT_INDEX));

        updateState();
        
        //Ampacheをデフォルトにする
        ContentValues values = new ContentValues();
        values.put("key_current_uri", MediaConsts.AMPACHE_AUTHORITY);
        try{
            getContentResolver().update(Uri.parse("content://jp.co.kayo.android.localplayer/pref"), values, null, null);
        }
        catch(Exception e){
            
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        Editor editor = mPref.edit();
        editor.putBoolean(CNF_PASS_LOOP, mChkPassLoop.isChecked());
        editor.putBoolean(CNF_WIFI_ONLY, mChkWIFIOnly.isChecked());
        editor.commit();
    }

    @Override
    public void onClick(View v) {
        int btnId = v.getId();

        switch (btnId) {
        case R.id.btnSet: {
            final ArrayList<ServerConfig> configs = getHistory();
            if (configs.size() > 0) {
                String[] str_items = new String[configs.size()];
                for (int i = 0; i < configs.size(); i++) {
                    str_items[i] = configs.get(i).host;
                }
                new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.lb_select_server))
                        .setItems(str_items,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                            int which) {
                                        setConfig(configs.get(which));
                                    }
                                }).show();
            }
        }
            break;
        case R.id.btnConnect: {
            ServerConfig config = new ServerConfig();
            config.host = mEditHost.getText().toString().trim();
            config.user = mEditUsername.getText().toString().trim();
            config.pass = mEditPassword.getText().toString().trim();
            // config.useSSL = mChkUseSSL.isChecked();
            config.dbIndex = mSpinner.getSelectedItemPosition();

            // TODO: 入力値エラーチェック
            if (nullCheck(config.host) != true) {
                Toast.makeText(this,
                        getString(R.string.lb_input_ampache_server),
                        Toast.LENGTH_SHORT).show();
                return;
            }
            if (nullCheck(config.user) != true) {
                Toast.makeText(this, getString(R.string.lb_input_ampache_usid),
                        Toast.LENGTH_SHORT).show();
                return;
            }
            if (nullCheck(config.pass) != true) {
                Toast.makeText(this, getString(R.string.lb_input_ampache_pswd),
                        Toast.LENGTH_SHORT).show();
                return;
            }
            
            getContentResolver().query(MediaConsts.CLOSE_CONTENT_URI, null,
                    null, null, null);
            AmpacheConnector connector = new AmpacheConnector(this, mHandler,
                    config);
            connector.setMessage(getString(R.string.txt_connecting));
            connector.execute();
        }
            break;
        case R.id.btnAllSync: {
            syncProc();
        }
            break;
        case R.id.btnDbClear: {
            resetDb();
        }
            break;
        case R.id.btnMediaSync: {
            syncMusic();
        }
            break;
        case R.id.btnVideosSync: {
            syncVideo();
        }
            break;
        }
    }

    private boolean nullCheck(String s) {
        if (s != null && s.trim().length() > 0) {
            return true;
        }
        return false;
    }

    private void resetDb() {
        final String[] dblist = new String[MAX_INSTANCE];
        final boolean[] chklist = new boolean[MAX_INSTANCE];
        for (int i = 0; i < MAX_INSTANCE; i++) {
            String dbname = AmpacheHelper.getDbName(i);
            dblist[i] = dbname;
            chklist[i] = false;
        }

        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.txt_check_dbclear))
                .setMultiChoiceItems(dblist, chklist,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which, boolean isChecked) {
                                chklist[which] = isChecked;
                            }
                        })
                .setPositiveButton(getString(R.string.lb_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int whichButton) {
                                for (int i = 0; i < chklist.length; i++) {
                                    if (chklist[i]) {
                                        String dbname = dblist[i];
                                        AmpacheDatabaseHelper helper = new AmpacheDatabaseHelper(
                                                MainActivity.this, dbname);
                                        SQLiteDatabase db = helper
                                                .getWritableDatabase();
                                        try {
                                            db.beginTransaction();
                                            helper.rebuild(db);
                                            db.setTransactionSuccessful();
                                        } finally {
                                            db.endTransaction();
                                            db.close();
                                            helper.close();
                                        }
                                    }
                                }
                                updateState();
                            }
                        })
                .setNegativeButton(getString(R.string.lb_cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int whichButton) {
                            }
                        }).show();
    }

    private void syncProc() {
        getContentResolver().query(
                MediaConsts.RESET_CONTENT_URI,
                new String[] { TableConsts.TBNAME_ALBUM,
                        TableConsts.TBNAME_ARTIST, TableConsts.TBNAME_GENRES,
                        TableConsts.TBNAME_PLAYLIST,
                        TableConsts.TBNAME_PLAYLIST_AUDIO,
                        TableConsts.TBNAME_AUDIO }, null, null, null);

        ServerConfig config = new ServerConfig();
        config.host = mEditHost.getText().toString().trim();
        config.user = mEditUsername.getText().toString().trim();
        config.pass = mEditPassword.getText().toString().trim();
        // config.useSSL = mChkUseSSL.isChecked();
        config.dbIndex = mSpinner.getSelectedItemPosition();

        mTask = new MakeAmpacheIndex(this, createProgressDialog(), config);
        mTask.execute(new String[] { TableConsts.TBNAME_ALBUM,
                TableConsts.TBNAME_ARTIST, TableConsts.TBNAME_GENRES,
                TableConsts.TBNAME_PLAYLIST });
    }

    private void syncMusic() {
        getContentResolver().query(MediaConsts.RESET_CONTENT_URI,
                new String[] { TableConsts.TBNAME_AUDIO }, null, null, null);

        ServerConfig config = new ServerConfig();
        config.host = mEditHost.getText().toString().trim();
        config.user = mEditUsername.getText().toString().trim();
        config.pass = mEditPassword.getText().toString().trim();
        // config.useSSL = mChkUseSSL.isChecked();
        config.dbIndex = mSpinner.getSelectedItemPosition();

        mTask = new MakeAmpacheIndex(this, createProgressDialog(), config);
        mTask.execute(new String[] { TableConsts.TBNAME_AUDIO });
    }

    private void syncVideo() {
        getContentResolver().query(MediaConsts.RESET_CONTENT_URI,
                new String[] { TableConsts.TBNAME_VIDEO }, null, null, null);

        ServerConfig config = new ServerConfig();
        config.host = mEditHost.getText().toString().trim();
        config.user = mEditUsername.getText().toString().trim();
        config.pass = mEditPassword.getText().toString().trim();
        // config.useSSL = mChkUseSSL.isChecked();
        config.dbIndex = mSpinner.getSelectedItemPosition();

        mTask = new MakeAmpacheIndex(this, createProgressDialog(), config);
        mTask.execute(new String[] { TableConsts.TBNAME_VIDEO });
    }

    private Dialog createProgressDialog() {
        Dialog pDiag = new SyncProgressDialog(this);
        pDiag.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pDiag.setContentView(R.layout.ampache_progress);
        pDiag.setCancelable(false);

        Button btnCancel = (Button) pDiag.findViewById(R.id.btnProgressCancel);
        OnClickListener listener;
        listener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTask != null) {
                    mTask.helper.forceEnd();
                    mTask.mForceClose = true;
                    // mTask.cancel(true);
                    // mTask = null;
                }
            }
        };
        btnCancel.setOnClickListener(listener);

        return pDiag;
    }

    private void setConfig(ServerConfig config) {
        mEditHost.setText(config.host);
        mEditUsername.setText(config.user);
        mEditPassword.setText(config.pass);
        // mChkUseSSL.setChecked(config.useSSL);
        mSpinner.setSelection(config.dbIndex);
        updateState();
        btnStateCheck();
    }

    private void updateState() {
        loadSyncDate(mEditHost.getText().toString().trim());

        String dbname = AmpacheHelper.getDbName(mSpinner
                .getSelectedItemPosition());
        collectDatabaseInfo(dbname);
    }

    private void collectDatabaseInfo(String dbname) {
        AmpacheDatabaseHelper helper = null;
        SQLiteDatabase db = null;
        Cursor cur = null;
        try {
            helper = new AmpacheDatabaseHelper(this, dbname);
            db = helper.getReadableDatabase();
            setTextToTextView(R.id.textLocalAlbums, Long.toString(helper
                    .getCount(db, TableConsts.TBNAME_ALBUM)));
            setTextToTextView(R.id.textLocalArtists, Long.toString(helper
                    .getCount(db, TableConsts.TBNAME_ARTIST)));
            setTextToTextView(R.id.textLocalPlaylists, Long.toString(helper
                    .getCount(db, TableConsts.TBNAME_PLAYLIST)));
            setTextToTextView(R.id.textLocalVideos, Long.toString(helper
                    .getCount(db, TableConsts.TBNAME_VIDEO)));
            setTextToTextView(R.id.textLocalTag, Long.toString(helper.getCount(
                    db, TableConsts.TBNAME_GENRES)));
            setTextToTextView(R.id.textLocalSongs, Long.toString(helper
                    .getCount(db, TableConsts.TBNAME_AUDIO)));
        } finally {
            if (cur != null) {
                cur.close();
            }
            if (db != null) {
                db.close();
                db = null;
            }
            if (helper != null) {
                helper.close();
            }
        }
    }

    private void setTextToTextView(int id, String s) {
        TextView text = (TextView) findViewById(id);
        text.setText(s);
    }

    private void loadSyncDate(String url) {
        if (url != null) {
            AmpacheHelper helper = new AmpacheHelper(getApplicationContext());
            String lastUpdate = getDate(helper.getLastUpdate(url, false));
            setTextToTextView(R.id.textSongsSync, lastUpdate);
        } else {
            setTextToTextView(R.id.textSongsSync,
                    getString(R.string.lb_notdata));
        }
    }

    private String getDate(long date) {
        if (date > -1) {
            DateFormat fmt = android.text.format.DateFormat
                    .getMediumDateFormat(this);
            return fmt.format(new Date(date));
        } else {
            return "-";
        }
    }

    private void saveHistory(ArrayList<ServerConfig> configs) {
        JSONArray array = new JSONArray();
        for (ServerConfig conf : configs) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("host", conf.host);
                obj.put("user", conf.user);
                obj.put("pass", conf.pass);
                obj.put("index", conf.dbIndex);
                // obj.put("ssl", conf.useSSL);
                array.put(obj);
            } catch (JSONException e) {
            }
        }
        Editor editor = mPref.edit();
        editor.putString(KEY_CONFIG_HISTORY, array.toString());
        editor.commit();
    }

    private ArrayList<ServerConfig> getHistory() {
        ArrayList<ServerConfig> configs = new ArrayList<ServerConfig>();
        String history = mPref.getString(KEY_CONFIG_HISTORY, "");
        if (history.length() > 0) {
            try {
                JSONArray array = new JSONArray(history);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    ServerConfig conf = new ServerConfig();
                    conf.host = obj.getString("host");
                    conf.user = obj.getString("user");
                    conf.pass = obj.getString("pass");
                    conf.dbIndex = obj.getInt("index");
                    /*
                     * if (!obj.isNull("ssl")) conf.useSSL =
                     * obj.getBoolean("ssl"); else conf.useSSL = false;
                     */
                    configs.add(conf);
                }
            } catch (Exception e) {
            }
        }
        return configs;
    }

    private class SyncProgressDialog extends Dialog {
        public SyncProgressDialog(Context context) {
            super(context);
        }

        @Override
        public void onBackPressed() {
            if (mTask != null) {
                mTask.helper.forceEnd();
                mTask.mForceClose = true;
                // mTask.cancel(true);
                // mTask = null;
            }
        }
    }

    class MakeAmpacheIndex extends ShowProgressTask {
        public boolean mForceClose = false;;
        private final int GET_LIMIT = 30;
        private ServerConfig mConfig;
        private int mProgMax = 0;
        private ProgressBar mProgbar1;
        private ProgressBar mProgbar2;
        int mOffset = 0;
        int mAlbumCount = 0;
        int mArtistCount = 0;
        int mTagCount = 0;
        int mPlaylistCount = 0;
        int mVideoCount = 0;
        int mSongCount = 0;
        AmpacheServer mServ;
        SQLiteDatabase mDb;
        AmpacheHelper helper;
        HashSet<String> albumkeys = new HashSet<String>();
        HashSet<String> artistkeys = new HashSet<String>();
        HashSet<String> tagkeys = new HashSet<String>();
        Calendar cal = Calendar.getInstance();

        public MakeAmpacheIndex(Context context, Dialog dialog,
                ServerConfig config) {
            super(context, dialog);
            this.mConfig = config;
            helper = new AmpacheHelper(getContext());
        }

        private void setMax(int max) {
            mProgMax = max;
            if (mProgbar1 != null) {
                mProgbar1.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mProgMax > 0) {
                            mProgbar1.setMax(mProgMax);
                            mProgbar1.setVisibility(View.VISIBLE);
                            mProgbar2.setVisibility(View.GONE);
                        } else {
                            mProgbar1.setVisibility(View.GONE);
                            mProgbar2.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            super.onProgressUpdate(values);
            String msg = (String) values[0];
            ((TextView) getProgressDialog().findViewById(R.id.txtProgress))
                    .setText(msg);
            if (values.length > 1) {
                int pos = (Integer) values[1];
                if (mProgbar1 != null) {
                    mProgbar1.setProgress(pos);
                }
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);
            updateState();
        }

        @Override
        protected Object doInBackground(Object... params) {
            String[] syncIds = (String[]) params;

            // Server info
            String authToken = helper.auth(getContext(), mConfig.host,
                    mConfig.user, mConfig.pass);
            if (authToken != null) {
                mServ = helper.getServerInfo(getContext(), mConfig.host,
                        mConfig.user, mConfig.pass);
                if (mServ != null && mServ.getErrorText() == null) {
                    String dbname = AmpacheHelper.getDbName(mConfig.dbIndex);
                    AmpacheDatabaseHelper dbhelper = new AmpacheDatabaseHelper(
                            getContext(), dbname);
                    try {
                        mDb = dbhelper.getWritableDatabase();
                        mDb.beginTransaction();
                        for (String task : syncIds) {
                            if (task.equals(TableConsts.TBNAME_ALBUM)) {
                                setMax(mServ.getNumAlbum());
                                updateAlbum(authToken, null);
                            } else if (task.equals(TableConsts.TBNAME_ARTIST)) {
                                setMax(mServ.getNumArtist());
                                updateArtist(authToken, null);
                            } else if (task.equals(TableConsts.TBNAME_GENRES)) {
                                setMax(mServ.getNumTag());
                                updateGenres(authToken, null);
                            } else if (task.equals(TableConsts.TBNAME_PLAYLIST)) {
                                setMax(mServ.getNumPlaylist());
                                updatePlaylist(authToken, null);
                            } else if (task.equals(TableConsts.TBNAME_AUDIO)) {
                                setMax(mServ.getNumSong());
                                updateAudio(mServ, helper, dbhelper, authToken,
                                        null);
                            } else if (task.equals(TableConsts.TBNAME_VIDEO)) {
                                setMax(mServ.getNumVideo());
                                updateVideo(authToken, null);
                            }
                        }
                        mDb.setTransactionSuccessful();
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } finally {
                        if (mDb != null) {
                            mDb.endTransaction();
                            mDb.close();
                            mDb = null;
                            dbhelper.close();
                        }
                    }
                } else if (mServ != null && mServ.getErrorText() != null) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this,
                                    mServ.getErrorText(), Toast.LENGTH_SHORT)
                                    .show();
                        }
                    });
                }
            }

            return null;
        }

        Creator ampacheCreator = new Creator() {

            @Override
            public void createAlbum(Album album) {
                helper.insertAlbum(mDb, album);

                mAlbumCount++;
                publishProgress(getContext()
                        .getString(R.string.fmt_sync_albums)
                        + mAlbumCount
                        + " / " + mServ.getNumAlbum(), mAlbumCount);
            }

            @Override
            public void createArtist(Artist artist) {
                helper.insertArtist(mDb, artist);

                mArtistCount++;
                publishProgress(
                        getContext().getString(R.string.fmt_sync_artists)
                                + mArtistCount + " / " + mServ.getNumArtist(),
                        mArtistCount);
            }

            @Override
            public void createTag(Tag tag) {
                helper.insertGenres(mDb, tag);

                mTagCount++;
                publishProgress(getContext().getString(R.string.fmt_sync_tag)
                        + mTagCount, mTagCount);
            }

            @Override
            public void createPlaylist(Playlist playlist) {
                helper.insertPlaylist(mDb, playlist);

                mPlaylistCount++;
                publishProgress(
                        getContext().getString(R.string.fmt_sync_playlists)
                                + mPlaylistCount + " / "
                                + mServ.getNumPlaylist(), mPlaylistCount);
            }

            @Override
            public void createVideo(Video video) {
                helper.insertVideo(mDb, video);

                mVideoCount++;
                publishProgress(getContext()
                        .getString(R.string.fmt_sync_videos)
                        + mVideoCount
                        + " / " + mServ.getNumVideo(), mVideoCount);
            }

            @Override
            public void createSong(Song song) {
                //Ampacheの同期で音楽の登録時に呼ばれる
                ContentValues dvalues = new ContentValues();
                dvalues.put(AudioMedia._ID, song.getId());
                dvalues.put(AudioMedia.MEDIA_KEY, song.getId());
                dvalues.put(AudioMedia.TITLE, song.getTitle());
                if (song.getTitle() != null) {
                    dvalues.put(AudioMedia.TITLE_KEY, song.getTitle()
                            .hashCode());
                }
                dvalues.put(AudioMedia.DURATION, song.getSongTime());
                if (song.getArtist() != null) {
                    dvalues.put(AudioMedia.ARTIST, song.getArtist().getName());
                    dvalues.put(AudioMedia.ARTIST_KEY, song.getArtist().getId());
                    artistkeys.add(song.getArtist().getId());
                }
                if (song.getAlbum() != null) {
                    dvalues.put(AudioMedia.ALBUM, song.getAlbum().getName());
                    dvalues.put(AudioMedia.ALBUM_KEY, song.getAlbum().getId());
                    albumkeys.add(song.getAlbum().getId());
                }
                if (song.getTags() != null && song.getTags().size() > 0) {
                    for (Tag t : song.getTags()) {
                        if (t.getId() != null && t.getId().length() > 0) {
                            tagkeys.add(t.getId());
                        }
                    }
                }
                // dvalues.put(AudioMedia.ALBUM_ART,
                // song.getArt());
                dvalues.put(AudioMedia.DATA, song.getUrl());
                dvalues.put(AudioMedia.TRACK, song.getTrack());
                dvalues.put(AudioMedia.DATE_ADDED, cal.getTimeInMillis());
                dvalues.put(AudioMedia.DATE_MODIFIED, cal.getTimeInMillis());
                dvalues.put(TableConsts.AUDIO_DEL_FLG, 0);
                dvalues.put(TableConsts.GENRES_TAGS, song.getTagString());
                if (song.getAlbum() != null) {
                    dvalues.put(AudioMedia.YEAR, song.getAlbum().getYear());
                }
                dvalues.put(TableConsts.FAVORITE_POINT, song.getRating());
                
                //キャッシュの有無
                File cacheFile = RequestCache.getCacheFile(song.getUrl());
                if(cacheFile!=null && cacheFile.exists()){
                    dvalues.put(TableConsts.AUDIO_CACHE_FILE, cacheFile.getName());
                }
                long id = mDb.replace(TableConsts.TBNAME_AUDIO, null, dvalues);

                mSongCount++;
                publishProgress(getContext().getString(R.string.fmt_sync_song)
                        + mSongCount + " / " + mServ.getNumSong(), mSongCount);
            }

        };

        
        
        private void updateAlbum(String authToken, String fromDate)
                throws MalformedURLException {
            mOffset = 0;
            boolean mContinue = true;
            while (mContinue && !mForceClose) {
                mContinue = false;
                StringBuilder buf = new StringBuilder();
                AmpacheHelper.appendSubPath(buf, mConfig.host,
                        "server/xml.server.php?action=albums&auth=");
                buf.append(authToken);
                buf.append("&offset=").append(mOffset);
                if (fromDate != null) {
                    buf.append("&add=").append(fromDate);
                }
                buf.append("&limit=").append(GET_LIMIT);

                Logger.d("url=" + buf.toString());
                AmpacheXMLHandler handler = (AmpacheXMLHandler) helper
                        .parseXml(new URL(buf.toString()),
                                new AmpacheXMLHandler(mConfig.host,
                                        ampacheCreator));
                if (!mChkPassLoop.isChecked() && mServ.getNumAlbum() > 0) {
                    mContinue = mOffset < mServ.getNumAlbum();
                } else {
                    mContinue = handler.mContinue;
                }
                /*
                 * final byte[] bufs = helper.getBytes(new URL(buf.toString()));
                 * if (bufs == null || bufs.length == 0) { break; } else {
                 * AmpacheXMLHandler handler = (AmpacheXMLHandler)
                 * helper.parseXml(bufs, new AmpacheXMLHandler(mConfig.host,
                 * ampacheCreator)); if(!mChkPassLoop.isChecked() &&
                 * mServ.getNumAlbum()>0){ mContinue =
                 * mOffset<mServ.getNumAlbum(); } else{ mContinue =
                 * handler.mContinue; } }
                 */
                mOffset += GET_LIMIT;
            }
        }

        private void updateArtist(String authToken, String fromDate)
                throws MalformedURLException {
            boolean mContinue = true;
            mOffset = 0;
            while (mContinue && !mForceClose) {
                mContinue = false;
                StringBuilder buf = new StringBuilder();
                AmpacheHelper.appendSubPath(buf, mConfig.host,
                        "server/xml.server.php?action=artists&auth=");
                buf.append(authToken);
                buf.append("&offset=").append(mOffset);
                if (fromDate != null) {
                    buf.append("&add=").append(fromDate);
                }
                buf.append("&limit=").append(GET_LIMIT);

                AmpacheXMLHandler handler = (AmpacheXMLHandler) helper
                        .parseXml(new URL(buf.toString()),
                                new AmpacheXMLHandler(mConfig.host,
                                        ampacheCreator));
                if (!mChkPassLoop.isChecked() && mServ.getNumArtist() > 0) {
                    mContinue = mOffset < mServ.getNumArtist();
                } else {
                    mContinue = handler.mContinue;
                }
                /*
                 * final byte[] bufs = helper.getBytes(new URL(buf.toString()));
                 * if (bufs == null || bufs.length == 0) { break; } else {
                 * AmpacheXMLHandler handler = (AmpacheXMLHandler)
                 * helper.parseXml(bufs, new AmpacheXMLHandler(mConfig.host,
                 * ampacheCreator)); if(!mChkPassLoop.isChecked() &&
                 * mServ.getNumArtist()>0){ mContinue =
                 * mOffset<mServ.getNumArtist(); } else{ mContinue =
                 * handler.mContinue; } }
                 */

                mOffset += GET_LIMIT;
            }
        }

        private void updateGenres(String authToken, String fromDate)
                throws MalformedURLException {
            boolean mContinue = true;
            mOffset = 0;
            while (mContinue && !mForceClose) {
                mContinue = false;

                StringBuilder buf = new StringBuilder();
                AmpacheHelper.appendSubPath(buf, mConfig.host,
                        "server/xml.server.php?action=tags&auth=");
                buf.append(authToken);
                buf.append("&offset=").append(mOffset);
                buf.append("&limit=").append(GET_LIMIT);

                AmpacheXMLHandler handler = (AmpacheXMLHandler) helper
                        .parseXml(new URL(buf.toString()),
                                new AmpacheXMLHandler(mConfig.host,
                                        ampacheCreator));
                if (!mChkPassLoop.isChecked() && mServ.getNumTag() > 0) {
                    mContinue = mOffset < mServ.getNumTag();
                } else {
                    mContinue = handler.mContinue;
                }
                /*
                 * final byte[] bufs = helper.getBytes(new URL(buf.toString()));
                 * if (bufs == null || bufs.length == 0) { break; } else {
                 * AmpacheXMLHandler handler = (AmpacheXMLHandler)
                 * helper.parseXml(bufs, new AmpacheXMLHandler(mConfig.host,
                 * ampacheCreator)); if(!mChkPassLoop.isChecked() &&
                 * mServ.getNumTag()>0){ mContinue = mOffset<mServ.getNumTag();
                 * } else{ mContinue = handler.mContinue; } }
                 */

                mOffset += GET_LIMIT;
            }
        }

        private void updatePlaylist(String authToken, String fromDate)
                throws MalformedURLException {
            boolean mContinue = true;
            mOffset = 0;
            while (mContinue && !mForceClose) {
                mContinue = false;
                StringBuilder buf = new StringBuilder();
                AmpacheHelper.appendSubPath(buf, mConfig.host,
                        "server/xml.server.php?action=playlists&auth=");
                buf.append(authToken);
                buf.append("&offset=").append(mOffset);
                if (fromDate != null) {
                    buf.append("&add=").append(fromDate);
                }
                buf.append("&limit=").append(GET_LIMIT);

                AmpacheXMLHandler handler = (AmpacheXMLHandler) helper
                        .parseXml(new URL(buf.toString()),
                                new AmpacheXMLHandler(mConfig.host,
                                        ampacheCreator));
                if (!mChkPassLoop.isChecked() && mServ.getNumPlaylist() > 0) {
                    mContinue = mOffset < mServ.getNumPlaylist();
                } else {
                    mContinue = handler.mContinue;
                }
                /*
                 * final byte[] bufs = helper.getBytes(new URL(buf.toString()));
                 * if (bufs == null || bufs.length == 0) { break; } else {
                 * AmpacheXMLHandler handler = (AmpacheXMLHandler)
                 * helper.parseXml(bufs, new AmpacheXMLHandler(mConfig.host,
                 * ampacheCreator)); if(!mChkPassLoop.isChecked() &&
                 * mServ.getNumPlaylist()>0){ mContinue =
                 * mOffset<mServ.getNumPlaylist(); } else{ mContinue =
                 * handler.mContinue; } }
                 */

                mOffset += GET_LIMIT;
            }
        }

        private void updateVideo(String authToken, String fromDate)
                throws MalformedURLException {
            boolean mContinue = true;
            mOffset = 0;
            while (mContinue && !mForceClose) {
                mContinue = false;
                StringBuilder buf = new StringBuilder();
                AmpacheHelper.appendSubPath(buf, mConfig.host,
                        "server/xml.server.php?action=videos&auth=");
                buf.append(authToken);
                buf.append("&offset=").append(mOffset);
                if (fromDate != null) {
                    buf.append("&add=").append(fromDate);
                }
                buf.append("&limit=").append(GET_LIMIT);

                AmpacheXMLHandler handler = (AmpacheXMLHandler) helper
                        .parseXml(new URL(buf.toString()),
                                new AmpacheXMLHandler(mConfig.host,
                                        ampacheCreator));
                if (!mChkPassLoop.isChecked() && mServ.getNumVideo() > 0) {
                    mContinue = mOffset < mServ.getNumVideo();
                } else {
                    mContinue = handler.mContinue;
                }
                /*
                 * final byte[] bufs = helper.getBytes(new URL(buf.toString()));
                 * if (bufs == null || bufs.length == 0) { break; } else {
                 * AmpacheXMLHandler handler = (AmpacheXMLHandler)
                 * helper.parseXml(bufs, new AmpacheXMLHandler(mConfig.host,
                 * ampacheCreator)); if(!mChkPassLoop.isChecked() &&
                 * mServ.getNumVideo()>0){ mContinue =
                 * mOffset<mServ.getNumVideo(); } else{ mContinue =
                 * handler.mContinue; } }
                 */

                mOffset += GET_LIMIT;
            }
        }

        private void updateAudio(final AmpacheServer serv,
                final AmpacheHelper helper, AmpacheDatabaseHelper dbhelper,
                String authToken, String fromDate) throws MalformedURLException {
            boolean mContinue = true;
            mOffset = 0;
            while (mContinue && !mForceClose) {
                mContinue = false;
                StringBuilder buf = new StringBuilder();
                AmpacheHelper.appendSubPath(buf, mConfig.host,
                        "server/xml.server.php?action=songs&auth=");
                buf.append(authToken);
                buf.append("&offset=").append(mOffset);
                if (fromDate != null) {
                    buf.append("&add=").append(fromDate);
                }
                buf.append("&limit=").append(GET_LIMIT);

                AmpacheXMLHandler handler = (AmpacheXMLHandler) helper
                        .parseXml(new URL(buf.toString()),
                                new AmpacheXMLHandler(mConfig.host,
                                        ampacheCreator));
                if (!mChkPassLoop.isChecked() && mServ.getNumSong() > 0) {
                    mContinue = mOffset < mServ.getNumSong();
                } else {
                    mContinue = handler.mContinue;
                }

                mOffset += GET_LIMIT;
            }

            if (!mForceClose) {
                setMax(albumkeys.size());
                for (Iterator<String> ite = albumkeys.iterator(); ite.hasNext()
                        && mContinue;) {
                    String album_key = ite.next();

                    if (!helper.isRegisterdAlbum(mDb, album_key)) {
                        StringBuilder buf = new StringBuilder();
                        AmpacheHelper.appendSubPath(buf, mConfig.host,
                                "server/xml.server.php?action=album&auth=");
                        buf.append(authToken);
                        buf.append("&filter=").append(album_key);
                        Logger.d(buf.toString());
                        helper.parseXml(new URL(buf.toString()),
                                new AmpacheXMLHandler(mConfig.host,
                                        ampacheCreator));
                    } else {
                        mAlbumCount++;
                        publishProgress(
                                getContext()
                                        .getString(R.string.fmt_sync_albums)
                                        + mAlbumCount
                                        + " / "
                                        + albumkeys.size(), mAlbumCount);
                    }

                    ContentValues val = new ContentValues();
                    val.put(TableConsts.ALBUM_INIT_FLG, 1);
                    mDb.update(TableConsts.TBNAME_ALBUM, val,
                            AudioAlbum.ALBUM_KEY + "=?",
                            new String[] { album_key });
                }
            }

            if (!mForceClose) {
                setMax(artistkeys.size());
                for (Iterator<String> ite = artistkeys.iterator(); ite
                        .hasNext() && mContinue;) {
                    String artist_key = ite.next();

                    if (!helper.isRegisterdArtist(mDb, artist_key)) {
                        StringBuilder buf = new StringBuilder();
                        AmpacheHelper.appendSubPath(buf, mConfig.host,
                                "server/xml.server.php?action=artist&auth=");
                        buf.append(authToken);
                        buf.append("&filter=").append(artist_key);
                        Logger.d(buf.toString());
                        helper.parseXml(new URL(buf.toString()),
                                new AmpacheXMLHandler(mConfig.host,
                                        ampacheCreator));
                    } else {
                        mArtistCount++;
                        publishProgress(
                                getContext().getString(
                                        R.string.fmt_sync_artists)
                                        + mArtistCount
                                        + " / "
                                        + artistkeys.size(), mArtistCount);
                    }
                }
            }
            if (!mForceClose) {
                setMax(tagkeys.size());
                for (Iterator<String> ite = tagkeys.iterator(); ite.hasNext()
                        && mContinue;) {
                    String genres_key = ite.next();

                    if (!helper.isRegisterdGenres(mDb, genres_key)) {
                        StringBuilder buf = new StringBuilder();
                        AmpacheHelper.appendSubPath(buf, mConfig.host,
                                "server/xml.server.php?action=tag&auth=");
                        buf.append(authToken);
                        buf.append("&filter=").append(genres_key);
                        Logger.d(buf.toString());
                        helper.parseXml(new URL(buf.toString()),
                                new AmpacheXMLHandler(mConfig.host,
                                        ampacheCreator));
                    } else {
                        mTagCount++;
                        publishProgress(
                                getContext().getString(R.string.fmt_sync_tag)
                                        + mTagCount + " / " + tagkeys.size(),
                                mTagCount);
                    }
                }
            }
        }

    }
}
