package jp.co.kayo.android.localplayer.ds.ampache;

import jp.co.kayo.android.localplayer.ds.ampache.util.XMLUtils;

import org.xmlpull.v1.XmlPullParser;

public class AuthHandler extends XMLHandler {
    private String mAuthToken;

    public String getAuthToken() {
        return mAuthToken;
    }

    public void setAuthToken(String authToken) {
        mAuthToken = authToken;
    }

    @Override
    public boolean endTag(XmlPullParser parser) {
        return true;
    }

    @Override
    public boolean startTag(XmlPullParser parser) {
        String tag = parser.getName();
        if (tag.equals("auth")) {
            mAuthToken = XMLUtils.getTextValue(parser);
            return false;
        }
        return true;
    }

}
