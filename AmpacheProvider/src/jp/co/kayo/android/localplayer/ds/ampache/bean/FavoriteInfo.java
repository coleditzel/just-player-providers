package jp.co.kayo.android.localplayer.ds.ampache.bean;

public class FavoriteInfo {
    private long mId;
    private long mMediaId;
    private int mRating;

    public void setId(long id) {
        mId = id;
    }

    public long getId() {
        return mId;
    }

    public void setMediaId(long mediaId) {
        mMediaId = mediaId;
    }

    public long getMediaId() {
        return mMediaId;
    }

    public void setRating(int rating) {
        mRating = rating;
    }

    public int getRating() {
        return mRating;
    }
}
