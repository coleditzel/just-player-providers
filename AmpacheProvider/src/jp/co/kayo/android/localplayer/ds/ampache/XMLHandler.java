package jp.co.kayo.android.localplayer.ds.ampache;

import java.io.IOException;

import jp.co.kayo.android.localplayer.ds.ampache.util.Logger;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public abstract class XMLHandler {

    public String getText(XmlPullParser parser) {
        try {
            return parser.nextText();
        } catch (XmlPullParserException e) {
            Logger.e("XMLHandler.XmlPullParserException", e);
        } catch (IOException e) {
            Logger.e("XMLHandler.IOException", e);
        }
        return "";
    }

    public abstract boolean endTag(XmlPullParser parser);

    public abstract boolean startTag(XmlPullParser parser);

}
