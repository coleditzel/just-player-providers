package jp.co.kayo.android.localplayer.ds.ampache.connect;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;

public class SimpleResponseHandlerFactory {

    /**
     * アクセスして結果を返すだけのResponseHandler
     * 
     * @return
     */
    public ResponseHandler<String> simpleResponseHandler()
            throws RuntimeException, ClientProtocolException, IOException {
        ResponseHandler<String> respHandler = new ResponseHandler<String>() {
            @Override
            public String handleResponse(HttpResponse response)
                    throws ClientProtocolException, IOException {
                int statusCode = response.getStatusLine().getStatusCode();
                switch (statusCode) {
                case HttpStatus.SC_OK: {
                    return EntityUtils.toString(response.getEntity(), "UTF-8");
                }
                case HttpStatus.SC_NOT_FOUND: {
                    throw new RuntimeException("NOT_FOUND");
                }
                default: {
                    throw new RuntimeException("Connection Error");
                }
                }
            }
        };

        return respHandler;
    }

    /**
     * InputStreamで結果を返すResponseHandler
     * 
     * @return
     */
    public ResponseHandler<InputStream> inputStreamResponseHandler()
            throws RuntimeException, ClientProtocolException, IOException {
        ResponseHandler<InputStream> respHandler = new ResponseHandler<InputStream>() {
            @Override
            public InputStream handleResponse(HttpResponse response)
                    throws ClientProtocolException, IOException {
                int statusCode = response.getStatusLine().getStatusCode();
                switch (statusCode) {
                case HttpStatus.SC_OK: {
                    InputStream in = response.getEntity().getContent();

                    return in;
                }
                case HttpStatus.SC_NOT_FOUND: {
                    throw new RuntimeException("NOT_FOUND");
                }
                default: {
                    throw new RuntimeException("Connection Error");
                }
                }
            }
        };

        return respHandler;
    }
}
