package jp.co.kayo.android.localplayer.ds.ampache.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.text.format.Time;
import android.util.TimeFormatException;

public class ValueRetriever {

    public static int getInt(String s) {
        if (s != null && s.length() > 0) {
            try {
                return Integer.parseInt(s);
            } catch (Exception e) {
            }
        }
        return 0;
    }

    public static long getLong(String text) {
        if (text != null && text.length() > 0) {
            try {
                return Long.parseLong(text);
            } catch (Exception e) {

            }
        }
        return 0;
    }

    static SimpleDateFormat _fmt = new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ssZ");

    public static long parseDate(String text) {
        if (text != null && text.length() > 0) {
            try {
                // 2011-08-01T16:03:16+09:00
                Date date = _fmt.parse(text);
                return date.getTime();
            } catch (Exception e) {
            }
        }
        return 0;
    }

}
